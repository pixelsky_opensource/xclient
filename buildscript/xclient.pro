-injars  build/xprotect_deobf.jar
-outjars build/xprotect_obf.jar
-keepattributes *Annotation*
-keep class !ru.pixelsky.** { *; }

-libraryjars  <java.home>/lib/rt.jar
-libraryjars  <java.home>/lib/jce.jar
-libraryjars  jars/bin/lwjgl.jar
-libraryjars  jars/bin/jinput.jar
-libraryjars  jars/bin/lwjgl_util.jar
-libraryjars  lib/asm-debug-all-4.1.jar
-libraryjars lib/bcprov-debug-jdk15on-148.jar
-libraryjars lib/guava-14.0-rc3.jar
-libraryjars lib/guava-14.0-rc3.jar
-libraryjars build/minecraft_library.jar

-ignorewarnings

-printmapping build/xprotect_obf.map

-dontshrink
-dontoptimize
-dontpreverify
