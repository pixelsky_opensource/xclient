#!/bin/bash

# PART 1.COMPILATION.
./recompile.sh || exit 1

# PART 2. OBFUSCATION.
rm -rf psky-classes
mkdir psky-classes
mkdir psky-classes/ru
mkdir psky-classes/ru/pixelsky
mv bin/minecraft/ru/pixelsky/xprotect psky-classes/ru/pixelsky/

# Extract XProtect classes
pushd psky-classes/
rm ../build/xprotect_deobf.jar
zip -r -9 ../build/xprotect_deobf.jar .
popd

# Make deobfuscated minecraft.jar for proguard library
pushd bin/minecraft
rm ../../build/minecraft_library.jar
zip -r -9 ../../build/minecraft_library.jar .
popd

# Obfuscate XProtect
~/apps/proguard4.9/bin/proguard.sh @xclient.pro || exit 1

# Copy obfuscated xProtect
unzip build/xprotect_obf.jar -d bin/minecraft

./reobfuscate.sh || exit 1

# PART 3. PACKING.

# Make empty directory for build
rm -rf build/classes
mkdir build/classes

# Unpack minecraft.jar only with Forge
unzip build/minecraft.jar -d build/classes

# Copy modified files
cp -R reobf/minecraft/* build/classes/ 

# Copy resource files
cp -R res/* build/classes/ 

# Copy liteloader
# Not needed now
# unzip -o build/liteloader_1.5.2.zip -d build/classes || exit 1

# Zip to minecraft.jar
cd build/classes
rm ../new_minecraft.jar
zip -r -9 ../new_minecraft.jar .

# Delete dir
rm -rf build/classes
