package net.minecraft.server.management;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.network.packet.Packet56MapChunks;
import net.minecraft.server.management.PlayerInstance;
import net.minecraft.src.CompactArrayList;
import net.minecraft.src.Config;
import net.minecraft.util.LongHashMap;
import net.minecraft.world.ChunkCoordIntPair;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.WorldServer;
import net.minecraft.world.chunk.Chunk;

public class PlayerManager {

   private final WorldServer theWorldServer;
   private final List players = new ArrayList();
   private final LongHashMap playerInstances = new LongHashMap();
   private List chunkWatcherWithPlayers = new ArrayList();
   public CompactArrayList chunkCoordsNotLoaded = new CompactArrayList(100, 0.8F);
   private int playerViewRadius;
   private final int[][] xzDirectionsConst = new int[][]{{1, 0}, {0, 1}, {-1, 0}, {0, -1}};


   public PlayerManager(WorldServer par1WorldServer, int par2) {
      if(par2 > 15) {
         throw new IllegalArgumentException("Too big view radius!");
      } else if(par2 < 3) {
         throw new IllegalArgumentException("Too small view radius!");
      } else {
         this.playerViewRadius = Config.getChunkViewDistance();
         Config.dbg("ViewRadius: " + this.playerViewRadius + ", for: " + this + " (constructor)");
         this.theWorldServer = par1WorldServer;
      }
   }

   public WorldServer getWorldServer() {
      return this.theWorldServer;
   }

   public void updatePlayerInstances() {
      int ip;
      for(ip = 0; ip < this.chunkWatcherWithPlayers.size(); ++ip) {
         ((PlayerInstance)this.chunkWatcherWithPlayers.get(ip)).sendChunkUpdate();
      }

      this.chunkWatcherWithPlayers.clear();
      if(this.players.isEmpty()) {
         WorldProvider var18 = this.theWorldServer.provider;
         if(!var18.canRespawnHere()) {
            this.theWorldServer.theChunkProviderServer.unloadAllChunks();
         }
      }

      if(this.playerViewRadius != Config.getChunkViewDistance()) {
         this.setPlayerViewRadius(Config.getChunkViewDistance());
      }

      if(this.chunkCoordsNotLoaded.size() > 0) {
         for(ip = 0; ip < this.players.size(); ++ip) {
            EntityPlayerMP player = (EntityPlayerMP)this.players.get(ip);
            int px = player.chunkCoordX;
            int pz = player.chunkCoordZ;
            int maxRadius = this.playerViewRadius + 1;
            int maxRadius2 = maxRadius / 2;
            int maxDistSq = maxRadius * maxRadius + maxRadius2 * maxRadius2;
            int bestDistSq = maxDistSq;
            int bestIndex = -1;
            PlayerInstance bestCw = null;
            ChunkCoordIntPair bestCoords = null;

            for(int chunk = 0; chunk < this.chunkCoordsNotLoaded.size(); ++chunk) {
               ChunkCoordIntPair coords = (ChunkCoordIntPair)this.chunkCoordsNotLoaded.get(chunk);
               if(coords != null) {
                  PlayerInstance cw = this.getOrCreateChunkWatcher(coords.chunkXPos, coords.chunkZPos, false);
                  if(cw != null && !cw.chunkLoaded) {
                     int dx = px - coords.chunkXPos;
                     int dz = pz - coords.chunkZPos;
                     int distSq = dx * dx + dz * dz;
                     if(distSq < bestDistSq) {
                        bestDistSq = distSq;
                        bestIndex = chunk;
                        bestCw = cw;
                        bestCoords = coords;
                     }
                  } else {
                     this.chunkCoordsNotLoaded.set(chunk, (Object)null);
                  }
               }
            }

            if(bestIndex >= 0) {
               this.chunkCoordsNotLoaded.set(bestIndex, (Object)null);
            }

            if(bestCw != null) {
               bestCw.chunkLoaded = true;
               this.getWorldServer().theChunkProviderServer.loadChunk(bestCoords.chunkXPos, bestCoords.chunkZPos);
               bestCw.sendThisChunkToAllPlayers();
               break;
            }
         }

         this.chunkCoordsNotLoaded.compact();
      }

   }

   private PlayerInstance getOrCreateChunkWatcher(int par1, int par2, boolean par3) {
      return this.getOrCreateChunkWatcher(par1, par2, par3, false);
   }

   private PlayerInstance getOrCreateChunkWatcher(int par1, int par2, boolean par3, boolean lazy) {
      long var4 = (long)par1 + 2147483647L | (long)par2 + 2147483647L << 32;
      PlayerInstance var6 = (PlayerInstance)this.playerInstances.getValueByKey(var4);
      if(var6 == null && par3) {
         var6 = new PlayerInstance(this, par1, par2, lazy);
         this.playerInstances.add(var4, var6);
      }

      return var6;
   }

   public void flagChunkForUpdate(int par1, int par2, int par3) {
      int var4 = par1 >> 4;
      int var5 = par3 >> 4;
      PlayerInstance var6 = this.getOrCreateChunkWatcher(var4, var5, false);
      if(var6 != null) {
         var6.flagChunkForUpdate(par1 & 15, par2, par3 & 15);
      }

   }

   public void addPlayer(EntityPlayerMP par1EntityPlayerMP) {
      int var2 = (int)par1EntityPlayerMP.posX >> 4;
      int var3 = (int)par1EntityPlayerMP.posZ >> 4;
      par1EntityPlayerMP.managedPosX = par1EntityPlayerMP.posX;
      par1EntityPlayerMP.managedPosZ = par1EntityPlayerMP.posZ;
      ArrayList spawnList = new ArrayList(1);

      for(int var4 = var2 - this.playerViewRadius; var4 <= var2 + this.playerViewRadius; ++var4) {
         for(int var5 = var3 - this.playerViewRadius; var5 <= var3 + this.playerViewRadius; ++var5) {
            this.getOrCreateChunkWatcher(var4, var5, true).addPlayerToChunkWatchingList(par1EntityPlayerMP);
            if(var4 >= var2 - 1 && var4 <= var2 + 1 && var5 >= var3 - 1 && var5 <= var3 + 1) {
               Chunk spawnChunk = this.getWorldServer().theChunkProviderServer.loadChunk(var4, var5);
               spawnList.add(spawnChunk);
            }
         }
      }

      par1EntityPlayerMP.playerNetServerHandler.sendPacketToPlayer(new Packet56MapChunks(spawnList));
      this.players.add(par1EntityPlayerMP);
      this.filterChunkLoadQueue(par1EntityPlayerMP);
   }

   public void filterChunkLoadQueue(EntityPlayerMP par1EntityPlayerMP) {
      ArrayList var2 = new ArrayList(par1EntityPlayerMP.loadedChunks);
      int var3 = 0;
      int var4 = this.playerViewRadius;
      int var5 = (int)par1EntityPlayerMP.posX >> 4;
      int var6 = (int)par1EntityPlayerMP.posZ >> 4;
      int var7 = 0;
      int var8 = 0;
      ChunkCoordIntPair var9 = PlayerInstance.getChunkLocation(this.getOrCreateChunkWatcher(var5, var6, true));
      par1EntityPlayerMP.loadedChunks.clear();
      if(var2.contains(var9)) {
         par1EntityPlayerMP.loadedChunks.add(var9);
      }

      int var10;
      for(var10 = 1; var10 <= var4 * 2; ++var10) {
         for(int var11 = 0; var11 < 2; ++var11) {
            int[] var12 = this.xzDirectionsConst[var3++ % 4];

            for(int var13 = 0; var13 < var10; ++var13) {
               var7 += var12[0];
               var8 += var12[1];
               var9 = PlayerInstance.getChunkLocation(this.getOrCreateChunkWatcher(var5 + var7, var6 + var8, true));
               if(var2.contains(var9)) {
                  par1EntityPlayerMP.loadedChunks.add(var9);
               }
            }
         }
      }

      var3 %= 4;

      for(var10 = 0; var10 < var4 * 2; ++var10) {
         var7 += this.xzDirectionsConst[var3][0];
         var8 += this.xzDirectionsConst[var3][1];
         var9 = PlayerInstance.getChunkLocation(this.getOrCreateChunkWatcher(var5 + var7, var6 + var8, true));
         if(var2.contains(var9)) {
            par1EntityPlayerMP.loadedChunks.add(var9);
         }
      }

   }

   public void removePlayer(EntityPlayerMP par1EntityPlayerMP) {
      int var2 = (int)par1EntityPlayerMP.managedPosX >> 4;
      int var3 = (int)par1EntityPlayerMP.managedPosZ >> 4;

      for(int var4 = var2 - this.playerViewRadius; var4 <= var2 + this.playerViewRadius; ++var4) {
         for(int var5 = var3 - this.playerViewRadius; var5 <= var3 + this.playerViewRadius; ++var5) {
            PlayerInstance var6 = this.getOrCreateChunkWatcher(var4, var5, false);
            if(var6 != null) {
               var6.sendThisChunkToPlayer(par1EntityPlayerMP, false);
            }
         }
      }

      this.players.remove(par1EntityPlayerMP);
   }

   private boolean func_72684_a(int par1, int par2, int par3, int par4, int par5) {
      int var6 = par1 - par3;
      int var7 = par2 - par4;
      return var6 >= -par5 && var6 <= par5?var7 >= -par5 && var7 <= par5:false;
   }

   public void updateMountedMovingPlayer(EntityPlayerMP par1EntityPlayerMP) {
      int var2 = (int)par1EntityPlayerMP.posX >> 4;
      int var3 = (int)par1EntityPlayerMP.posZ >> 4;
      double var4 = par1EntityPlayerMP.managedPosX - par1EntityPlayerMP.posX;
      double var6 = par1EntityPlayerMP.managedPosZ - par1EntityPlayerMP.posZ;
      double var8 = var4 * var4 + var6 * var6;
      if(var8 >= 64.0D) {
         int var10 = (int)par1EntityPlayerMP.managedPosX >> 4;
         int var11 = (int)par1EntityPlayerMP.managedPosZ >> 4;
         int var12 = this.playerViewRadius;
         int var13 = var2 - var10;
         int var14 = var3 - var11;
         if(var13 != 0 || var14 != 0) {
            for(int var15 = var2 - var12; var15 <= var2 + var12; ++var15) {
               for(int var16 = var3 - var12; var16 <= var3 + var12; ++var16) {
                  if(!this.func_72684_a(var15, var16, var10, var11, var12)) {
                     this.getOrCreateChunkWatcher(var15, var16, true, true).addPlayerToChunkWatchingList(par1EntityPlayerMP);
                  }

                  if(!this.func_72684_a(var15 - var13, var16 - var14, var2, var3, var12)) {
                     PlayerInstance var17 = this.getOrCreateChunkWatcher(var15 - var13, var16 - var14, false);
                     if(var17 != null) {
                        var17.sendThisChunkToPlayer(par1EntityPlayerMP);
                     }
                  }
               }
            }

            this.filterChunkLoadQueue(par1EntityPlayerMP);
            par1EntityPlayerMP.managedPosX = par1EntityPlayerMP.posX;
            par1EntityPlayerMP.managedPosZ = par1EntityPlayerMP.posZ;
         }
      }

   }

   public boolean isPlayerWatchingChunk(EntityPlayerMP par1EntityPlayerMP, int par2, int par3) {
      PlayerInstance var4 = this.getOrCreateChunkWatcher(par2, par3, false);
      return var4 == null?false:PlayerInstance.getPlayersInChunk(var4).contains(par1EntityPlayerMP) && !par1EntityPlayerMP.loadedChunks.contains(PlayerInstance.getChunkLocation(var4));
   }

   public static int getFurthestViewableBlock(int par0) {
      return par0 * 16 - 16;
   }

   static WorldServer getWorldServer(PlayerManager par0PlayerManager) {
      return par0PlayerManager.theWorldServer;
   }

   static LongHashMap getChunkWatchers(PlayerManager par0PlayerManager) {
      return par0PlayerManager.playerInstances;
   }

   static List getChunkWatchersWithPlayers(PlayerManager par0PlayerManager) {
      return par0PlayerManager.chunkWatcherWithPlayers;
   }

   private void setPlayerViewRadius(int newRadius) {
      if(this.playerViewRadius != newRadius) {
         EntityPlayerMP[] eps = (EntityPlayerMP[])((EntityPlayerMP[])this.players.toArray(new EntityPlayerMP[this.players.size()]));

         int i;
         EntityPlayerMP ep;
         for(i = 0; i < eps.length; ++i) {
            ep = eps[i];
            this.removePlayer(ep);
         }

         this.playerViewRadius = newRadius;

         for(i = 0; i < eps.length; ++i) {
            ep = eps[i];
            this.addPlayer(ep);
         }

         Config.dbg("ViewRadius: " + this.playerViewRadius + ", for: " + this + " (detect)");
      }
   }
}
