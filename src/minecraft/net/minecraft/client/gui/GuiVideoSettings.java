package net.minecraft.client.gui;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.src.GuiAnimationSettingsOF;
import net.minecraft.src.GuiDetailSettingsOF;
import net.minecraft.src.GuiOtherSettingsOF;
import net.minecraft.src.GuiPerformanceSettingsOF;
import net.minecraft.src.GuiQualitySettingsOF;
import net.minecraft.util.StringTranslate;

public class GuiVideoSettings extends GuiScreen {

   private GuiScreen parentGuiScreen;
   protected String screenTitle = "Настройка графики";
   private GameSettings guiGameSettings;
   private boolean is64bit = false;
   private static EnumOptions[] videoOptions = new EnumOptions[]{EnumOptions.GRAPHICS, EnumOptions.RENDER_DISTANCE_FINE, EnumOptions.AMBIENT_OCCLUSION, EnumOptions.FRAMERATE_LIMIT_FINE, EnumOptions.AO_LEVEL, EnumOptions.VIEW_BOBBING, EnumOptions.GUI_SCALE, EnumOptions.ADVANCED_OPENGL, EnumOptions.GAMMA, EnumOptions.CHUNK_LOADING, EnumOptions.FOG_FANCY, EnumOptions.FOG_START, EnumOptions.USE_SERVER_TEXTURES};
   private int lastMouseX = 0;
   private int lastMouseY = 0;
   private long mouseStillTime = 0L;


   public GuiVideoSettings(GuiScreen par1GuiScreen, GameSettings par2GameSettings) {
      this.parentGuiScreen = par1GuiScreen;
      this.guiGameSettings = par2GameSettings;
   }

   public void initGui() {
      StringTranslate var1 = StringTranslate.getInstance();
      this.screenTitle = var1.translateKey("options.videoTitle");
      int var2 = 0;
      EnumOptions[] var3 = videoOptions;
      int var4 = var3.length;

      int var5;
      int x;
      for(var5 = 0; var5 < var4; ++var5) {
         EnumOptions y = var3[var5];
         x = super.width / 2 - 155 + var5 % 2 * 160;
         int var9 = super.height / 6 + 21 * (var5 / 2) - 10;
         if(y.getEnumFloat()) {
            super.buttonList.add(new GuiSlider(y.returnEnumOrdinal(), x, var9, y, this.guiGameSettings.getKeyBinding(y), this.guiGameSettings.getOptionFloatValue(y)));
         } else {
            super.buttonList.add(new GuiSmallButton(y.returnEnumOrdinal(), x, var9, y, this.guiGameSettings.getKeyBinding(y)));
         }

         ++var2;
      }

      int var13 = super.height / 6 + 21 * (var5 / 2) - 10;
      boolean var14 = false;
      x = super.width / 2 - 155 + 160;
      super.buttonList.add(new GuiSmallButton(102, x, var13, "Качество"));
      var13 += 21;
      x = super.width / 2 - 155 + 0;
      super.buttonList.add(new GuiSmallButton(101, x, var13, "Детализация"));
      x = super.width / 2 - 155 + 160;
      super.buttonList.add(new GuiSmallButton(112, x, var13, "Производительность"));
      var13 += 21;
      x = super.width / 2 - 155 + 0;
      super.buttonList.add(new GuiSmallButton(111, x, var13, "Анимации"));
      x = super.width / 2 - 155 + 160;
      super.buttonList.add(new GuiSmallButton(122, x, var13, "Прочее"));
      super.buttonList.add(new GuiButton(200, super.width / 2 - 100, super.height / 6 + 168 + 11, var1.translateKey("gui.done")));
      this.is64bit = false;
      String[] var15 = new String[]{"sun.arch.data.model", "com.ibm.vm.bitmode", "os.arch"};
      String[] var10 = var15;
      var5 = var15.length;

      for(int var11 = 0; var11 < var5; ++var11) {
         String var7 = var10[var11];
         String var8 = System.getProperty(var7);
         if(var8 != null && var8.contains("64")) {
            this.is64bit = true;
            break;
         }
      }

   }

   protected void actionPerformed(GuiButton par1GuiButton) {
      if(par1GuiButton.enabled) {
         int var2 = this.guiGameSettings.guiScale;
         if(par1GuiButton.id < 100 && par1GuiButton instanceof GuiSmallButton) {
            this.guiGameSettings.setOptionValue(((GuiSmallButton)par1GuiButton).returnEnumOptions(), 1);
            par1GuiButton.displayString = this.guiGameSettings.getKeyBinding(EnumOptions.getEnumOptions(par1GuiButton.id));
         }

         if(par1GuiButton.id == 200) {
            super.mc.gameSettings.saveOptions();
            super.mc.displayGuiScreen(this.parentGuiScreen);
         }

         if(this.guiGameSettings.guiScale != var2) {
            ScaledResolution scr = new ScaledResolution(super.mc.gameSettings, super.mc.displayWidth, super.mc.displayHeight);
            int var4 = scr.getScaledWidth();
            int var5 = scr.getScaledHeight();
            this.setWorldAndResolution(super.mc, var4, var5);
         }

         if(par1GuiButton.id == 101) {
            super.mc.gameSettings.saveOptions();
            GuiDetailSettingsOF scr1 = new GuiDetailSettingsOF(this, this.guiGameSettings);
            super.mc.displayGuiScreen(scr1);
         }

         if(par1GuiButton.id == 102) {
            super.mc.gameSettings.saveOptions();
            GuiQualitySettingsOF scr2 = new GuiQualitySettingsOF(this, this.guiGameSettings);
            super.mc.displayGuiScreen(scr2);
         }

         if(par1GuiButton.id == 111) {
            super.mc.gameSettings.saveOptions();
            GuiAnimationSettingsOF scr3 = new GuiAnimationSettingsOF(this, this.guiGameSettings);
            super.mc.displayGuiScreen(scr3);
         }

         if(par1GuiButton.id == 112) {
            super.mc.gameSettings.saveOptions();
            GuiPerformanceSettingsOF scr4 = new GuiPerformanceSettingsOF(this, this.guiGameSettings);
            super.mc.displayGuiScreen(scr4);
         }

         if(par1GuiButton.id == 122) {
            super.mc.gameSettings.saveOptions();
            GuiOtherSettingsOF scr5 = new GuiOtherSettingsOF(this, this.guiGameSettings);
            super.mc.displayGuiScreen(scr5);
         }

         if(par1GuiButton.id == EnumOptions.AO_LEVEL.ordinal()) {
            return;
         }
      }

   }

   public void drawScreen(int x, int y, float z) {
      this.drawDefaultBackground();
      this.drawCenteredString(super.fontRenderer, this.screenTitle, super.width / 2, 20, 16777215);
      super.drawScreen(x, y, z);
      if(Math.abs(x - this.lastMouseX) <= 5 && Math.abs(y - this.lastMouseY) <= 5) {
         short activateDelay = 700;
         if(System.currentTimeMillis() >= this.mouseStillTime + (long)activateDelay) {
            int x1 = super.width / 2 - 150;
            int y1 = super.height / 6 - 5;
            if(y <= y1 + 98) {
               y1 += 105;
            }

            int x2 = x1 + 150 + 150;
            int y2 = y1 + 84 + 10;
            GuiButton btn = this.getSelectedButton(x, y);
            if(btn != null) {
               String s = this.getButtonName(btn.displayString);
               String[] lines = this.getTooltipLines(s);
               if(lines == null) {
                  return;
               }

               this.drawGradientRect(x1, y1, x2, y2, -536870912, -536870912);

               for(int i = 0; i < lines.length; ++i) {
                  String line = lines[i];
                  super.fontRenderer.drawStringWithShadow(line, x1 + 5, y1 + 5 + i * 11, 14540253);
               }
            }

         }
      } else {
         this.lastMouseX = x;
         this.lastMouseY = y;
         this.mouseStillTime = System.currentTimeMillis();
      }
   }

   private String[] getTooltipLines(String btnName) {
     return btnName.equals("Graphics")?new String[]{"Визуальное качество", "  Быстро - плохое качество", "  Красиво - высокое качество", "Меняет прорисовку облаков, лавы, воды", "теней и травы."}:(btnName.equals("Render Distance")?new String[]{"Видимое расстояние", "  Крошечный - 32м (Очень быстро)", "  Короткий - 64м (быстро)", "  Нормальный - 128м", "  Далеко - 256m (медленно)", "  Экстримальный - 512м (Очень медленно!)", "Этот вид дистанции очень требователен к ресурсам компьютера!"}:(btnName.equals("Smooth Lighting")?new String[]{"Мягкое освещение", "  Выкл. - нет мягкого освещения (быстро)", "  1% - мягкое освещение (медленно)", "  100% - сильное мягкое освещение (очень медленно)"}:(btnName.equals("Performance")?new String[]{"FPS ограничение", "  Max FPS - нет ограничений (очень быстро)", "  Баланс - ограничение в 120 FPS (медленно)", "  Сохранение энергии - ограничение в 40 FPS (очень медленно)", "  VSync - ограничение до частоты монитора (60, 30, 20)", "Баланс и сохранение энергии уменьшают при необходимости FPS", "Предельное значение не достигнуто"}:(btnName.equals("3D Anaglyph")?new String[]{"3D мод используется вместе с 3D очками."}:(btnName.equals("View Bobbing")?new String[]{"Больше реалистичных движений.", "Если используется mip-текстурирование, отключите для достижения наилучших результатов."}:(btnName.equals("GUI Scale")?new String[]{"GUI масштаб", "чем меньше GUI, тем быстрее"}:(btnName.equals("Advanced OpenGL")?new String[]{"Обнаружение и отображение только видимой геометрии", "  Вкл. - отображается вся геометрия (медленно)", "  Быстро - отображается только видимая геометрия (очень быстро)", "  Красиво - умеренный, избегает графических артефактов (быстро)", "Опция доступна, если она поддерживается", "графической картой."}:(btnName.equals("Fog")?new String[]{"Тип тумана", "  Быстро - низкое качество тумана", "  Красиво - высокое качество тумана, выглядит хорошо", "  Выкл. - нет тумана, очень быстро", "Опция доступна, если она поддерживается", "графической картой."}:(btnName.equals("Fog Start")?new String[]{"Запуск тумана", "  0.2 - туман появляется непосредственно рядом с игроком", "  0.8 - туман появляется далеко от игрока", "Эта опция обычно не влияет на производительность."}:(btnName.equals("Brightness")?new String[]{"Повышение яркости темных объектов", "  OFF - стандартная яркость", "  100% - максимальная яркость для темных объектов", "Эта опция не изменяет яркости ", "полностью черных объектов"}:(btnName.equals("Chunk Loading")?new String[]{"Загрузка чанков", "  По умолчанию - нестабильный FPS при загрузке чанков", "  Гладко - стабильный FPS", "  Multi-Core - стабильный FPS, загрузка мира в 3 раза быстрее", "Smooth и Multi-Core удаляют затормаживание", "вызванные загрузкой чанков.", "Multi-Core может ускорить загрузку мира до 3х раз", "увеличив FPS с помощью использования других ядер компьютера."}:null)))))))))));   }

   private String getButtonName(String displayString) {
      int pos = displayString.indexOf(58);
      return pos < 0?displayString:displayString.substring(0, pos);
   }

   private GuiButton getSelectedButton(int i, int j) {
      for(int k = 0; k < super.buttonList.size(); ++k) {
         GuiButton btn = (GuiButton)super.buttonList.get(k);
         boolean flag = i >= btn.xPosition && j >= btn.yPosition && i < btn.xPosition + btn.width && j < btn.yPosition + btn.height;
         if(flag) {
            return btn;
         }
      }

      return null;
   }

}
