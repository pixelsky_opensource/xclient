package net.minecraft.client.renderer;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.lang.reflect.Field;
import java.nio.FloatBuffer;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.GuiDownloadTerrain;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.particle.EffectRenderer;
import net.minecraft.client.particle.EntityRainFX;
import net.minecraft.client.particle.EntitySmokeFX;
import net.minecraft.client.renderer.ActiveRenderInfo;
import net.minecraft.client.renderer.CallableMouseLocation;
import net.minecraft.client.renderer.CallableScreenName;
import net.minecraft.client.renderer.CallableScreenSize;
import net.minecraft.client.renderer.GLAllocation;
import net.minecraft.client.renderer.ItemRenderer;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.culling.ClippingHelperImpl;
import net.minecraft.client.renderer.culling.Frustrum;
import net.minecraft.crash.CrashReport;
import net.minecraft.crash.CrashReportCategory;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.boss.BossStatus;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.potion.Potion;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraft.src.Config;
import net.minecraft.src.CustomColorizer;
import net.minecraft.src.RandomMobs;
import net.minecraft.src.Reflector;
import net.minecraft.src.WrUpdates;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MouseFilter;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.ReportedException;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.biome.BiomeGenBase;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.GLU;

public class EntityRenderer {

   public static boolean anaglyphEnable = false;
   public static int anaglyphField;
   private Minecraft mc;
   private float farPlaneDistance = 0.0F;
   public ItemRenderer itemRenderer;
   private int rendererUpdateCount;
   private Entity pointedEntity = null;
   private MouseFilter mouseFilterXAxis = new MouseFilter();
   private MouseFilter mouseFilterYAxis = new MouseFilter();
   private MouseFilter mouseFilterDummy1 = new MouseFilter();
   private MouseFilter mouseFilterDummy2 = new MouseFilter();
   private MouseFilter mouseFilterDummy3 = new MouseFilter();
   private MouseFilter mouseFilterDummy4 = new MouseFilter();
   private float thirdPersonDistance = 4.0F;
   private float thirdPersonDistanceTemp = 4.0F;
   private float debugCamYaw = 0.0F;
   private float prevDebugCamYaw = 0.0F;
   private float debugCamPitch = 0.0F;
   private float prevDebugCamPitch = 0.0F;
   private float smoothCamYaw;
   private float smoothCamPitch;
   private float smoothCamFilterX;
   private float smoothCamFilterY;
   private float smoothCamPartialTicks;
   private float debugCamFOV = 0.0F;
   private float prevDebugCamFOV = 0.0F;
   private float camRoll = 0.0F;
   private float prevCamRoll = 0.0F;
   public int lightmapTexture;
   private int[] lightmapColors;
   private float fovModifierHand;
   private float fovModifierHandPrev;
   private float fovMultiplierTemp;
   private float field_82831_U;
   private float field_82832_V;
   private boolean cloudFog = false;
   private double cameraZoom = 1.0D;
   private double cameraYaw = 0.0D;
   private double cameraPitch = 0.0D;
   private long prevFrameTime = Minecraft.getSystemTime();
   private long renderEndNanoTime = 0L;
   private boolean lightmapUpdateNeeded = false;
   public float torchFlickerX = 0.0F;
   float torchFlickerDX = 0.0F;
   float torchFlickerY = 0.0F;
   float torchFlickerDY = 0.0F;
   private Random random = new Random();
   private int rainSoundCounter = 0;
   float[] rainXCoords;
   float[] rainYCoords;
   volatile int field_78523_k = 0;
   volatile int field_78520_l = 0;
   FloatBuffer fogColorBuffer = GLAllocation.createDirectFloatBuffer(16);
   float fogColorRed;
   float fogColorGreen;
   float fogColorBlue;
   private float fogColor2;
   private float fogColor1;
   public int debugViewDirection;
   private World updatedWorld = null;
   private boolean showDebugInfo = false;
   private boolean fullscreenModeChecked = false;
   private boolean desktopModeChecked = false;
   private String lastTexturePack = null;
   private long lastServerTime = 0L;
   private int lastServerTicks = 0;
   private int serverWaitTime = 0;
   private int serverWaitTimeCurrent = 0;
   private float avgServerTimeDiff = 0.0F;
   private float avgServerTickDiff = 0.0F;
   public long[] frameTimes = new long[512];
   public long[] tickTimes = new long[512];
   public long[] chunkTimes = new long[512];
   public long[] serverTimes = new long[512];
   public int numRecordedFrameTimes = 0;
   public long prevFrameTimeNano = -1L;
   private boolean lastShowDebugInfo = false;
   private boolean showExtendedDebugInfo = false;


   public EntityRenderer(Minecraft par1Minecraft) {
      this.mc = par1Minecraft;
      this.itemRenderer = new ItemRenderer(par1Minecraft);
      this.lightmapTexture = par1Minecraft.renderEngine.allocateAndSetupTexture(new BufferedImage(16, 16, 1));
      this.lightmapColors = new int[256];
   }

   public void updateRenderer() {
      this.updateFovModifierHand();
      this.updateTorchFlicker();
      this.fogColor2 = this.fogColor1;
      this.thirdPersonDistanceTemp = this.thirdPersonDistance;
      this.prevDebugCamYaw = this.debugCamYaw;
      this.prevDebugCamPitch = this.debugCamPitch;
      this.prevDebugCamFOV = this.debugCamFOV;
      this.prevCamRoll = this.camRoll;
      float var1;
      float var2;
      if(this.mc.gameSettings.smoothCamera) {
         var1 = this.mc.gameSettings.mouseSensitivity * 0.6F + 0.2F;
         var2 = var1 * var1 * var1 * 8.0F;
         this.smoothCamFilterX = this.mouseFilterXAxis.smooth(this.smoothCamYaw, 0.05F * var2);
         this.smoothCamFilterY = this.mouseFilterYAxis.smooth(this.smoothCamPitch, 0.05F * var2);
         this.smoothCamPartialTicks = 0.0F;
         this.smoothCamYaw = 0.0F;
         this.smoothCamPitch = 0.0F;
      }

      if(this.mc.renderViewEntity == null) {
         this.mc.renderViewEntity = this.mc.thePlayer;
      }

      var1 = this.mc.theWorld.getLightBrightness(MathHelper.floor_double(this.mc.renderViewEntity.posX), MathHelper.floor_double(this.mc.renderViewEntity.posY), MathHelper.floor_double(this.mc.renderViewEntity.posZ));
      var2 = (float)(3 - this.mc.gameSettings.renderDistance) / 3.0F;
      float var3 = var1 * (1.0F - var2) + var2;
      this.fogColor1 += (var3 - this.fogColor1) * 0.1F;
      ++this.rendererUpdateCount;
      this.itemRenderer.updateEquippedItem();
      this.addRainParticles();
      this.field_82832_V = this.field_82831_U;
      if(BossStatus.field_82825_d) {
         this.field_82831_U += 0.05F;
         if(this.field_82831_U > 1.0F) {
            this.field_82831_U = 1.0F;
         }

         BossStatus.field_82825_d = false;
      } else if(this.field_82831_U > 0.0F) {
         this.field_82831_U -= 0.0125F;
      }

   }

   public void getMouseOver(float par1) {
      if(this.mc.renderViewEntity != null && this.mc.theWorld != null) {
         this.mc.pointedEntityLiving = null;
         double var2 = (double)this.mc.playerController.getBlockReachDistance();
         this.mc.objectMouseOver = this.mc.renderViewEntity.rayTrace(var2, par1);
         double var4 = var2;
         Vec3 var6 = this.mc.renderViewEntity.getPosition(par1);
         if(this.mc.playerController.extendedReach()) {
            var2 = 6.0D;
            var4 = 6.0D;
         } else {
            if(var2 > 3.0D) {
               var4 = 3.0D;
            }

            var2 = var4;
         }

         if(this.mc.objectMouseOver != null) {
            var4 = this.mc.objectMouseOver.hitVec.distanceTo(var6);
         }

         Vec3 var7 = this.mc.renderViewEntity.getLook(par1);
         Vec3 var8 = var6.addVector(var7.xCoord * var2, var7.yCoord * var2, var7.zCoord * var2);
         this.pointedEntity = null;
         float var9 = 1.0F;
         List var10 = this.mc.theWorld.getEntitiesWithinAABBExcludingEntity(this.mc.renderViewEntity, this.mc.renderViewEntity.boundingBox.addCoord(var7.xCoord * var2, var7.yCoord * var2, var7.zCoord * var2).expand((double)var9, (double)var9, (double)var9));
         double var11 = var4;

         for(int var13 = 0; var13 < var10.size(); ++var13) {
            Entity var14 = (Entity)var10.get(var13);
            if(var14.canBeCollidedWith()) {
               float var15 = var14.getCollisionBorderSize();
               AxisAlignedBB var16 = var14.boundingBox.expand((double)var15, (double)var15, (double)var15);
               MovingObjectPosition var17 = var16.calculateIntercept(var6, var8);
               if(var16.isVecInside(var6)) {
                  if(0.0D < var11 || var11 == 0.0D) {
                     this.pointedEntity = var14;
                     var11 = 0.0D;
                  }
               } else if(var17 != null) {
                  double var18 = var6.distanceTo(var17.hitVec);
                  if(var18 < var11 || var11 == 0.0D) {
                     this.pointedEntity = var14;
                     var11 = var18;
                  }
               }
            }
         }

         if(this.pointedEntity != null && (var11 < var4 || this.mc.objectMouseOver == null)) {
            this.mc.objectMouseOver = new MovingObjectPosition(this.pointedEntity);
            if(this.pointedEntity instanceof EntityLiving) {
               this.mc.pointedEntityLiving = (EntityLiving)this.pointedEntity;
            }
         }
      }

   }

   private void updateFovModifierHand() {
      if(this.mc.renderViewEntity instanceof EntityPlayerSP) {
         EntityPlayerSP var1 = (EntityPlayerSP)this.mc.renderViewEntity;
         this.fovMultiplierTemp = var1.getFOVMultiplier();
      } else {
         this.fovMultiplierTemp = this.mc.thePlayer.getFOVMultiplier();
      }

      this.fovModifierHandPrev = this.fovModifierHand;
      this.fovModifierHand += (this.fovMultiplierTemp - this.fovModifierHand) * 0.5F;
      if(this.fovModifierHand > 1.5F) {
         this.fovModifierHand = 1.5F;
      }

      if(this.fovModifierHand < 0.1F) {
         this.fovModifierHand = 0.1F;
      }

   }

   private float getFOVModifier(float par1, boolean par2) {
      if(this.debugViewDirection > 0) {
         return 90.0F;
      } else {
         EntityLiving var3 = this.mc.renderViewEntity;
         float var4 = 70.0F;
         if(par2) {
            var4 += this.mc.gameSettings.fovSetting * 40.0F;
            var4 *= this.fovModifierHandPrev + (this.fovModifierHand - this.fovModifierHandPrev) * par1;
         }

         boolean zoomActive = false;
         if(this.mc.currentScreen == null) {
            if(this.mc.gameSettings.ofKeyBindZoom.keyCode < 0) {
               zoomActive = Mouse.isButtonDown(this.mc.gameSettings.ofKeyBindZoom.keyCode + 100);
            } else {
               zoomActive = Keyboard.isKeyDown(this.mc.gameSettings.ofKeyBindZoom.keyCode);
            }
         }

         if(zoomActive) {
            if(!Config.zoomMode) {
               Config.zoomMode = true;
               this.mc.gameSettings.smoothCamera = true;
            }

            if(Config.zoomMode) {
               var4 /= 4.0F;
            }
         } else if(Config.zoomMode) {
            Config.zoomMode = false;
            this.mc.gameSettings.smoothCamera = false;
            this.mouseFilterXAxis = new MouseFilter();
            this.mouseFilterYAxis = new MouseFilter();
         }

         if(var3.getHealth() <= 0) {
            float var6 = (float)var3.deathTime + par1;
            var4 /= (1.0F - 500.0F / (var6 + 500.0F)) * 2.0F + 1.0F;
         }

         int var61 = ActiveRenderInfo.getBlockIdAtEntityViewpoint(this.mc.theWorld, var3, par1);
         if(var61 != 0 && Block.blocksList[var61].blockMaterial == Material.water) {
            var4 = var4 * 60.0F / 70.0F;
         }

         return var4 + this.prevDebugCamFOV + (this.debugCamFOV - this.prevDebugCamFOV) * par1;
      }
   }

   private void hurtCameraEffect(float par1) {
      EntityLiving var2 = this.mc.renderViewEntity;
      float var3 = (float)var2.hurtTime - par1;
      float var4;
      if(var2.getHealth() <= 0) {
         var4 = (float)var2.deathTime + par1;
         GL11.glRotatef(40.0F - 8000.0F / (var4 + 200.0F), 0.0F, 0.0F, 1.0F);
      }

      if(var3 >= 0.0F) {
         var3 /= (float)var2.maxHurtTime;
         var3 = MathHelper.sin(var3 * var3 * var3 * var3 * 3.1415927F);
         var4 = var2.attackedAtYaw;
         GL11.glRotatef(-var4, 0.0F, 1.0F, 0.0F);
         GL11.glRotatef(-var3 * 14.0F, 0.0F, 0.0F, 1.0F);
         GL11.glRotatef(var4, 0.0F, 1.0F, 0.0F);
      }

   }

   private void setupViewBobbing(float par1) {
      if(this.mc.renderViewEntity instanceof EntityPlayer) {
         EntityPlayer var2 = (EntityPlayer)this.mc.renderViewEntity;
         float var3 = var2.distanceWalkedModified - var2.prevDistanceWalkedModified;
         float var4 = -(var2.distanceWalkedModified + var3 * par1);
         float var5 = var2.prevCameraYaw + (var2.cameraYaw - var2.prevCameraYaw) * par1;
         float var6 = var2.prevCameraPitch + (var2.cameraPitch - var2.prevCameraPitch) * par1;
         GL11.glTranslatef(MathHelper.sin(var4 * 3.1415927F) * var5 * 0.5F, -Math.abs(MathHelper.cos(var4 * 3.1415927F) * var5), 0.0F);
         GL11.glRotatef(MathHelper.sin(var4 * 3.1415927F) * var5 * 3.0F, 0.0F, 0.0F, 1.0F);
         GL11.glRotatef(Math.abs(MathHelper.cos(var4 * 3.1415927F - 0.2F) * var5) * 5.0F, 1.0F, 0.0F, 0.0F);
         GL11.glRotatef(var6, 1.0F, 0.0F, 0.0F);
      }

   }

   private void orientCamera(float par1) {
      EntityLiving var2 = this.mc.renderViewEntity;
      float var3 = var2.yOffset - 1.62F;
      double var4 = var2.prevPosX + (var2.posX - var2.prevPosX) * (double)par1;
      double var6 = var2.prevPosY + (var2.posY - var2.prevPosY) * (double)par1 - (double)var3;
      double var8 = var2.prevPosZ + (var2.posZ - var2.prevPosZ) * (double)par1;
      GL11.glRotatef(this.prevCamRoll + (this.camRoll - this.prevCamRoll) * par1, 0.0F, 0.0F, 1.0F);
      if(var2.isPlayerSleeping()) {
         var3 = (float)((double)var3 + 1.0D);
         GL11.glTranslatef(0.0F, 0.3F, 0.0F);
         if(!this.mc.gameSettings.debugCamEnable) {
            int var27 = this.mc.theWorld.getBlockId(MathHelper.floor_double(var2.posX), MathHelper.floor_double(var2.posY), MathHelper.floor_double(var2.posZ));
            if(Reflector.ForgeHooksClient_orientBedCamera.exists()) {
               Reflector.callVoid(Reflector.ForgeHooksClient_orientBedCamera, new Object[]{this.mc, var2});
            } else if(var27 == Block.bed.blockID) {
               int var11 = this.mc.theWorld.getBlockMetadata(MathHelper.floor_double(var2.posX), MathHelper.floor_double(var2.posY), MathHelper.floor_double(var2.posZ));
               int var13 = var11 & 3;
               GL11.glRotatef((float)(var13 * 90), 0.0F, 1.0F, 0.0F);
            }

            GL11.glRotatef(var2.prevRotationYaw + (var2.rotationYaw - var2.prevRotationYaw) * par1 + 180.0F, 0.0F, -1.0F, 0.0F);
            GL11.glRotatef(var2.prevRotationPitch + (var2.rotationPitch - var2.prevRotationPitch) * par1, -1.0F, 0.0F, 0.0F);
         }
      } else if(this.mc.gameSettings.thirdPersonView > 0) {
         double var271 = (double)(this.thirdPersonDistanceTemp + (this.thirdPersonDistance - this.thirdPersonDistanceTemp) * par1);
         float var28;
         float var281;
         if(this.mc.gameSettings.debugCamEnable) {
            var28 = this.prevDebugCamYaw + (this.debugCamYaw - this.prevDebugCamYaw) * par1;
            var281 = this.prevDebugCamPitch + (this.debugCamPitch - this.prevDebugCamPitch) * par1;
            GL11.glTranslatef(0.0F, 0.0F, (float)(-var271));
            GL11.glRotatef(var281, 1.0F, 0.0F, 0.0F);
            GL11.glRotatef(var28, 0.0F, 1.0F, 0.0F);
         } else {
            var28 = var2.rotationYaw;
            var281 = var2.rotationPitch;
            if(this.mc.gameSettings.thirdPersonView == 2) {
               var281 += 180.0F;
            }

            double var14 = (double)(-MathHelper.sin(var28 / 180.0F * 3.1415927F) * MathHelper.cos(var281 / 180.0F * 3.1415927F)) * var271;
            double var16 = (double)(MathHelper.cos(var28 / 180.0F * 3.1415927F) * MathHelper.cos(var281 / 180.0F * 3.1415927F)) * var271;
            double var18 = (double)(-MathHelper.sin(var281 / 180.0F * 3.1415927F)) * var271;

            for(int var20 = 0; var20 < 8; ++var20) {
               float var21 = (float)((var20 & 1) * 2 - 1);
               float var22 = (float)((var20 >> 1 & 1) * 2 - 1);
               float var23 = (float)((var20 >> 2 & 1) * 2 - 1);
               var21 *= 0.1F;
               var22 *= 0.1F;
               var23 *= 0.1F;
               MovingObjectPosition var24 = this.mc.theWorld.rayTraceBlocks(this.mc.theWorld.getWorldVec3Pool().getVecFromPool(var4 + (double)var21, var6 + (double)var22, var8 + (double)var23), this.mc.theWorld.getWorldVec3Pool().getVecFromPool(var4 - var14 + (double)var21 + (double)var23, var6 - var18 + (double)var22, var8 - var16 + (double)var23));
               if(var24 != null) {
                  double var25 = var24.hitVec.distanceTo(this.mc.theWorld.getWorldVec3Pool().getVecFromPool(var4, var6, var8));
                  if(var25 < var271) {
                     var271 = var25;
                  }
               }
            }

            if(this.mc.gameSettings.thirdPersonView == 2) {
               GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
            }

            GL11.glRotatef(var2.rotationPitch - var281, 1.0F, 0.0F, 0.0F);
            GL11.glRotatef(var2.rotationYaw - var28, 0.0F, 1.0F, 0.0F);
            GL11.glTranslatef(0.0F, 0.0F, (float)(-var271));
            GL11.glRotatef(var28 - var2.rotationYaw, 0.0F, 1.0F, 0.0F);
            GL11.glRotatef(var281 - var2.rotationPitch, 1.0F, 0.0F, 0.0F);
         }
      } else {
         GL11.glTranslatef(0.0F, 0.0F, -0.1F);
      }

      if(!this.mc.gameSettings.debugCamEnable) {
         GL11.glRotatef(var2.prevRotationPitch + (var2.rotationPitch - var2.prevRotationPitch) * par1, 1.0F, 0.0F, 0.0F);
         GL11.glRotatef(var2.prevRotationYaw + (var2.rotationYaw - var2.prevRotationYaw) * par1 + 180.0F, 0.0F, 1.0F, 0.0F);
      }

      GL11.glTranslatef(0.0F, var3, 0.0F);
      var4 = var2.prevPosX + (var2.posX - var2.prevPosX) * (double)par1;
      var6 = var2.prevPosY + (var2.posY - var2.prevPosY) * (double)par1 - (double)var3;
      var8 = var2.prevPosZ + (var2.posZ - var2.prevPosZ) * (double)par1;
      this.cloudFog = this.mc.renderGlobal.hasCloudFog(var4, var6, var8, par1);
   }

   private void setupCameraTransform(float par1, int par2) {
      this.farPlaneDistance = (float)(32 << 3 - this.mc.gameSettings.renderDistance);
      this.farPlaneDistance = (float)this.mc.gameSettings.ofRenderDistanceFine;
      if(Config.isFogFancy()) {
         this.farPlaneDistance *= 0.95F;
      }

      if(Config.isFogFast()) {
         this.farPlaneDistance *= 0.83F;
      }

      GL11.glMatrixMode(5889);
      GL11.glLoadIdentity();
      float var3 = 0.07F;
      if(this.mc.gameSettings.anaglyph) {
         GL11.glTranslatef((float)(-(par2 * 2 - 1)) * var3, 0.0F, 0.0F);
      }

      float clipDistance = this.farPlaneDistance * 2.0F;
      if(clipDistance < 128.0F) {
         clipDistance = 128.0F;
      }

      if(this.cameraZoom != 1.0D) {
         GL11.glTranslatef((float)this.cameraYaw, (float)(-this.cameraPitch), 0.0F);
         GL11.glScaled(this.cameraZoom, this.cameraZoom, 1.0D);
      }

      GLU.gluPerspective(this.getFOVModifier(par1, true), (float)this.mc.displayWidth / (float)this.mc.displayHeight, 0.05F, clipDistance);
      float var4;
      if(this.mc.playerController.enableEverythingIsScrewedUpMode()) {
         var4 = 0.6666667F;
         GL11.glScalef(1.0F, var4, 1.0F);
      }

      GL11.glMatrixMode(5888);
      GL11.glLoadIdentity();
      if(this.mc.gameSettings.anaglyph) {
         GL11.glTranslatef((float)(par2 * 2 - 1) * 0.1F, 0.0F, 0.0F);
      }

      this.hurtCameraEffect(par1);
      if(this.mc.gameSettings.viewBobbing) {
         this.setupViewBobbing(par1);
      }

      var4 = this.mc.thePlayer.prevTimeInPortal + (this.mc.thePlayer.timeInPortal - this.mc.thePlayer.prevTimeInPortal) * par1;
      if(var4 > 0.0F) {
         byte var7 = 20;
         if(this.mc.thePlayer.isPotionActive(Potion.confusion)) {
            var7 = 7;
         }

         float var6 = 5.0F / (var4 * var4 + 5.0F) - var4 * 0.04F;
         var6 *= var6;
         GL11.glRotatef(((float)this.rendererUpdateCount + par1) * (float)var7, 0.0F, 1.0F, 1.0F);
         GL11.glScalef(1.0F / var6, 1.0F, 1.0F);
         GL11.glRotatef(-((float)this.rendererUpdateCount + par1) * (float)var7, 0.0F, 1.0F, 1.0F);
      }

      this.orientCamera(par1);
      if(this.debugViewDirection > 0) {
         int var71 = this.debugViewDirection - 1;
         if(var71 == 1) {
            GL11.glRotatef(90.0F, 0.0F, 1.0F, 0.0F);
         }

         if(var71 == 2) {
            GL11.glRotatef(180.0F, 0.0F, 1.0F, 0.0F);
         }

         if(var71 == 3) {
            GL11.glRotatef(-90.0F, 0.0F, 1.0F, 0.0F);
         }

         if(var71 == 4) {
            GL11.glRotatef(90.0F, 1.0F, 0.0F, 0.0F);
         }

         if(var71 == 5) {
            GL11.glRotatef(-90.0F, 1.0F, 0.0F, 0.0F);
         }
      }

   }

   private void renderHand(float par1, int par2) {
      if(this.debugViewDirection <= 0) {
         GL11.glMatrixMode(5889);
         GL11.glLoadIdentity();
         float var3 = 0.07F;
         if(this.mc.gameSettings.anaglyph) {
            GL11.glTranslatef((float)(-(par2 * 2 - 1)) * var3, 0.0F, 0.0F);
         }

         if(this.cameraZoom != 1.0D) {
            GL11.glTranslatef((float)this.cameraYaw, (float)(-this.cameraPitch), 0.0F);
            GL11.glScaled(this.cameraZoom, this.cameraZoom, 1.0D);
         }

         GLU.gluPerspective(this.getFOVModifier(par1, false), (float)this.mc.displayWidth / (float)this.mc.displayHeight, 0.05F, this.farPlaneDistance * 2.0F);
         if(this.mc.playerController.enableEverythingIsScrewedUpMode()) {
            float var4 = 0.6666667F;
            GL11.glScalef(1.0F, var4, 1.0F);
         }

         GL11.glMatrixMode(5888);
         GL11.glLoadIdentity();
         if(this.mc.gameSettings.anaglyph) {
            GL11.glTranslatef((float)(par2 * 2 - 1) * 0.1F, 0.0F, 0.0F);
         }

         GL11.glPushMatrix();
         this.hurtCameraEffect(par1);
         if(this.mc.gameSettings.viewBobbing) {
            this.setupViewBobbing(par1);
         }

         if(this.mc.gameSettings.thirdPersonView == 0 && !this.mc.renderViewEntity.isPlayerSleeping() && !this.mc.gameSettings.hideGUI && !this.mc.playerController.enableEverythingIsScrewedUpMode()) {
            this.enableLightmap((double)par1);
            this.itemRenderer.renderItemInFirstPerson(par1);
            this.disableLightmap((double)par1);
         }

         GL11.glPopMatrix();
         if(this.mc.gameSettings.thirdPersonView == 0 && !this.mc.renderViewEntity.isPlayerSleeping()) {
            this.itemRenderer.renderOverlays(par1);
            this.hurtCameraEffect(par1);
         }

         if(this.mc.gameSettings.viewBobbing) {
            this.setupViewBobbing(par1);
         }
      }

   }

   public void disableLightmap(double par1) {
      OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
      GL11.glDisable(3553);
      OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
   }

   public void enableLightmap(double par1) {
      OpenGlHelper.setActiveTexture(OpenGlHelper.lightmapTexUnit);
      GL11.glMatrixMode(5890);
      GL11.glLoadIdentity();
      float var3 = 0.00390625F;
      GL11.glScalef(var3, var3, var3);
      GL11.glTranslatef(8.0F, 8.0F, 8.0F);
      GL11.glMatrixMode(5888);
      GL11.glBindTexture(3553, this.lightmapTexture);
      GL11.glTexParameteri(3553, 10241, 9729);
      GL11.glTexParameteri(3553, 10240, 9729);
      GL11.glTexParameteri(3553, 10242, 10496);
      GL11.glTexParameteri(3553, 10243, 10496);
      GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
      GL11.glEnable(3553);
      this.mc.renderEngine.resetBoundTexture();
      OpenGlHelper.setActiveTexture(OpenGlHelper.defaultTexUnit);
   }

   private void updateTorchFlicker() {
      this.torchFlickerDX = (float)((double)this.torchFlickerDX + (Math.random() - Math.random()) * Math.random() * Math.random());
      this.torchFlickerDY = (float)((double)this.torchFlickerDY + (Math.random() - Math.random()) * Math.random() * Math.random());
      this.torchFlickerDX = (float)((double)this.torchFlickerDX * 0.9D);
      this.torchFlickerDY = (float)((double)this.torchFlickerDY * 0.9D);
      this.torchFlickerX += (this.torchFlickerDX - this.torchFlickerX) * 1.0F;
      this.torchFlickerY += (this.torchFlickerDY - this.torchFlickerY) * 1.0F;
      this.lightmapUpdateNeeded = true;
   }

   private void updateLightmap(float par1) {
      WorldClient var2 = this.mc.theWorld;
      if(var2 != null) {
         if(CustomColorizer.updateLightmap(var2, this, this.lightmapColors, this.mc.thePlayer.isPotionActive(Potion.nightVision))) {
            this.mc.renderEngine.createTextureFromBytes(this.lightmapColors, 16, 16, this.lightmapTexture);
            return;
         }

         for(int var3 = 0; var3 < 256; ++var3) {
            float var4 = var2.getSunBrightness(1.0F) * 0.95F + 0.05F;
            float var5 = var2.provider.lightBrightnessTable[var3 / 16] * var4;
            float var6 = var2.provider.lightBrightnessTable[var3 % 16] * (this.torchFlickerX * 0.1F + 1.5F);
            if(var2.lastLightningBolt > 0) {
               var5 = var2.provider.lightBrightnessTable[var3 / 16];
            }

            float var7 = var5 * (var2.getSunBrightness(1.0F) * 0.65F + 0.35F);
            float var8 = var5 * (var2.getSunBrightness(1.0F) * 0.65F + 0.35F);
            float var11 = var6 * ((var6 * 0.6F + 0.4F) * 0.6F + 0.4F);
            float var12 = var6 * (var6 * var6 * 0.6F + 0.4F);
            float var13 = var7 + var6;
            float var14 = var8 + var11;
            float var15 = var5 + var12;
            var13 = var13 * 0.96F + 0.03F;
            var14 = var14 * 0.96F + 0.03F;
            var15 = var15 * 0.96F + 0.03F;
            float var16;
            if(this.field_82831_U > 0.0F) {
               var16 = this.field_82832_V + (this.field_82831_U - this.field_82832_V) * par1;
               var13 = var13 * (1.0F - var16) + var13 * 0.7F * var16;
               var14 = var14 * (1.0F - var16) + var14 * 0.6F * var16;
               var15 = var15 * (1.0F - var16) + var15 * 0.6F * var16;
            }

            if(var2.provider.dimensionId == 1) {
               var13 = 0.22F + var6 * 0.75F;
               var14 = 0.28F + var11 * 0.75F;
               var15 = 0.25F + var12 * 0.75F;
            }

            float var17;
            if(this.mc.thePlayer.isPotionActive(Potion.nightVision)) {
               var16 = this.getNightVisionBrightness(this.mc.thePlayer, par1);
               var17 = 1.0F / var13;
               if(var17 > 1.0F / var14) {
                  var17 = 1.0F / var14;
               }

               if(var17 > 1.0F / var15) {
                  var17 = 1.0F / var15;
               }

               var13 = var13 * (1.0F - var16) + var13 * var17 * var16;
               var14 = var14 * (1.0F - var16) + var14 * var17 * var16;
               var15 = var15 * (1.0F - var16) + var15 * var17 * var16;
            }

            if(var13 > 1.0F) {
               var13 = 1.0F;
            }

            if(var14 > 1.0F) {
               var14 = 1.0F;
            }

            if(var15 > 1.0F) {
               var15 = 1.0F;
            }

            var16 = this.mc.gameSettings.gammaSetting;
            var17 = 1.0F - var13;
            float var18 = 1.0F - var14;
            float var19 = 1.0F - var15;
            var17 = 1.0F - var17 * var17 * var17 * var17;
            var18 = 1.0F - var18 * var18 * var18 * var18;
            var19 = 1.0F - var19 * var19 * var19 * var19;
            var13 = var13 * (1.0F - var16) + var17 * var16;
            var14 = var14 * (1.0F - var16) + var18 * var16;
            var15 = var15 * (1.0F - var16) + var19 * var16;
            var13 = var13 * 0.96F + 0.03F;
            var14 = var14 * 0.96F + 0.03F;
            var15 = var15 * 0.96F + 0.03F;
            if(var13 > 1.0F) {
               var13 = 1.0F;
            }

            if(var14 > 1.0F) {
               var14 = 1.0F;
            }

            if(var15 > 1.0F) {
               var15 = 1.0F;
            }

            if(var13 < 0.0F) {
               var13 = 0.0F;
            }

            if(var14 < 0.0F) {
               var14 = 0.0F;
            }

            if(var15 < 0.0F) {
               var15 = 0.0F;
            }

            short var20 = 255;
            int var21 = (int)(var13 * 255.0F);
            int var22 = (int)(var14 * 255.0F);
            int var23 = (int)(var15 * 255.0F);
            this.lightmapColors[var3] = var20 << 24 | var21 << 16 | var22 << 8 | var23;
         }

         this.mc.renderEngine.createTextureFromBytes(this.lightmapColors, 16, 16, this.lightmapTexture);
      }

   }

   private float getNightVisionBrightness(EntityPlayer par1EntityPlayer, float par2) {
      int var3 = par1EntityPlayer.getActivePotionEffect(Potion.nightVision).getDuration();
      return var3 > 200?1.0F:0.7F + MathHelper.sin(((float)var3 - par2) * 3.1415927F * 0.2F) * 0.3F;
   }

   public void updateCameraAndRender(float par1) {
      this.mc.mcProfiler.startSection("lightTex");
      WorldClient world = this.mc.theWorld;
      this.checkDisplayMode();
      if(world != null && Config.getNewRelease() != null) {
         String var2 = "HD_U " + Config.getNewRelease();
         this.mc.ingameGUI.getChatGUI().printChatMessage("A new §eOptiFine§f version is available: §e" + var2 + "§f");
         Config.setNewRelease((String)null);
      }

      if(this.mc.currentScreen instanceof GuiMainMenu) {
         this.updateMainMenu((GuiMainMenu)this.mc.currentScreen);
      }

      if(this.updatedWorld != world) {
         RandomMobs.worldChanged(this.updatedWorld, world);
         Config.updateThreadPriorities();
         this.lastServerTime = 0L;
         this.lastServerTicks = 0;
         this.updatedWorld = world;
      }

      if(this.lastTexturePack == null) {
         this.lastTexturePack = this.mc.texturePackList.getSelectedTexturePack().getTexturePackFileName();
      }

      if(!this.lastTexturePack.equals(this.mc.texturePackList.getSelectedTexturePack().getTexturePackFileName())) {
         this.mc.renderGlobal.loadRenderers();
         this.lastTexturePack = this.mc.texturePackList.getSelectedTexturePack().getTexturePackFileName();
      }

      RenderBlocks.fancyGrass = Config.isGrassFancy() || Config.isBetterGrassFancy();
      Block.leaves.setGraphicsLevel(Config.isTreesFancy());
      if(this.lightmapUpdateNeeded) {
         this.updateLightmap(par1);
      }

      this.mc.mcProfiler.endSection();
      boolean var21 = Display.isActive();
      if(!var21 && this.mc.gameSettings.pauseOnLostFocus && (!this.mc.gameSettings.touchscreen || !Mouse.isButtonDown(1))) {
         if(Minecraft.getSystemTime() - this.prevFrameTime > 500L) {
            this.mc.displayInGameMenu();
         }
      } else {
         this.prevFrameTime = Minecraft.getSystemTime();
      }

      this.mc.mcProfiler.startSection("mouse");
      if(this.mc.inGameHasFocus && var21) {
         this.mc.mouseHelper.mouseXYChange();
         float var13 = this.mc.gameSettings.mouseSensitivity * 0.6F + 0.2F;
         float var14 = var13 * var13 * var13 * 8.0F;
         float var15 = (float)this.mc.mouseHelper.deltaX * var14;
         float var16 = (float)this.mc.mouseHelper.deltaY * var14;
         byte var17 = 1;
         if(this.mc.gameSettings.invertMouse) {
            var17 = -1;
         }

         if(this.mc.gameSettings.smoothCamera) {
            this.smoothCamYaw += var15;
            this.smoothCamPitch += var16;
            float var18 = par1 - this.smoothCamPartialTicks;
            this.smoothCamPartialTicks = par1;
            var15 = this.smoothCamFilterX * var18;
            var16 = this.smoothCamFilterY * var18;
            this.mc.thePlayer.setAngles(var15, var16 * (float)var17);
         } else {
            this.mc.thePlayer.setAngles(var15, var16 * (float)var17);
         }
      }

      this.mc.mcProfiler.endSection();
      if(!this.mc.skipRenderWorld) {
         anaglyphEnable = this.mc.gameSettings.anaglyph;
         ScaledResolution var132 = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
         int var141 = var132.getScaledWidth();
         int var151 = var132.getScaledHeight();
         int var161 = Mouse.getX() * var141 / this.mc.displayWidth;
         int var171 = var151 - Mouse.getY() * var151 / this.mc.displayHeight - 1;
         int var181 = performanceToFps(this.mc.gameSettings.limitFramerate);
         if(this.mc.theWorld != null) {
            this.mc.mcProfiler.startSection("level");
            if(this.mc.gameSettings.limitFramerate == 0) {
               this.renderWorld(par1, 0L);
            } else {
               this.renderWorld(par1, this.renderEndNanoTime + (long)(1000000000 / var181));
            }

            this.renderEndNanoTime = System.nanoTime();
            this.mc.mcProfiler.endStartSection("gui");
            if(!this.mc.gameSettings.hideGUI || this.mc.currentScreen != null) {
               this.mc.ingameGUI.renderGameOverlay(par1, this.mc.currentScreen != null, var161, var171);
            }

            this.mc.mcProfiler.endSection();
         } else {
            GL11.glViewport(0, 0, this.mc.displayWidth, this.mc.displayHeight);
            GL11.glMatrixMode(5889);
            GL11.glLoadIdentity();
            GL11.glMatrixMode(5888);
            GL11.glLoadIdentity();
            this.setupOverlayRendering();
            this.renderEndNanoTime = System.nanoTime();
         }

         if(this.mc.currentScreen != null) {
            GL11.glClear(256);

            try {
               this.mc.currentScreen.drawScreen(var161, var171, par1);
            } catch (Throwable var131) {
               CrashReport var10 = CrashReport.makeCrashReport(var131, "Rendering screen");
               CrashReportCategory var11 = var10.makeCategory("Screen render details");
               var11.addCrashSectionCallable("Screen name", new CallableScreenName(this));
               var11.addCrashSectionCallable("Mouse location", new CallableMouseLocation(this, var161, var171));
               var11.addCrashSectionCallable("Screen size", new CallableScreenSize(this, var132));
               throw new ReportedException(var10);
            }

            if(this.mc.currentScreen != null && this.mc.currentScreen.guiParticles != null) {
               this.mc.currentScreen.guiParticles.draw(par1);
            }
         }
      }

      this.waitForServerThread();
      if(this.mc.gameSettings.showDebugInfo != this.lastShowDebugInfo) {
         this.showExtendedDebugInfo = this.mc.gameSettings.showDebugProfilerChart;
         this.lastShowDebugInfo = this.mc.gameSettings.showDebugInfo;
      }

      if(this.mc.gameSettings.showDebugInfo) {
         this.showLagometer(this.mc.mcProfiler.timeTickNano, this.mc.mcProfiler.timeUpdateChunksNano);
      }

      if(this.mc.gameSettings.ofProfiler) {
         this.mc.gameSettings.showDebugProfilerChart = true;
      }

   }

   private void waitForServerThread() {
      this.serverWaitTimeCurrent = 0;
      if(!Config.isSmoothWorld()) {
         this.lastServerTime = 0L;
         this.lastServerTicks = 0;
      } else if(this.mc.getIntegratedServer() != null) {
         IntegratedServer srv = this.mc.getIntegratedServer();
         boolean paused = srv.getServerListeningThread().isGamePaused();
         if(paused) {
            if(this.mc.currentScreen instanceof GuiDownloadTerrain) {
               Config.sleep(20L);
            }

            this.lastServerTime = 0L;
            this.lastServerTicks = 0;
         } else {
            if(this.serverWaitTime > 0) {
               Config.sleep((long)this.serverWaitTime);
               this.serverWaitTimeCurrent = this.serverWaitTime;
            }

            long timeNow = System.nanoTime() / 1000000L;
            if(this.lastServerTime != 0L && this.lastServerTicks != 0) {
               long timeDiff = timeNow - this.lastServerTime;
               if(timeDiff < 0L) {
                  this.lastServerTime = timeNow;
                  timeDiff = 0L;
               }

               if(timeDiff >= 50L) {
                  this.lastServerTime = timeNow;
                  int ticks = srv.getTickCounter();
                  int tickDiff = ticks - this.lastServerTicks;
                  if(tickDiff < 0) {
                     this.lastServerTicks = ticks;
                     tickDiff = 0;
                  }

                  if(tickDiff < 1 && this.serverWaitTime < 100) {
                     this.serverWaitTime += 2;
                  }

                  if(tickDiff > 1 && this.serverWaitTime > 0) {
                     --this.serverWaitTime;
                  }

                  this.lastServerTicks = ticks;
               }
            } else {
               this.lastServerTime = timeNow;
               this.lastServerTicks = srv.getTickCounter();
               this.avgServerTickDiff = 1.0F;
               this.avgServerTimeDiff = 50.0F;
            }
         }
      }
   }

   private void showLagometer(long tickTimeNano, long chunkTimeNano) {
      if(this.mc.gameSettings.ofLagometer || this.showExtendedDebugInfo) {
         if(this.prevFrameTimeNano == -1L) {
            this.prevFrameTimeNano = System.nanoTime();
         }

         long timeNowNano = System.nanoTime();
         int currFrameIndex = this.numRecordedFrameTimes & this.frameTimes.length - 1;
         this.tickTimes[currFrameIndex] = tickTimeNano;
         this.chunkTimes[currFrameIndex] = chunkTimeNano;
         this.serverTimes[currFrameIndex] = (long)this.serverWaitTimeCurrent;
         this.frameTimes[currFrameIndex] = timeNowNano - this.prevFrameTimeNano;
         ++this.numRecordedFrameTimes;
         this.prevFrameTimeNano = timeNowNano;
         GL11.glClear(256);
         GL11.glMatrixMode(5889);
         GL11.glEnable(2903);
         GL11.glLoadIdentity();
         GL11.glOrtho(0.0D, (double)this.mc.displayWidth, (double)this.mc.displayHeight, 0.0D, 1000.0D, 3000.0D);
         GL11.glMatrixMode(5888);
         GL11.glLoadIdentity();
         GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
         GL11.glLineWidth(1.0F);
         GL11.glDisable(3553);
         Tessellator tessellator = Tessellator.instance;
         tessellator.startDrawing(1);

         for(int frameNum = 0; frameNum < this.frameTimes.length; ++frameNum) {
            int lum = (frameNum - this.numRecordedFrameTimes & this.frameTimes.length - 1) * 255 / this.frameTimes.length;
            long heightFrame = this.frameTimes[frameNum] / 200000L;
            float baseHeight = (float)this.mc.displayHeight;
            tessellator.setColorOpaque_I(-16777216 + lum * 256);
            tessellator.addVertex((double)((float)frameNum + 0.5F), (double)(baseHeight - (float)heightFrame + 0.5F), 0.0D);
            tessellator.addVertex((double)((float)frameNum + 0.5F), (double)(baseHeight + 0.5F), 0.0D);
            baseHeight -= (float)heightFrame;
            long heightTick = this.tickTimes[frameNum] / 200000L;
            tessellator.setColorOpaque_I(-16777216 + lum * 65536 + lum * 256 + lum * 1);
            tessellator.addVertex((double)((float)frameNum + 0.5F), (double)(baseHeight + 0.5F), 0.0D);
            tessellator.addVertex((double)((float)frameNum + 0.5F), (double)(baseHeight + (float)heightTick + 0.5F), 0.0D);
            baseHeight += (float)heightTick;
            long heightChunk = this.chunkTimes[frameNum] / 200000L;
            tessellator.setColorOpaque_I(-16777216 + lum * 65536);
            tessellator.addVertex((double)((float)frameNum + 0.5F), (double)(baseHeight + 0.5F), 0.0D);
            tessellator.addVertex((double)((float)frameNum + 0.5F), (double)(baseHeight + (float)heightChunk + 0.5F), 0.0D);
            baseHeight += (float)heightChunk;
            long srvTime = this.serverTimes[frameNum];
            if(srvTime > 0L) {
               long heightSrv = srvTime * 1000000L / 200000L;
               tessellator.setColorOpaque_I(-16777216 + lum * 1);
               tessellator.addVertex((double)((float)frameNum + 0.5F), (double)(baseHeight + 0.5F), 0.0D);
               tessellator.addVertex((double)((float)frameNum + 0.5F), (double)(baseHeight + (float)heightSrv + 0.5F), 0.0D);
            }
         }

         tessellator.draw();
      }
   }

   private void updateMainMenu(GuiMainMenu mainGui) {
      try {
         String e = null;
         Calendar calendar = Calendar.getInstance();
         calendar.setTime(new Date());
         int day = calendar.get(5);
         int month = calendar.get(2) + 1;
         if(day == 8 && month == 4) {
            e = "Happy birthday, OptiFine!";
         }

         if(day == 14 && month == 8) {
            e = "Happy birthday, sp614x!";
         }

         if(e == null) {
            return;
         }

         Field[] fs = GuiMainMenu.class.getDeclaredFields();

         for(int i = 0; i < fs.length; ++i) {
            if(fs[i].getType() == String.class) {
               fs[i].setAccessible(true);
               fs[i].set(mainGui, e);
               break;
            }
         }
      } catch (Throwable var8) {
         ;
      }

   }

   private void checkDisplayMode() {
      try {
         DisplayMode e;
         if(Display.isFullscreen()) {
            if(this.fullscreenModeChecked) {
               return;
            }

            this.fullscreenModeChecked = true;
            this.desktopModeChecked = false;
            e = Display.getDisplayMode();
            Dimension dim = Config.getFullscreenDimension();
            if(dim == null) {
               return;
            }

            if(e.getWidth() == dim.width && e.getHeight() == dim.height) {
               return;
            }

            DisplayMode newMode = Config.getDisplayMode(dim);
            Display.setDisplayMode(newMode);
            this.mc.displayWidth = Display.getDisplayMode().getWidth();
            this.mc.displayHeight = Display.getDisplayMode().getHeight();
            if(this.mc.displayWidth <= 0) {
               this.mc.displayWidth = 1;
            }

            if(this.mc.displayHeight <= 0) {
               this.mc.displayHeight = 1;
            }

            Display.setFullscreen(true);
            this.mc.gameSettings.updateVSync();
            Display.update();
            GL11.glEnable(3553);
         } else {
            if(this.desktopModeChecked) {
               return;
            }

            this.desktopModeChecked = true;
            this.fullscreenModeChecked = false;
            if(Config.getDesktopDisplayMode() == null) {
               Config.setDesktopDisplayMode(Display.getDesktopDisplayMode());
            }

            e = Display.getDisplayMode();
            if(e.equals(Config.getDesktopDisplayMode())) {
               return;
            }

            Display.setDisplayMode(Config.getDesktopDisplayMode());
            if(this.mc.mcCanvas != null) {
               this.mc.displayWidth = this.mc.mcCanvas.getWidth();
               this.mc.displayHeight = this.mc.mcCanvas.getHeight();
            }

            if(this.mc.displayWidth <= 0) {
               this.mc.displayWidth = 1;
            }

            if(this.mc.displayHeight <= 0) {
               this.mc.displayHeight = 1;
            }

            Display.setFullscreen(false);
            this.mc.gameSettings.updateVSync();
            Display.update();
            GL11.glEnable(3553);
         }
      } catch (Exception var4) {
         var4.printStackTrace();
      }

   }

   public void renderWorld(float par1, long par2) {
      this.mc.mcProfiler.startSection("lightTex");
      if(this.lightmapUpdateNeeded) {
         this.updateLightmap(par1);
      }

      GL11.glEnable(2884);
      GL11.glEnable(2929);
      if(this.mc.renderViewEntity == null) {
         this.mc.renderViewEntity = this.mc.thePlayer;
      }

      this.mc.mcProfiler.endStartSection("pick");
      this.getMouseOver(par1);
      EntityLiving var4 = this.mc.renderViewEntity;
      RenderGlobal var5 = this.mc.renderGlobal;
      EffectRenderer var6 = this.mc.effectRenderer;
      double var7 = var4.lastTickPosX + (var4.posX - var4.lastTickPosX) * (double)par1;
      double var9 = var4.lastTickPosY + (var4.posY - var4.lastTickPosY) * (double)par1;
      double var11 = var4.lastTickPosZ + (var4.posZ - var4.lastTickPosZ) * (double)par1;
      this.mc.mcProfiler.endStartSection("center");

      for(int var13 = 0; var13 < 2; ++var13) {
         if(this.mc.gameSettings.anaglyph) {
            anaglyphField = var13;
            if(anaglyphField == 0) {
               GL11.glColorMask(false, true, true, false);
            } else {
               GL11.glColorMask(true, false, false, false);
            }
         }

         this.mc.mcProfiler.endStartSection("clear");
         GL11.glViewport(0, 0, this.mc.displayWidth, this.mc.displayHeight);
         this.updateFogColor(par1);
         GL11.glClear(16640);
         GL11.glEnable(2884);
         this.mc.mcProfiler.endStartSection("camera");
         this.setupCameraTransform(par1, var13);
         ActiveRenderInfo.updateRenderInfo(this.mc.thePlayer, this.mc.gameSettings.thirdPersonView == 2);
         this.mc.mcProfiler.endStartSection("frustrum");
         ClippingHelperImpl.getInstance();
         if(!Config.isSkyEnabled() && !Config.isSunMoonEnabled() && !Config.isStarsEnabled()) {
            GL11.glDisable(3042);
         } else {
            this.setupFog(-1, par1);
            this.mc.mcProfiler.endStartSection("sky");
            var5.renderSky(par1);
         }

         GL11.glEnable(2912);
         this.setupFog(1, par1);
         if(this.mc.gameSettings.ambientOcclusion != 0) {
            GL11.glShadeModel(7425);
         }

         this.mc.mcProfiler.endStartSection("culling");
         Frustrum var14 = new Frustrum();
         var14.setPosition(var7, var9, var11);
         this.mc.renderGlobal.clipRenderersByFrustum(var14, par1);
         if(var13 == 0) {
            this.mc.mcProfiler.endStartSection("updatechunks");

            while(!this.mc.renderGlobal.updateRenderers(var4, false) && par2 != 0L) {
               long var17 = par2 - System.nanoTime();
               if(var17 < 0L || var17 > 1000000000L) {
                  break;
               }
            }
         }

         if(var4.posY < 128.0D) {
            this.renderCloudsCheck(var5, par1);
         }

         this.mc.mcProfiler.endStartSection("prepareterrain");
         this.setupFog(0, par1);
         GL11.glEnable(2912);
         this.mc.renderEngine.bindTexture("/terrain.png");
         RenderHelper.disableStandardItemLighting();
         this.mc.mcProfiler.endStartSection("terrain");
         var5.sortAndRender(var4, 0, (double)par1);
         GL11.glShadeModel(7424);
         boolean hasForge = Reflector.ForgeHooksClient.exists();
         EntityPlayer var18;
         if(this.debugViewDirection == 0) {
            RenderHelper.enableStandardItemLighting();
            this.mc.mcProfiler.endStartSection("entities");
            if(hasForge) {
               Reflector.callVoid(Reflector.ForgeHooksClient_setRenderPass, new Object[]{Integer.valueOf(0)});
            }

            var5.renderEntities(var4.getPosition(par1), var14, par1);
            if(hasForge) {
               Reflector.callVoid(Reflector.ForgeHooksClient_setRenderPass, new Object[]{Integer.valueOf(-1)});
            }

            this.enableLightmap((double)par1);
            this.mc.mcProfiler.endStartSection("litParticles");
            var6.renderLitParticles(var4, par1);
            RenderHelper.disableStandardItemLighting();
            this.setupFog(0, par1);
            this.mc.mcProfiler.endStartSection("particles");
            var6.renderParticles(var4, par1);
            this.disableLightmap((double)par1);
            if(this.mc.objectMouseOver != null && var4.isInsideOfMaterial(Material.water) && var4 instanceof EntityPlayer && !this.mc.gameSettings.hideGUI) {
               var18 = (EntityPlayer)var4;
               GL11.glDisable(3008);
               this.mc.mcProfiler.endStartSection("outline");
               if(!hasForge || !Reflector.callBoolean(Reflector.ForgeHooksClient_onDrawBlockHighlight, new Object[]{var5, var18, this.mc.objectMouseOver, Integer.valueOf(0), var18.inventory.getCurrentItem(), Float.valueOf(par1)})) {
                  var5.drawBlockBreaking(var18, this.mc.objectMouseOver, 0, var18.inventory.getCurrentItem(), par1);
                  if(!this.mc.gameSettings.hideGUI) {
                     var5.drawSelectionBox(var18, this.mc.objectMouseOver, 0, var18.inventory.getCurrentItem(), par1);
                  }
               }

               GL11.glEnable(3008);
            }
         }

         GL11.glDisable(3042);
         GL11.glEnable(2884);
         GL11.glBlendFunc(770, 771);
         GL11.glDepthMask(true);
         this.setupFog(0, par1);
         GL11.glEnable(3042);
         GL11.glDisable(2884);
         this.mc.renderEngine.bindTexture("/terrain.png");
         WrUpdates.resumeBackgroundUpdates();
         if(Config.isWaterFancy()) {
            this.mc.mcProfiler.endStartSection("water");
            if(this.mc.gameSettings.ambientOcclusion != 0) {
               GL11.glShadeModel(7425);
            }

            GL11.glColorMask(false, false, false, false);
            int num = var5.renderAllSortedRenderers(1, (double)par1);
            if(this.mc.gameSettings.anaglyph) {
               if(anaglyphField == 0) {
                  GL11.glColorMask(false, true, true, true);
               } else {
                  GL11.glColorMask(true, false, false, true);
               }
            } else {
               GL11.glColorMask(true, true, true, true);
            }

            if(num > 0) {
               var5.renderAllSortedRenderers(1, (double)par1);
            }

            GL11.glShadeModel(7424);
         } else {
            this.mc.mcProfiler.endStartSection("water");
            var5.renderAllSortedRenderers(1, (double)par1);
         }

         WrUpdates.pauseBackgroundUpdates();
         GL11.glDepthMask(true);
         GL11.glEnable(2884);
         GL11.glDisable(3042);
         if(this.cameraZoom == 1.0D && var4 instanceof EntityPlayer && !this.mc.gameSettings.hideGUI && this.mc.objectMouseOver != null && !var4.isInsideOfMaterial(Material.water)) {
            var18 = (EntityPlayer)var4;
            GL11.glDisable(3008);
            this.mc.mcProfiler.endStartSection("outline");
            if(!hasForge || !Reflector.callBoolean(Reflector.ForgeHooksClient_onDrawBlockHighlight, new Object[]{var5, var18, this.mc.objectMouseOver, Integer.valueOf(0), var18.inventory.getCurrentItem(), Float.valueOf(par1)})) {
               var5.drawBlockBreaking(var18, this.mc.objectMouseOver, 0, var18.inventory.getCurrentItem(), par1);
               if(!this.mc.gameSettings.hideGUI) {
                  var5.drawSelectionBox(var18, this.mc.objectMouseOver, 0, var18.inventory.getCurrentItem(), par1);
               }
            }

            GL11.glEnable(3008);
         }

         this.mc.mcProfiler.endStartSection("destroyProgress");
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 1);
         var5.drawBlockDamageTexture(Tessellator.instance, var4, par1);
         GL11.glDisable(3042);
         this.mc.mcProfiler.endStartSection("weather");
         this.renderRainSnow(par1);
         GL11.glDisable(2912);
         if(var4.posY >= 128.0D) {
            this.renderCloudsCheck(var5, par1);
         }

         if(hasForge) {
            this.mc.mcProfiler.endStartSection("FRenderLast");
            Reflector.callVoid(Reflector.ForgeHooksClient_dispatchRenderLast, new Object[]{var5, Float.valueOf(par1)});
         }

         this.mc.mcProfiler.endStartSection("hand");
         if(this.cameraZoom == 1.0D) {
            GL11.glClear(256);
            this.renderHand(par1, var13);
         }

         if(!this.mc.gameSettings.anaglyph) {
            this.mc.mcProfiler.endSection();
            return;
         }
      }

      GL11.glColorMask(true, true, true, false);
      this.mc.mcProfiler.endSection();
   }

   private void renderCloudsCheck(RenderGlobal par1RenderGlobal, float par2) {
      if(this.mc.gameSettings.shouldRenderClouds()) {
         this.mc.mcProfiler.endStartSection("clouds");
         GL11.glPushMatrix();
         this.setupFog(0, par2);
         GL11.glEnable(2912);
         par1RenderGlobal.renderClouds(par2);
         GL11.glDisable(2912);
         this.setupFog(1, par2);
         GL11.glPopMatrix();
      }

   }

   private void addRainParticles() {
      float var1 = this.mc.theWorld.getRainStrength(1.0F);
      if(!Config.isRainFancy()) {
         var1 /= 2.0F;
      }

      if(Config.isRainSplash()) {
         this.random.setSeed((long)this.rendererUpdateCount * 312987231L);
         EntityLiving var2 = this.mc.renderViewEntity;
         WorldClient var3 = this.mc.theWorld;
         int var4 = MathHelper.floor_double(var2.posX);
         int var5 = MathHelper.floor_double(var2.posY);
         int var6 = MathHelper.floor_double(var2.posZ);
         byte var7 = 10;
         double var8 = 0.0D;
         double var10 = 0.0D;
         double var12 = 0.0D;
         int var14 = 0;
         int var15 = (int)(100.0F * var1 * var1);
         if(this.mc.gameSettings.particleSetting == 1) {
            var15 >>= 1;
         } else if(this.mc.gameSettings.particleSetting == 2) {
            var15 = 0;
         }

         for(int var16 = 0; var16 < var15; ++var16) {
            int var17 = var4 + this.random.nextInt(var7) - this.random.nextInt(var7);
            int var18 = var6 + this.random.nextInt(var7) - this.random.nextInt(var7);
            int var19 = var3.getPrecipitationHeight(var17, var18);
            int var20 = var3.getBlockId(var17, var19 - 1, var18);
            BiomeGenBase var21 = var3.getBiomeGenForCoords(var17, var18);
            if(var19 <= var5 + var7 && var19 >= var5 - var7 && var21.canSpawnLightningBolt() && var21.getFloatTemperature() >= 0.2F) {
               float var22 = this.random.nextFloat();
               float var23 = this.random.nextFloat();
               if(var20 > 0) {
                  if(Block.blocksList[var20].blockMaterial == Material.lava) {
                     this.mc.effectRenderer.addEffect(new EntitySmokeFX(var3, (double)((float)var17 + var22), (double)((float)var19 + 0.1F) - Block.blocksList[var20].getBlockBoundsMinY(), (double)((float)var18 + var23), 0.0D, 0.0D, 0.0D));
                  } else {
                     ++var14;
                     if(this.random.nextInt(var14) == 0) {
                        var8 = (double)((float)var17 + var22);
                        var10 = (double)((float)var19 + 0.1F) - Block.blocksList[var20].getBlockBoundsMinY();
                        var12 = (double)((float)var18 + var23);
                     }

                     EntityRainFX fx = new EntityRainFX(var3, (double)((float)var17 + var22), (double)((float)var19 + 0.1F) - Block.blocksList[var20].getBlockBoundsMinY(), (double)((float)var18 + var23));
                     CustomColorizer.updateWaterFX(fx, var3);
                     this.mc.effectRenderer.addEffect(fx);
                  }
               }
            }
         }

         if(var14 > 0 && this.random.nextInt(3) < this.rainSoundCounter++) {
            this.rainSoundCounter = 0;
            if(var10 > var2.posY + 1.0D && var3.getPrecipitationHeight(MathHelper.floor_double(var2.posX), MathHelper.floor_double(var2.posZ)) > MathHelper.floor_double(var2.posY)) {
               this.mc.theWorld.playSound(var8, var10, var12, "ambient.weather.rain", 0.1F, 0.5F, false);
            } else {
               this.mc.theWorld.playSound(var8, var10, var12, "ambient.weather.rain", 0.2F, 1.0F, false);
            }
         }
      }

   }

   protected void renderRainSnow(float par1) {
      float var2 = this.mc.theWorld.getRainStrength(par1);
      if(var2 > 0.0F) {
         this.enableLightmap((double)par1);
         if(this.rainXCoords == null) {
            this.rainXCoords = new float[1024];
            this.rainYCoords = new float[1024];

            for(int var41 = 0; var41 < 32; ++var41) {
               for(int var42 = 0; var42 < 32; ++var42) {
                  float var43 = (float)(var42 - 16);
                  float var44 = (float)(var41 - 16);
                  float var45 = MathHelper.sqrt_float(var43 * var43 + var44 * var44);
                  this.rainXCoords[var41 << 5 | var42] = -var44 / var45;
                  this.rainYCoords[var41 << 5 | var42] = var43 / var45;
               }
            }
         }

         if(Config.isRainOff()) {
            return;
         }

         EntityLiving var411 = this.mc.renderViewEntity;
         WorldClient var421 = this.mc.theWorld;
         int var431 = MathHelper.floor_double(var411.posX);
         int var441 = MathHelper.floor_double(var411.posY);
         int var451 = MathHelper.floor_double(var411.posZ);
         Tessellator var8 = Tessellator.instance;
         GL11.glDisable(2884);
         GL11.glNormal3f(0.0F, 1.0F, 0.0F);
         GL11.glEnable(3042);
         GL11.glBlendFunc(770, 771);
         GL11.glAlphaFunc(516, 0.01F);
         this.mc.renderEngine.bindTexture("/environment/snow.png");
         double var9 = var411.lastTickPosX + (var411.posX - var411.lastTickPosX) * (double)par1;
         double var11 = var411.lastTickPosY + (var411.posY - var411.lastTickPosY) * (double)par1;
         double var13 = var411.lastTickPosZ + (var411.posZ - var411.lastTickPosZ) * (double)par1;
         int var15 = MathHelper.floor_double(var11);
         byte var16 = 5;
         if(Config.isRainFancy()) {
            var16 = 10;
         }

         boolean var17 = false;
         byte var18 = -1;
         float var19 = (float)this.rendererUpdateCount + par1;
         if(Config.isRainFancy()) {
            var16 = 10;
         }

         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         var17 = false;

         for(int var20 = var451 - var16; var20 <= var451 + var16; ++var20) {
            for(int var21 = var431 - var16; var21 <= var431 + var16; ++var21) {
               int var22 = (var20 - var451 + 16) * 32 + var21 - var431 + 16;
               float var23 = this.rainXCoords[var22] * 0.5F;
               float var24 = this.rainYCoords[var22] * 0.5F;
               BiomeGenBase var25 = var421.getBiomeGenForCoords(var21, var20);
               if(var25.canSpawnLightningBolt() || var25.getEnableSnow()) {
                  int var26 = var421.getPrecipitationHeight(var21, var20);
                  int var27 = var441 - var16;
                  int var28 = var441 + var16;
                  if(var27 < var26) {
                     var27 = var26;
                  }

                  if(var28 < var26) {
                     var28 = var26;
                  }

                  float var29 = 1.0F;
                  int var30 = var26;
                  if(var26 < var15) {
                     var30 = var15;
                  }

                  if(var27 != var28) {
                     this.random.setSeed((long)(var21 * var21 * 3121 + var21 * 45238971 ^ var20 * var20 * 418711 + var20 * 13761));
                     float var31 = var25.getFloatTemperature();
                     float var32;
                     double var35;
                     if(var421.getWorldChunkManager().getTemperatureAtHeight(var31, var26) >= 0.15F) {
                        if(var18 != 0) {
                           if(var18 >= 0) {
                              var8.draw();
                           }

                           var18 = 0;
                           this.mc.renderEngine.bindTexture("/environment/rain.png");
                           var8.startDrawingQuads();
                        }

                        var32 = ((float)(this.rendererUpdateCount + var21 * var21 * 3121 + var21 * 45238971 + var20 * var20 * 418711 + var20 * 13761 & 31) + par1) / 32.0F * (3.0F + this.random.nextFloat());
                        double var46 = (double)((float)var21 + 0.5F) - var411.posX;
                        var35 = (double)((float)var20 + 0.5F) - var411.posZ;
                        float var47 = MathHelper.sqrt_double(var46 * var46 + var35 * var35) / (float)var16;
                        float var38 = 1.0F;
                        var8.setBrightness(var421.getLightBrightnessForSkyBlocks(var21, var30, var20, 0));
                        var8.setColorRGBA_F(var38, var38, var38, ((1.0F - var47 * var47) * 0.5F + 0.5F) * var2);
                        var8.setTranslation(-var9 * 1.0D, -var11 * 1.0D, -var13 * 1.0D);
                        var8.addVertexWithUV((double)((float)var21 - var23) + 0.5D, (double)var27, (double)((float)var20 - var24) + 0.5D, (double)(0.0F * var29), (double)((float)var27 * var29 / 4.0F + var32 * var29));
                        var8.addVertexWithUV((double)((float)var21 + var23) + 0.5D, (double)var27, (double)((float)var20 + var24) + 0.5D, (double)(1.0F * var29), (double)((float)var27 * var29 / 4.0F + var32 * var29));
                        var8.addVertexWithUV((double)((float)var21 + var23) + 0.5D, (double)var28, (double)((float)var20 + var24) + 0.5D, (double)(1.0F * var29), (double)((float)var28 * var29 / 4.0F + var32 * var29));
                        var8.addVertexWithUV((double)((float)var21 - var23) + 0.5D, (double)var28, (double)((float)var20 - var24) + 0.5D, (double)(0.0F * var29), (double)((float)var28 * var29 / 4.0F + var32 * var29));
                        var8.setTranslation(0.0D, 0.0D, 0.0D);
                     } else {
                        if(var18 != 1) {
                           if(var18 >= 0) {
                              var8.draw();
                           }

                           var18 = 1;
                           this.mc.renderEngine.bindTexture("/environment/snow.png");
                           var8.startDrawingQuads();
                        }

                        var32 = ((float)(this.rendererUpdateCount & 511) + par1) / 512.0F;
                        float var461 = this.random.nextFloat() + var19 * 0.01F * (float)this.random.nextGaussian();
                        float var34 = this.random.nextFloat() + var19 * (float)this.random.nextGaussian() * 0.001F;
                        var35 = (double)((float)var21 + 0.5F) - var411.posX;
                        double var471 = (double)((float)var20 + 0.5F) - var411.posZ;
                        float var39 = MathHelper.sqrt_double(var35 * var35 + var471 * var471) / (float)var16;
                        float var40 = 1.0F;
                        var8.setBrightness((var421.getLightBrightnessForSkyBlocks(var21, var30, var20, 0) * 3 + 15728880) / 4);
                        var8.setColorRGBA_F(var40, var40, var40, ((1.0F - var39 * var39) * 0.3F + 0.5F) * var2);
                        var8.setTranslation(-var9 * 1.0D, -var11 * 1.0D, -var13 * 1.0D);
                        var8.addVertexWithUV((double)((float)var21 - var23) + 0.5D, (double)var27, (double)((float)var20 - var24) + 0.5D, (double)(0.0F * var29 + var461), (double)((float)var27 * var29 / 4.0F + var32 * var29 + var34));
                        var8.addVertexWithUV((double)((float)var21 + var23) + 0.5D, (double)var27, (double)((float)var20 + var24) + 0.5D, (double)(1.0F * var29 + var461), (double)((float)var27 * var29 / 4.0F + var32 * var29 + var34));
                        var8.addVertexWithUV((double)((float)var21 + var23) + 0.5D, (double)var28, (double)((float)var20 + var24) + 0.5D, (double)(1.0F * var29 + var461), (double)((float)var28 * var29 / 4.0F + var32 * var29 + var34));
                        var8.addVertexWithUV((double)((float)var21 - var23) + 0.5D, (double)var28, (double)((float)var20 - var24) + 0.5D, (double)(0.0F * var29 + var461), (double)((float)var28 * var29 / 4.0F + var32 * var29 + var34));
                        var8.setTranslation(0.0D, 0.0D, 0.0D);
                     }
                  }
               }
            }
         }

         if(var18 >= 0) {
            var8.draw();
         }

         GL11.glEnable(2884);
         GL11.glDisable(3042);
         GL11.glAlphaFunc(516, 0.1F);
         this.disableLightmap((double)par1);
      }

   }

   public void setupOverlayRendering() {
      ScaledResolution var1 = new ScaledResolution(this.mc.gameSettings, this.mc.displayWidth, this.mc.displayHeight);
      GL11.glClear(256);
      GL11.glMatrixMode(5889);
      GL11.glLoadIdentity();
      GL11.glOrtho(0.0D, var1.getScaledWidth_double(), var1.getScaledHeight_double(), 0.0D, 1000.0D, 3000.0D);
      GL11.glMatrixMode(5888);
      GL11.glLoadIdentity();
      GL11.glTranslatef(0.0F, 0.0F, -2000.0F);
   }

   private void updateFogColor(float par1) {
      WorldClient var2 = this.mc.theWorld;
      EntityLiving var3 = this.mc.renderViewEntity;
      float var4 = 1.0F / (float)(4 - this.mc.gameSettings.renderDistance);
      var4 = 1.0F - (float)Math.pow((double)var4, 0.25D);
      Vec3 var5 = var2.getSkyColor(this.mc.renderViewEntity, par1);
      int worldType = var2.provider.dimensionId;
      switch(worldType) {
      case 0:
         var5 = CustomColorizer.getSkyColor(var5, this.mc.theWorld, this.mc.renderViewEntity.posX, this.mc.renderViewEntity.posY + 1.0D, this.mc.renderViewEntity.posZ);
         break;
      case 1:
         var5 = CustomColorizer.getSkyColorEnd(var5);
      }

      float var6 = (float)var5.xCoord;
      float var7 = (float)var5.yCoord;
      float var8 = (float)var5.zCoord;
      Vec3 var9 = var2.getFogColor(par1);
      switch(worldType) {
      case -1:
         var9 = CustomColorizer.getFogColorNether(var9);
         break;
      case 0:
         var9 = CustomColorizer.getFogColor(var9, this.mc.theWorld, this.mc.renderViewEntity.posX, this.mc.renderViewEntity.posY + 1.0D, this.mc.renderViewEntity.posZ);
         break;
      case 1:
         var9 = CustomColorizer.getFogColorEnd(var9);
      }

      this.fogColorRed = (float)var9.xCoord;
      this.fogColorGreen = (float)var9.yCoord;
      this.fogColorBlue = (float)var9.zCoord;
      float var11;
      if(this.mc.gameSettings.renderDistance < 2) {
         Vec3 var19 = MathHelper.sin(var2.getCelestialAngleRadians(par1)) > 0.0F?var2.getWorldVec3Pool().getVecFromPool(-1.0D, 0.0D, 0.0D):var2.getWorldVec3Pool().getVecFromPool(1.0D, 0.0D, 0.0D);
         var11 = (float)var3.getLook(par1).dotProduct(var19);
         if(var11 < 0.0F) {
            var11 = 0.0F;
         }

         if(var11 > 0.0F) {
            float[] var20 = var2.provider.calcSunriseSunsetColors(var2.getCelestialAngle(par1), par1);
            if(var20 != null) {
               var11 *= var20[3];
               this.fogColorRed = this.fogColorRed * (1.0F - var11) + var20[0] * var11;
               this.fogColorGreen = this.fogColorGreen * (1.0F - var11) + var20[1] * var11;
               this.fogColorBlue = this.fogColorBlue * (1.0F - var11) + var20[2] * var11;
            }
         }
      }

      this.fogColorRed += (var6 - this.fogColorRed) * var4;
      this.fogColorGreen += (var7 - this.fogColorGreen) * var4;
      this.fogColorBlue += (var8 - this.fogColorBlue) * var4;
      float var191 = var2.getRainStrength(par1);
      float var201;
      if(var191 > 0.0F) {
         var11 = 1.0F - var191 * 0.5F;
         var201 = 1.0F - var191 * 0.4F;
         this.fogColorRed *= var11;
         this.fogColorGreen *= var11;
         this.fogColorBlue *= var201;
      }

      var11 = var2.getWeightedThunderStrength(par1);
      if(var11 > 0.0F) {
         var201 = 1.0F - var11 * 0.5F;
         this.fogColorRed *= var201;
         this.fogColorGreen *= var201;
         this.fogColorBlue *= var201;
      }

      int var21 = ActiveRenderInfo.getBlockIdAtEntityViewpoint(this.mc.theWorld, var3, par1);
      Vec3 var22;
      if(this.cloudFog) {
         var22 = var2.getCloudColour(par1);
         this.fogColorRed = (float)var22.xCoord;
         this.fogColorGreen = (float)var22.yCoord;
         this.fogColorBlue = (float)var22.zCoord;
      } else if(var21 != 0 && Block.blocksList[var21].blockMaterial == Material.water) {
         this.fogColorRed = 0.02F;
         this.fogColorGreen = 0.02F;
         this.fogColorBlue = 0.2F;
         var22 = CustomColorizer.getUnderwaterColor(this.mc.theWorld, this.mc.renderViewEntity.posX, this.mc.renderViewEntity.posY + 1.0D, this.mc.renderViewEntity.posZ);
         if(var22 != null) {
            this.fogColorRed = (float)var22.xCoord;
            this.fogColorGreen = (float)var22.yCoord;
            this.fogColorBlue = (float)var22.zCoord;
         }
      } else if(var21 != 0 && Block.blocksList[var21].blockMaterial == Material.lava) {
         this.fogColorRed = 0.6F;
         this.fogColorGreen = 0.1F;
         this.fogColorBlue = 0.0F;
      }

      float var221 = this.fogColor2 + (this.fogColor1 - this.fogColor2) * par1;
      this.fogColorRed *= var221;
      this.fogColorGreen *= var221;
      this.fogColorBlue *= var221;
      double fogYFactor = var2.provider.getVoidFogYFactor();
      if(!Config.isDepthFog()) {
         fogYFactor = 1.0D;
      }

      double var14 = (var3.lastTickPosY + (var3.posY - var3.lastTickPosY) * (double)par1) * fogYFactor;
      if(var3.isPotionActive(Potion.blindness)) {
         int var23 = var3.getActivePotionEffect(Potion.blindness).getDuration();
         if(var23 < 20) {
            var14 *= (double)(1.0F - (float)var23 / 20.0F);
         } else {
            var14 = 0.0D;
         }
      }

      if(var14 < 1.0D) {
         if(var14 < 0.0D) {
            var14 = 0.0D;
         }

         var14 *= var14;
         this.fogColorRed = (float)((double)this.fogColorRed * var14);
         this.fogColorGreen = (float)((double)this.fogColorGreen * var14);
         this.fogColorBlue = (float)((double)this.fogColorBlue * var14);
      }

      float var231;
      if(this.field_82831_U > 0.0F) {
         var231 = this.field_82832_V + (this.field_82831_U - this.field_82832_V) * par1;
         this.fogColorRed = this.fogColorRed * (1.0F - var231) + this.fogColorRed * 0.7F * var231;
         this.fogColorGreen = this.fogColorGreen * (1.0F - var231) + this.fogColorGreen * 0.6F * var231;
         this.fogColorBlue = this.fogColorBlue * (1.0F - var231) + this.fogColorBlue * 0.6F * var231;
      }

      float var17;
      if(var3.isPotionActive(Potion.nightVision)) {
         var231 = this.getNightVisionBrightness(this.mc.thePlayer, par1);
         var17 = 1.0F / this.fogColorRed;
         if(var17 > 1.0F / this.fogColorGreen) {
            var17 = 1.0F / this.fogColorGreen;
         }

         if(var17 > 1.0F / this.fogColorBlue) {
            var17 = 1.0F / this.fogColorBlue;
         }

         this.fogColorRed = this.fogColorRed * (1.0F - var231) + this.fogColorRed * var17 * var231;
         this.fogColorGreen = this.fogColorGreen * (1.0F - var231) + this.fogColorGreen * var17 * var231;
         this.fogColorBlue = this.fogColorBlue * (1.0F - var231) + this.fogColorBlue * var17 * var231;
      }

      if(this.mc.gameSettings.anaglyph) {
         var231 = (this.fogColorRed * 30.0F + this.fogColorGreen * 59.0F + this.fogColorBlue * 11.0F) / 100.0F;
         var17 = (this.fogColorRed * 30.0F + this.fogColorGreen * 70.0F) / 100.0F;
         float var18 = (this.fogColorRed * 30.0F + this.fogColorBlue * 70.0F) / 100.0F;
         this.fogColorRed = var231;
         this.fogColorGreen = var17;
         this.fogColorBlue = var18;
      }

      GL11.glClearColor(this.fogColorRed, this.fogColorGreen, this.fogColorBlue, 0.0F);
   }

   private void setupFog(int par1, float par2) {
      EntityLiving var3 = this.mc.renderViewEntity;
      boolean var4 = false;
      if(var3 instanceof EntityPlayer) {
         var4 = ((EntityPlayer)var3).capabilities.isCreativeMode;
      }

      if(par1 == 999) {
         GL11.glFog(2918, this.setFogColorBuffer(0.0F, 0.0F, 0.0F, 1.0F));
         GL11.glFogi(2917, 9729);
         GL11.glFogf(2915, 0.0F);
         GL11.glFogf(2916, 8.0F);
         if(GLContext.getCapabilities().GL_NV_fog_distance) {
            GL11.glFogi('\u855a', '\u855b');
         }

         GL11.glFogf(2915, 0.0F);
      } else {
         GL11.glFog(2918, this.setFogColorBuffer(this.fogColorRed, this.fogColorGreen, this.fogColorBlue, 1.0F));
         GL11.glNormal3f(0.0F, -1.0F, 0.0F);
         GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
         int var5 = ActiveRenderInfo.getBlockIdAtEntityViewpoint(this.mc.theWorld, var3, par2);
         float var6;
         if(var3.isPotionActive(Potion.blindness)) {
            var6 = 5.0F;
            int var8 = var3.getActivePotionEffect(Potion.blindness).getDuration();
            if(var8 < 20) {
               var6 = 5.0F + (this.farPlaneDistance - 5.0F) * (1.0F - (float)var8 / 20.0F);
            }

            GL11.glFogi(2917, 9729);
            if(par1 < 0) {
               GL11.glFogf(2915, 0.0F);
               GL11.glFogf(2916, var6 * 0.8F);
            } else {
               GL11.glFogf(2915, var6 * 0.25F);
               GL11.glFogf(2916, var6);
            }

            if(Config.isFogFancy()) {
               GL11.glFogi('\u855a', '\u855b');
            }
         } else {
            float var9;
            float var10;
            float var11;
            float var12;
            float var81;
            if(this.cloudFog) {
               GL11.glFogi(2917, 2048);
               GL11.glFogf(2914, 0.1F);
               var6 = 1.0F;
               var12 = 1.0F;
               var81 = 1.0F;
               if(this.mc.gameSettings.anaglyph) {
                  var9 = (var6 * 30.0F + var12 * 59.0F + var81 * 11.0F) / 100.0F;
                  var10 = (var6 * 30.0F + var12 * 70.0F) / 100.0F;
                  var11 = (var6 * 30.0F + var81 * 70.0F) / 100.0F;
               }
            } else {
               float fogStart1;
               if(var5 > 0 && Block.blocksList[var5].blockMaterial == Material.water) {
                  GL11.glFogi(2917, 2048);
                  fogStart1 = 0.1F;
                  if(var3.isPotionActive(Potion.waterBreathing)) {
                     fogStart1 = 0.05F;
                  }

                  if(Config.isClearWater()) {
                     fogStart1 /= 5.0F;
                  }

                  GL11.glFogf(2914, fogStart1);
                  var6 = 0.4F;
                  var12 = 0.4F;
                  var81 = 0.9F;
                  if(this.mc.gameSettings.anaglyph) {
                     var9 = (var6 * 30.0F + var12 * 59.0F + var81 * 11.0F) / 100.0F;
                     var10 = (var6 * 30.0F + var12 * 70.0F) / 100.0F;
                     var11 = (var6 * 30.0F + var81 * 70.0F) / 100.0F;
                  }
               } else if(var5 > 0 && Block.blocksList[var5].blockMaterial == Material.lava) {
                  GL11.glFogi(2917, 2048);
                  GL11.glFogf(2914, 2.0F);
                  var6 = 0.4F;
                  var12 = 0.3F;
                  var81 = 0.3F;
                  if(this.mc.gameSettings.anaglyph) {
                     var9 = (var6 * 30.0F + var12 * 59.0F + var81 * 11.0F) / 100.0F;
                     var10 = (var6 * 30.0F + var12 * 70.0F) / 100.0F;
                     var11 = (var6 * 30.0F + var81 * 70.0F) / 100.0F;
                  }
               } else {
                  var6 = this.farPlaneDistance;
                  if(Config.isDepthFog() && this.mc.theWorld.provider.getWorldHasVoidParticles() && !var4) {
                     double fogStart = (double)((var3.getBrightnessForRender(par2) & 15728640) >> 20) / 16.0D + (var3.lastTickPosY + (var3.posY - var3.lastTickPosY) * (double)par2 + 4.0D) / 32.0D;
                     if(fogStart < 1.0D) {
                        if(fogStart < 0.0D) {
                           fogStart = 0.0D;
                        }

                        fogStart *= fogStart;
                        var9 = 100.0F * (float)fogStart;
                        if(var9 < 5.0F) {
                           var9 = 5.0F;
                        }

                        if(var6 > var9) {
                           var6 = var9;
                        }
                     }
                  }

                  GL11.glFogi(2917, 9729);
                  if(GLContext.getCapabilities().GL_NV_fog_distance) {
                     if(Config.isFogFancy()) {
                        GL11.glFogi('\u855a', '\u855b');
                     }

                     if(Config.isFogFast()) {
                        GL11.glFogi('\u855a', '\u855c');
                     }
                  }

                  fogStart1 = Config.getFogStart();
                  float fogEnd = 1.0F;
                  if(par1 < 0) {
                     fogStart1 = 0.0F;
                     fogEnd = 0.8F;
                  }

                  if(this.mc.theWorld.provider.doesXZShowFog((int)var3.posX, (int)var3.posZ)) {
                     fogStart1 = 0.05F;
                     fogEnd = 1.0F;
                     var6 = this.farPlaneDistance;
                  }

                  GL11.glFogf(2915, var6 * fogStart1);
                  GL11.glFogf(2916, var6 * fogEnd);
               }
            }
         }

         GL11.glEnable(2903);
         GL11.glColorMaterial(1028, 4608);
      }

   }

   private FloatBuffer setFogColorBuffer(float par1, float par2, float par3, float par4) {
      this.fogColorBuffer.clear();
      this.fogColorBuffer.put(par1).put(par2).put(par3).put(par4);
      this.fogColorBuffer.flip();
      return this.fogColorBuffer;
   }

   public static int performanceToFps(int par0) {
      Minecraft mc = Config.getMinecraft();
      if(mc.currentScreen != null && mc.currentScreen instanceof GuiMainMenu) {
         return 35;
      } else if(mc.theWorld == null) {
         return 35;
      } else {
         int fpsLimit = Config.getGameSettings().ofLimitFramerateFine;
         if(fpsLimit <= 0) {
            fpsLimit = 10000;
         }

         return fpsLimit;
      }
   }

   static Minecraft getRendererMinecraft(EntityRenderer par0EntityRenderer) {
      return par0EntityRenderer.mc;
   }

}
