package net.minecraft.src;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiSlider;
import net.minecraft.client.gui.GuiSmallButton;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.settings.EnumOptions;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.StringTranslate;

public class GuiPerformanceSettingsOF extends GuiScreen {

   private GuiScreen prevScreen;
   protected String title = "Производительность";
   private GameSettings settings;
   private static EnumOptions[] enumOptions = new EnumOptions[]{EnumOptions.SMOOTH_FPS, EnumOptions.SMOOTH_WORLD, EnumOptions.LOAD_FAR, EnumOptions.PRELOADED_CHUNKS, EnumOptions.CHUNK_UPDATES, EnumOptions.CHUNK_UPDATES_DYNAMIC, EnumOptions.LAZY_CHUNK_LOADING};
   private int lastMouseX = 0;
   private int lastMouseY = 0;
   private long mouseStillTime = 0L;


   public GuiPerformanceSettingsOF(GuiScreen guiscreen, GameSettings gamesettings) {
      this.prevScreen = guiscreen;
      this.settings = gamesettings;
   }

   public void initGui() {
      StringTranslate stringtranslate = StringTranslate.getInstance();
      int i = 0;
      EnumOptions[] aenumoptions = enumOptions;
      int j = aenumoptions.length;

      for(int k = 0; k < j; ++k) {
         EnumOptions enumoptions = aenumoptions[k];
         int x = super.width / 2 - 155 + i % 2 * 160;
         int y = super.height / 6 + 21 * (i / 2) - 10;
         if(!enumoptions.getEnumFloat()) {
            super.buttonList.add(new GuiSmallButton(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions)));
         } else {
            super.buttonList.add(new GuiSlider(enumoptions.returnEnumOrdinal(), x, y, enumoptions, this.settings.getKeyBinding(enumoptions), this.settings.getOptionFloatValue(enumoptions)));
         }

         ++i;
      }

      super.buttonList.add(new GuiButton(200, super.width / 2 - 100, super.height / 6 + 168 + 11, stringtranslate.translateKey("gui.done")));
   }

   protected void actionPerformed(GuiButton guibutton) {
      if(guibutton.enabled) {
         if(guibutton.id < 100 && guibutton instanceof GuiSmallButton) {
            this.settings.setOptionValue(((GuiSmallButton)guibutton).returnEnumOptions(), 1);
            guibutton.displayString = this.settings.getKeyBinding(EnumOptions.getEnumOptions(guibutton.id));
         }

         if(guibutton.id == 200) {
            super.mc.gameSettings.saveOptions();
            super.mc.displayGuiScreen(this.prevScreen);
         }

         if(guibutton.id != EnumOptions.CLOUD_HEIGHT.ordinal()) {
            ScaledResolution scaledresolution = new ScaledResolution(super.mc.gameSettings, super.mc.displayWidth, super.mc.displayHeight);
            int i = scaledresolution.getScaledWidth();
            int j = scaledresolution.getScaledHeight();
            this.setWorldAndResolution(super.mc, i, j);
         }

      }
   }

   public void drawScreen(int x, int y, float f) {
      this.drawDefaultBackground();
      this.drawCenteredString(super.fontRenderer, this.title, super.width / 2, 20, 16777215);
      super.drawScreen(x, y, f);
      if(Math.abs(x - this.lastMouseX) <= 5 && Math.abs(y - this.lastMouseY) <= 5) {
         short activateDelay = 700;
         if(System.currentTimeMillis() >= this.mouseStillTime + (long)activateDelay) {
            int x1 = super.width / 2 - 150;
            int y1 = super.height / 6 - 5;
            if(y <= y1 + 98) {
               y1 += 105;
            }

            int x2 = x1 + 150 + 150;
            int y2 = y1 + 84 + 10;
            GuiButton btn = this.getSelectedButton(x, y);
            if(btn != null) {
               String s = this.getButtonName(btn.displayString);
               String[] lines = this.getTooltipLines(s);
               if(lines == null) {
                  return;
               }

               this.drawGradientRect(x1, y1, x2, y2, -536870912, -536870912);

               for(int i = 0; i < lines.length; ++i) {
                  String line = lines[i];
                  super.fontRenderer.drawStringWithShadow(line, x1 + 5, y1 + 5 + i * 11, 14540253);
               }
            }

         }
      } else {
         this.lastMouseX = x;
         this.lastMouseY = y;
         this.mouseStillTime = System.currentTimeMillis();
      }
   }

   private String[] getTooltipLines(String btnName) {
	   return btnName.equals("Smooth FPS")?new String[]{"Стабилизация FPS с помощью освобождения памяти", "  Выкл. - без стабилизации, FPS может колебаться", "  Вкл. - FPS стабильно", "Эта опция зависит от графического драйвера и эффект", "не всегда виден"}:(btnName.equals("Smooth World")?new String[]{"Избавление от лагов вызванных внутренним сервером.", "  Выкл. - без стабилизации, FPS может колебаться", "  Вкл. - FPS стабильно", "Стабилизирует FPS путем распределения разгрузки внутреннего сервера.", "Работает только на локальных мирах и одноядерных процессорах."}:(btnName.equals("Load Far")?new String[]{"Загрузка мировых чанков на дальней дистанции.", "Переключение рендеринга дистанции не затрагивает все чанки", "они должны быть загружены заново.", "  Выкл. - мировые чанки загружаются на нормальной дистанции", "  Вкл. - мировые чанки загружаются на дальней дистанции, возможно", "       короткая дистанция срабатывания"}:(btnName.equals("Preloaded Chunks")?new String[]{"Определение области в которой чанки не будут загружаться", "  Выкл. - новые чанки будут загружены после 5м", "  2 - новые чанки будут загружены после 32м", "  8 - новые чанки будут загружены после 128м", "Чем выше значение, тем больше времени требуется на загрузку чанков."}:(btnName.equals("Chunk Updates")?new String[]{"Обновление чанков за один кадр", " 1 - (по умолчанию) медленная загрузка мира, высокое FPS", " 3 - быстрая загрузка мира, малое FPS", " 5 - очень быстрая загрузка мира, очень малое FPS"}:(btnName.equals("Dynamic Updates")?new String[]{"Динамическое обновление чанков", " Выкл. - (по умолчанию) стандартное обновление чанков за один кадр", " Вкл. - повышенное обновление, когда игрок стоит на месте", "Динамическое обновление загружает большие чанки", "пока игрок стоит на месте, при этом загружая мир быстрее"}:(btnName.equals("Lazy Chunk Loading")?new String[]{"Ленивая загрузка чанков", " Выкл. - загрузка чанков по умолчанию", " Вкл. - медленная загрузка чанков (гладко)", "Смягчение интегрированной загрузки чанков", "распространяющей чанки за несколько тиков.", "Выключите параметр, если части мира загружаются не правильно.", "Эффективно только на локальных серверах и одноядерных процессорах."}:null))))));   }

   private String getButtonName(String displayString) {
      int pos = displayString.indexOf(58);
      return pos < 0?displayString:displayString.substring(0, pos);
   }

   private GuiButton getSelectedButton(int i, int j) {
      for(int k = 0; k < super.buttonList.size(); ++k) {
         GuiButton btn = (GuiButton)super.buttonList.get(k);
         boolean flag = i >= btn.xPosition && j >= btn.yPosition && i < btn.xPosition + btn.width && j < btn.yPosition + btn.height;
         if(flag) {
            return btn;
         }
      }

      return null;
   }

}
