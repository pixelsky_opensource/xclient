package net.minecraft.src;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.HashSet;
import java.util.Set;
import net.minecraft.client.renderer.RenderEngine;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.texture.TextureStitched;
import net.minecraft.src.Config;
import net.minecraft.util.Icon;

public class TextureUtils {

   public static final String texGrassTop = "grass_top";
   public static final String texStone = "stone";
   public static final String texDirt = "dirt";
   public static final String texGrassSide = "grass_side";
   public static final String texStoneslabSide = "stoneslab_side";
   public static final String texStoneslabTop = "stoneslab_top";
   public static final String texBedrock = "bedrock";
   public static final String texSand = "sand";
   public static final String texGravel = "gravel";
   public static final String texTreeSide = "tree_side";
   public static final String texTreeTop = "tree_top";
   public static final String texOreGold = "oreGold";
   public static final String texOreIron = "oreIron";
   public static final String texOreCoal = "oreCoal";
   public static final String texObsidian = "obsidian";
   public static final String texGrassSideOverlay = "grass_side_overlay";
   public static final String texSnow = "snow";
   public static final String texSnowSide = "snow_side";
   public static final String texMycelSide = "mycel_side";
   public static final String texMycelTop = "mycel_top";
   public static final String texOreDiamond = "oreDiamond";
   public static final String texOreRedstone = "oreRedstone";
   public static final String texOreLapis = "oreLapis";
   public static final String texLeaves = "leaves";
   public static final String texLeavesOpaque = "leaves_opaque";
   public static final String texLeavesJungle = "leaves_jungle";
   public static final String texLeavesJungleOpaque = "leaves_jungle_opaque";
   public static final String texCactusSide = "cactus_side";
   public static final String texClay = "clay";
   public static final String texFarmlandWet = "farmland_wet";
   public static final String texFarmlandDry = "farmland_dry";
   public static final String texHellrock = "hellrock";
   public static final String texHellsand = "hellsand";
   public static final String texLightgem = "lightgem";
   public static final String texTreeSpruce = "tree_spruce";
   public static final String texTreeBirch = "tree_birch";
   public static final String texLeavesSpruce = "leaves_spruce";
   public static final String texLeavesSpruceOpaque = "leaves_spruce_opaque";
   public static final String texTreeJungle = "tree_jungle";
   public static final String texWhiteStone = "whiteStone";
   public static final String texSandstoneTop = "sandstone_top";
   public static final String texSandstoneBottom = "sandstone_bottom";
   public static final String texRedstoneLight = "redstoneLight";
   public static final String texRedstoneLightLit = "redstoneLight_lit";
   public static final String texWater = "water";
   public static final String texWaterFlow = "water_flow";
   public static final String texLava = "lava";
   public static final String texLavaFlow = "lava_flow";
   public static final String texFire0 = "fire_0";
   public static final String texFire1 = "fire_1";
   public static final String texPortal = "portal";
   public static Icon iconGrassTop;
   public static Icon iconGrassSide;
   public static Icon iconGrassSideOverlay;
   public static Icon iconSnow;
   public static Icon iconSnowSide;
   public static Icon iconMycelSide;
   public static Icon iconMycelTop;
   public static Icon iconWater;
   public static Icon iconWaterFlow;
   public static Icon iconLava;
   public static Icon iconLavaFlow;
   public static Icon iconPortal;
   public static Icon iconFire0;
   public static Icon iconFire1;
   private static Set atlasNames = makeAtlasNames();
   private static Set atlasIds = new HashSet();


   private static Set makeAtlasNames() {
      HashSet set = new HashSet();
      set.add("/terrain.png");
      set.add("/gui/items.png");
      set.add("/ctm.png");
      set.add("/eloraam/world/world1.png");
      set.add("/gfx/buildcraft/blocks/blocks.png");
      return set;
   }

   public static void update(RenderEngine re) {
      TextureMap terrain = re.textureMapBlocks;
      iconGrassTop = terrain.registerIcon("grass_top");
      iconGrassSide = terrain.registerIcon("grass_side");
      iconGrassSideOverlay = terrain.registerIcon("grass_side_overlay");
      iconSnow = terrain.registerIcon("snow");
      iconSnowSide = terrain.registerIcon("snow_side");
      iconMycelSide = terrain.registerIcon("mycel_side");
      iconMycelTop = terrain.registerIcon("mycel_top");
      iconWater = terrain.registerIcon("water");
      iconWaterFlow = terrain.registerIcon("water_flow");
      iconLava = terrain.registerIcon("lava");
      iconLavaFlow = terrain.registerIcon("lava_flow");
      iconFire0 = terrain.registerIcon("fire_0");
      iconFire1 = terrain.registerIcon("fire_1");
      iconPortal = terrain.registerIcon("portal");
   }

   public static void textureCreated(String name, int id) {
      if(atlasNames.contains(name)) {
         atlasIds.add(Integer.valueOf(id));
      }

   }

   public static void addAtlasName(String name) {
      if(name != null) {
         atlasNames.add(name);
         Config.dbg("TextureAtlas: " + name);
      }
   }

   public static boolean isAtlasId(int i) {
      return atlasIds.contains(Integer.valueOf(i));
   }

   public static boolean isAtlasName(String name) {
      return atlasNames.contains(name);
   }

   public static BufferedImage fixTextureDimensions(String name, BufferedImage bi) {
      if(name.startsWith("/mob/zombie") || name.startsWith("/mob/pigzombie")) {
         int width = bi.getWidth();
         int height = bi.getHeight();
         if(width == height * 2) {
            BufferedImage scaledImage = new BufferedImage(width, height * 2, 2);
            Graphics2D gr = scaledImage.createGraphics();
            gr.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            gr.drawImage(bi, 0, 0, width, height, (ImageObserver)null);
            return scaledImage;
         }
      }

      return bi;
   }

   public static TextureStitched getTextureStitched(Icon icon) {
      return icon instanceof TextureStitched?(TextureStitched)icon:null;
   }

   public static int ceilPowerOfTwo(int val) {
      int i;
      for(i = 1; i < val; i *= 2) {
         ;
      }

      return i;
   }

   public static int getPowerOfTwo(int val) {
      int i = 1;

      int po2;
      for(po2 = 0; i < val; ++po2) {
         i *= 2;
      }

      return po2;
   }

   public static int twoToPower(int power) {
      int val = 1;

      for(int i = 0; i < power; ++i) {
         val *= 2;
      }

      return val;
   }

}
