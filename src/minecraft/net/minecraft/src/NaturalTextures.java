package net.minecraft.src;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import net.minecraft.client.renderer.RenderEngine;
import net.minecraft.client.renderer.texture.TextureMap;
import net.minecraft.client.renderer.texture.TextureStitched;
import net.minecraft.client.texturepacks.TexturePackDefault;
import net.minecraft.src.Config;
import net.minecraft.src.NaturalProperties;
import net.minecraft.util.Icon;

public class NaturalTextures {

   private static RenderEngine renderEngine = null;
   private static NaturalProperties[] propertiesByIndex = new NaturalProperties[0];


   public static void update(RenderEngine re) {
      propertiesByIndex = new NaturalProperties[0];
      renderEngine = re;
      if(Config.isNaturalTextures()) {
         String fileName = "/natural.properties";

         try {
            InputStream e = re.texturePack.getSelectedTexturePack().getResourceAsStream(fileName);
            if(e == null) {
               Config.dbg("NaturalTextures: configuration \"" + fileName + "\" not found");
               propertiesByIndex = makeDefaultProperties();
               return;
            }

            ArrayList list = new ArrayList(256);
            String configStr = Config.readInputStream(e);
            e.close();
            String[] configLines = Config.tokenize(configStr, "\n\r");
            Config.dbg("Natural Textures: Parsing configuration \"" + fileName + "\"");

            for(int i = 0; i < configLines.length; ++i) {
               String line = configLines[i].trim();
               if(!line.startsWith("#")) {
                  String[] strs = Config.tokenize(line, "=");
                  if(strs.length != 2) {
                     Config.dbg("Natural Textures: Invalid \"" + fileName + "\" line: " + line);
                  } else {
                     String key = strs[0].trim();
                     String type = strs[1].trim();
                     TextureStitched ts = re.textureMapBlocks.getIconSafe(key);
                     if(ts == null) {
                        Config.dbg("Natural Textures: Texture not found: \"" + fileName + "\" line: " + line);
                     } else {
                        int tileNum = ts.getIndexInMap();
                        if(tileNum < 0) {
                           Config.dbg("Natural Textures: Invalid \"" + fileName + "\" line: " + line);
                        } else {
                           NaturalProperties props = new NaturalProperties(type);
                           if(props.isValid()) {
                              while(list.size() <= tileNum) {
                                 list.add((Object)null);
                              }

                              list.set(tileNum, props);
                           }
                        }
                     }
                  }
               }
            }

            propertiesByIndex = (NaturalProperties[])((NaturalProperties[])list.toArray(new NaturalProperties[list.size()]));
         } catch (FileNotFoundException var15) {
            Config.dbg("NaturalTextures: configuration \"" + fileName + "\" not found");
            propertiesByIndex = makeDefaultProperties();
            return;
         } catch (Exception var16) {
            var16.printStackTrace();
         }

      }
   }

   public static NaturalProperties getNaturalProperties(Icon icon) {
      if(!(icon instanceof TextureStitched)) {
         return null;
      } else {
         TextureStitched ts = (TextureStitched)icon;
         int tileNum = ts.getIndexInMap();
         if(tileNum >= 0 && tileNum < propertiesByIndex.length) {
            NaturalProperties props = propertiesByIndex[tileNum];
            return props;
         } else {
            return null;
         }
      }
   }

   private static NaturalProperties[] makeDefaultProperties() {
      if(!(renderEngine.texturePack.getSelectedTexturePack() instanceof TexturePackDefault)) {
         Config.dbg("NaturalTextures: Texture pack is not default, ignoring default configuration.");
         return new NaturalProperties[0];
      } else {
         Config.dbg("Natural Textures: Using default configuration.");
         ArrayList propsList = new ArrayList();
         setIconProperties(propsList, "grass_top", "4F");
         setIconProperties(propsList, "stone", "2F");
         setIconProperties(propsList, "dirt", "4F");
         setIconProperties(propsList, "grass_side", "F");
         setIconProperties(propsList, "grass_side_overlay", "F");
         setIconProperties(propsList, "stoneslab_top", "F");
         setIconProperties(propsList, "bedrock", "2F");
         setIconProperties(propsList, "sand", "4F");
         setIconProperties(propsList, "gravel", "2");
         setIconProperties(propsList, "tree_side", "2F");
         setIconProperties(propsList, "tree_top", "4F");
         setIconProperties(propsList, "oreGold", "2F");
         setIconProperties(propsList, "oreIron", "2F");
         setIconProperties(propsList, "oreCoal", "2F");
         setIconProperties(propsList, "oreDiamond", "2F");
         setIconProperties(propsList, "oreRedstone", "2F");
         setIconProperties(propsList, "oreLapis", "2F");
         setIconProperties(propsList, "obsidian", "4F");
         setIconProperties(propsList, "leaves", "2F");
         setIconProperties(propsList, "leaves_opaque", "2F");
         setIconProperties(propsList, "leaves_jungle", "2");
         setIconProperties(propsList, "leaves_jungle_opaque", "2");
         setIconProperties(propsList, "snow", "4F");
         setIconProperties(propsList, "snow_side", "F");
         setIconProperties(propsList, "cactus_side", "2F");
         setIconProperties(propsList, "clay", "4F");
         setIconProperties(propsList, "mycel_side", "F");
         setIconProperties(propsList, "mycel_top", "4F");
         setIconProperties(propsList, "farmland_wet", "2F");
         setIconProperties(propsList, "farmland_dry", "2F");
         setIconProperties(propsList, "hellrock", "4F");
         setIconProperties(propsList, "hellsand", "4F");
         setIconProperties(propsList, "lightgem", "4");
         setIconProperties(propsList, "tree_spruce", "2F");
         setIconProperties(propsList, "tree_birch", "F");
         setIconProperties(propsList, "leaves_spruce", "2F");
         setIconProperties(propsList, "leaves_spruce_opaque", "2F");
         setIconProperties(propsList, "tree_jungle", "2F");
         setIconProperties(propsList, "whiteStone", "4");
         setIconProperties(propsList, "sandstone_top", "4");
         setIconProperties(propsList, "sandstone_bottom", "4F");
         setIconProperties(propsList, "redstoneLight_lit", "4F");
         NaturalProperties[] terrainProps = (NaturalProperties[])((NaturalProperties[])propsList.toArray(new NaturalProperties[propsList.size()]));
         return terrainProps;
      }
   }

   private static void setIconProperties(List propsList, String iconName, String propStr) {
      TextureMap terrainMap = renderEngine.textureMapBlocks;
      Icon icon = terrainMap.registerIcon(iconName);
      if(icon == null) {
         Config.dbg("*** NaturalProperties: Icon not found: " + iconName + " ***");
      } else if(!(icon instanceof TextureStitched)) {
         Config.dbg("*** NaturalProperties: Icon is not IconStitched: " + iconName + ": " + icon.getClass().getName() + " ***");
      } else {
         TextureStitched ts = (TextureStitched)icon;
         int index = ts.getIndexInMap();
         if(index < 0) {
            Config.dbg("*** NaturalProperties: Invalid index for icon: " + iconName + ": " + index + " ***");
         } else {
            while(index >= propsList.size()) {
               propsList.add((Object)null);
            }

            NaturalProperties props = new NaturalProperties(propStr);
            propsList.set(index, props);
         }
      }
   }

   public static void updateIcons(TextureMap textureMap) {}

}
