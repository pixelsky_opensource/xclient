package net.minecraft.src;

import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderEngine;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.client.texturepacks.ITexturePack;
import net.minecraft.client.texturepacks.TexturePackList;
import net.minecraft.server.ThreadMinecraftServer;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.WorldServer;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.ContextCapabilities;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.GLU;

public class Config
{
    public static final String OF_NAME = "OptiFine";
    public static final String MC_VERSION = "1.5.2";
    public static final String OF_EDITION = "HD_U";
    public static final String OF_RELEASE = "D3";
    public static final String VERSION = "OptiFine_1.5.2_HD_U_D3";
    private static String newRelease = null;

    private static GameSettings gameSettings = null;

    private static Minecraft minecraft = null;

    private static Thread minecraftThread = null;

    private static DisplayMode desktopDisplayMode = null;

    private static int antialiasingLevel = 0;

    private static int availableProcessors = 0;

    public static boolean zoomMode = false;

    private static int texturePackClouds = 0;

    private static PrintStream systemOut = new PrintStream(new FileOutputStream(FileDescriptor.out));

    public static final Boolean DEF_FOG_FANCY = Boolean.valueOf(true);
    public static final Float DEF_FOG_START = Float.valueOf(0.2F);
    public static final Boolean DEF_OPTIMIZE_RENDER_DISTANCE = Boolean.valueOf(false);
    public static final Boolean DEF_OCCLUSION_ENABLED = Boolean.valueOf(false);
    public static final Integer DEF_MIPMAP_LEVEL = Integer.valueOf(0);
    public static final Integer DEF_MIPMAP_TYPE = Integer.valueOf(9984);
    public static final Float DEF_ALPHA_FUNC_LEVEL = Float.valueOf(0.1F);
    public static final Boolean DEF_LOAD_CHUNKS_FAR = Boolean.valueOf(false);
    public static final Integer DEF_PRELOADED_CHUNKS = Integer.valueOf(0);
    public static final Integer DEF_CHUNKS_LIMIT = Integer.valueOf(25);
    public static final Integer DEF_UPDATES_PER_FRAME = Integer.valueOf(3);
    public static final Boolean DEF_DYNAMIC_UPDATES = Boolean.valueOf(false);

    public static String getVersion()
    {
        return "OptiFine_1.5.2_HD_U_D3";
    }

    private static void checkOpenGlCaps()
    {
        log("");
        log(getVersion());
        log("" + new Date());
        log("OS: " + System.getProperty("os.name") + " (" + System.getProperty("os.arch") + ") version " + System.getProperty("os.version"));
        log("Java: " + System.getProperty("java.version") + ", " + System.getProperty("java.vendor"));
        log("VM: " + System.getProperty("java.vm.name") + " (" + System.getProperty("java.vm.info") + "), " + System.getProperty("java.vm.vendor"));
        log("LWJGL: " + Sys.getVersion());
        log("OpenGL: " + GL11.glGetString(7937) + " version " + GL11.glGetString(7938) + ", " + GL11.glGetString(7936));

        int ver = getOpenGlVersion();
        String verStr = "" + ver / 10 + "." + ver % 10;
        log("OpenGL Version: " + verStr);

        if (!GLContext.getCapabilities().OpenGL12) {
            log("OpenGL Mipmap levels: Not available (GL12.GL_TEXTURE_MAX_LEVEL)");
        }
        if (!GLContext.getCapabilities().GL_NV_fog_distance) {
            log("OpenGL Fancy fog: Not available (GL_NV_fog_distance)");
        }
        if (!GLContext.getCapabilities().GL_ARB_occlusion_query) {
            log("OpenGL Occlussion culling: Not available (GL_ARB_occlusion_query)");
        }
        int maxTexSize = Minecraft.getGLMaximumTextureSize();
        dbg("Maximum texture size: " + maxTexSize + "x" + maxTexSize);
    }

    public static boolean isFancyFogAvailable()
    {
        return GLContext.getCapabilities().GL_NV_fog_distance;
    }

    public static boolean isOcclusionAvailable()
    {
        return GLContext.getCapabilities().GL_ARB_occlusion_query;
    }

    private static int getOpenGlVersion()
    {
        if (!GLContext.getCapabilities().OpenGL11)
            return 10;
        if (!GLContext.getCapabilities().OpenGL12)
            return 11;
        if (!GLContext.getCapabilities().OpenGL13)
            return 12;
        if (!GLContext.getCapabilities().OpenGL14)
            return 13;
        if (!GLContext.getCapabilities().OpenGL15) {
            return 14;
        }
        if (!GLContext.getCapabilities().OpenGL20)
            return 15;
        if (!GLContext.getCapabilities().OpenGL21)
            return 20;
        if (!GLContext.getCapabilities().OpenGL30)
            return 21;
        if (!GLContext.getCapabilities().OpenGL31)
            return 30;
        if (!GLContext.getCapabilities().OpenGL32)
            return 31;
        if (!GLContext.getCapabilities().OpenGL33)
            return 32;
        if (!GLContext.getCapabilities().OpenGL40) {
            return 33;
        }
        return 40;
    }

    public static void setGameSettings(GameSettings options)
    {
        if (gameSettings == null)
        {
            if (!Display.isCreated()) {
                return;
            }
            checkOpenGlCaps();

            startVersionCheckThread();
        }

        gameSettings = options;

        minecraft = Minecraft.getMinecraft();
        // minecraft = gameSettings.mc; Doesn't compile

        minecraftThread = Thread.currentThread();

        if (gameSettings != null) {
            antialiasingLevel = gameSettings.ofAaLevel;
        }
        updateThreadPriorities();
    }

    public static void updateThreadPriorities()
    {
        try
        {
            ThreadGroup tg = Thread.currentThread().getThreadGroup();
            if (tg == null)
                return;
            int num = (tg.activeCount() + 10) * 2;
            Thread[] ts = new Thread[num];
            tg.enumerate(ts, false);

            int prioMc = 5;
            int prioSrv = 5;
            if (isSmoothWorld())
            {
                prioSrv = 3;
            }

            minecraftThread.setPriority(prioMc);

            for (int i = 0; i < ts.length; i++)
            {
                Thread t = ts[i];
                if (t == null) {
                    continue;
                }
                if ((t instanceof ThreadMinecraftServer)) {
                    t.setPriority(prioSrv);
                }
            }
        }
        catch (Throwable e)
        {
            dbg(e.getMessage());
        }
    }

    public static boolean isMinecraftThread()
    {
        return Thread.currentThread() == minecraftThread;
    }

    private static void startVersionCheckThread()
    {
        VersionCheckThread vct = new VersionCheckThread();
        vct.start();
    }

    public static boolean isUseMipmaps()
    {
        int mipmapLevel = getMipmapLevel();
        return mipmapLevel > 0;
    }

    public static int getMipmapLevel()
    {
        if (gameSettings == null) {
            return DEF_MIPMAP_LEVEL.intValue();
        }
        return gameSettings.ofMipmapLevel;
    }

    public static int getMipmapType()
    {
        if (gameSettings == null) {
            return DEF_MIPMAP_TYPE.intValue();
        }

        switch (gameSettings.ofMipmapType)
        {
            case 0:
                return 9984;
            case 1:
                return 9986;
            case 2:
                if (isMultiTexture()) {
                    return 9985;
                }
                return 9986;
            case 3:
                if (isMultiTexture()) {
                    return 9987;
                }
                return 9986;
        }

        return 9984;
    }

    public static boolean isUseAlphaFunc()
    {
        float alphaFuncLevel = getAlphaFuncLevel();

        return alphaFuncLevel > DEF_ALPHA_FUNC_LEVEL.floatValue() + 1.0E-05F;
    }

    public static float getAlphaFuncLevel()
    {
        return DEF_ALPHA_FUNC_LEVEL.floatValue();
    }

    public static boolean isFogFancy()
    {
        if (!isFancyFogAvailable()) {
            return false;
        }
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofFogType == 2;
    }

    public static boolean isFogFast()
    {
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofFogType == 1;
    }

    public static boolean isFogOff()
    {
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofFogType == 3;
    }

    public static float getFogStart()
    {
        if (gameSettings == null) {
            return DEF_FOG_START.floatValue();
        }
        return gameSettings.ofFogStart;
    }

    public static boolean isOcclusionEnabled()
    {
        if (gameSettings == null) {
            return DEF_OCCLUSION_ENABLED.booleanValue();
        }
        return gameSettings.advancedOpengl;
    }

    public static boolean isOcclusionFancy()
    {
        if (!isOcclusionEnabled()) {
            return false;
        }
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofOcclusionFancy;
    }

    public static boolean isLoadChunksFar()
    {
        if (gameSettings == null) {
            return DEF_LOAD_CHUNKS_FAR.booleanValue();
        }
        return gameSettings.ofLoadFar;
    }

    public static int getPreloadedChunks()
    {
        if (gameSettings == null) {
            return DEF_PRELOADED_CHUNKS.intValue();
        }
        return gameSettings.ofPreloadedChunks;
    }

    public static void dbg(String s)
    {
        systemOut.print("[OptiFine] ");
        systemOut.println(s);
    }

    public static void log(String s)
    {
        dbg(s);
    }

    public static int getUpdatesPerFrame()
    {
        if (gameSettings != null) {
            return gameSettings.ofChunkUpdates;
        }

        return 1;
    }

    public static boolean isDynamicUpdates()
    {
        if (gameSettings != null) {
            return gameSettings.ofChunkUpdatesDynamic;
        }
        return true;
    }

    public static boolean isRainFancy()
    {
        if (gameSettings.ofRain == 0) {
            return gameSettings.fancyGraphics;
        }
        return gameSettings.ofRain == 2;
    }

    public static boolean isWaterFancy()
    {
        if (gameSettings.ofWater == 0) {
            return gameSettings.fancyGraphics;
        }
        return gameSettings.ofWater == 2;
    }

    public static boolean isRainOff()
    {
        return gameSettings.ofRain == 3;
    }

    public static boolean isCloudsFancy()
    {
        if (gameSettings.ofClouds != 0) {
            return gameSettings.ofClouds == 2;
        }
        if (texturePackClouds != 0) {
            return texturePackClouds == 2;
        }
        return gameSettings.fancyGraphics;
    }

    public static boolean isCloudsOff()
    {
        return gameSettings.ofClouds == 3;
    }

    public static void updateTexturePackClouds()
    {
        texturePackClouds = 0;

        RenderEngine re = getRenderEngine();
        if (re == null)
            return;
        ITexturePack tp = re.getTexturePack().getSelectedTexturePack();
        if (tp == null) {
            return;
        }

        try
        {
            InputStream in = tp.getResourceAsStream("/color.properties");
            if (in == null)
                return;
            Properties props = new Properties();
            props.load(in);

            in.close();

            String cloudStr = props.getProperty("clouds");
            if (cloudStr == null) {
                return;
            }
            dbg("Texture pack clouds: " + cloudStr);
            cloudStr = cloudStr.toLowerCase();
            if (cloudStr.equals("fast"))
                texturePackClouds = 1;
            if (cloudStr.equals("fancy"))
                texturePackClouds = 2;
        }
        catch (Exception e)
        {
        }
    }

    public static boolean isTreesFancy()
    {
        if (gameSettings.ofTrees == 0) {
            return gameSettings.fancyGraphics;
        }
        return gameSettings.ofTrees == 2;
    }

    public static boolean isGrassFancy()
    {
        if (gameSettings.ofGrass == 0) {
            return gameSettings.fancyGraphics;
        }
        return gameSettings.ofGrass == 2;
    }

    public static boolean isDroppedItemsFancy()
    {
        if (gameSettings.ofDroppedItems == 0) {
            return gameSettings.fancyGraphics;
        }
        return gameSettings.ofDroppedItems == 2;
    }

    public static int limit(int val, int min, int max)
    {
        if (val < min)
            return min;
        if (val > max) {
            return max;
        }
        return val;
    }

    public static float limit(float val, float min, float max)
    {
        if (val < min)
            return min;
        if (val > max) {
            return max;
        }
        return val;
    }

    public static float limitTo1(float val)
    {
        if (val < 0.0F)
            return 0.0F;
        if (val > 1.0F) {
            return 1.0F;
        }
        return val;
    }

    public static boolean isAnimatedWater()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedWater != 2;
        }
        return true;
    }

    public static boolean isGeneratedWater()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedWater == 1;
        }
        return true;
    }

    public static boolean isAnimatedPortal()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedPortal;
        }
        return true;
    }

    public static boolean isAnimatedLava()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedLava != 2;
        }
        return true;
    }

    public static boolean isGeneratedLava()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedLava == 1;
        }
        return true;
    }

    public static boolean isAnimatedFire()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedFire;
        }
        return true;
    }

    public static boolean isAnimatedRedstone()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedRedstone;
        }
        return true;
    }

    public static boolean isAnimatedExplosion()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedExplosion;
        }
        return true;
    }

    public static boolean isAnimatedFlame()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedFlame;
        }
        return true;
    }

    public static boolean isAnimatedSmoke()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedSmoke;
        }
        return true;
    }

    public static boolean isVoidParticles()
    {
        if (gameSettings != null) {
            return gameSettings.ofVoidParticles;
        }
        return true;
    }

    public static boolean isWaterParticles()
    {
        if (gameSettings != null) {
            return gameSettings.ofWaterParticles;
        }
        return true;
    }

    public static boolean isRainSplash()
    {
        if (gameSettings != null) {
            return gameSettings.ofRainSplash;
        }
        return true;
    }

    public static boolean isPortalParticles()
    {
        if (gameSettings != null) {
            return gameSettings.ofPortalParticles;
        }
        return true;
    }

    public static boolean isPotionParticles()
    {
        if (gameSettings != null) {
            return gameSettings.ofPotionParticles;
        }
        return true;
    }

    public static boolean isDepthFog()
    {
        if (gameSettings != null) {
            return gameSettings.ofDepthFog;
        }
        return true;
    }

    public static float getAmbientOcclusionLevel()
    {
        if (gameSettings != null) {
            return gameSettings.ofAoLevel;
        }
        return 0.0F;
    }

    private static Method getMethod(Class cls, String methodName, Object[] params)
    {
        Method[] methods = cls.getMethods();
        for (int i = 0; i < methods.length; i++)
        {
            Method m = methods[i];

            if (!m.getName().equals(methodName)) {
                continue;
            }
            if (m.getParameterTypes().length == params.length)
            {
                return m;
            }
        }
        dbg("No method found for: " + cls.getName() + "." + methodName + "(" + arrayToString(params) + ")");
        return null;
    }

    public static String arrayToString(Object[] arr)
    {
        if (arr == null) {
            return "";
        }
        StringBuffer buf = new StringBuffer(arr.length * 5);
        for (int i = 0; i < arr.length; i++)
        {
            Object obj = arr[i];
            if (i > 0)
                buf.append(", ");
            buf.append(String.valueOf(obj));
        }
        return buf.toString();
    }

    public static String arrayToString(int[] arr)
    {
        if (arr == null) {
            return "";
        }
        StringBuffer buf = new StringBuffer(arr.length * 5);
        for (int i = 0; i < arr.length; i++)
        {
            int x = arr[i];
            if (i > 0)
                buf.append(", ");
            buf.append(String.valueOf(x));
        }
        return buf.toString();
    }

    public static Minecraft getMinecraft()
    {
        return minecraft;
    }

    public static RenderEngine getRenderEngine()
    {
        return minecraft.renderEngine;
    }

    public static RenderGlobal getRenderGlobal()
    {
        if (minecraft == null) {
            return null;
        }
        return minecraft.renderGlobal;
    }

    public static int getMaxDynamicTileWidth()
    {
        return 64;
    }

    public static Icon getSideGrassTexture(IBlockAccess blockAccess, int x, int y, int z, int side, Icon icon)
    {
        if (!isBetterGrass())
        {
            return icon;
        }

        Icon fullIcon = TextureUtils.iconGrassTop;

        int destBlockId = 2;

        if (icon == TextureUtils.iconMycelSide)
        {
            fullIcon = TextureUtils.iconMycelTop;

            destBlockId = 110;
        }

        if (isBetterGrassFancy())
        {
            y--;
            switch (side)
            {
                case 2:
                    z--;
                    break;
                case 3:
                    z++;
                    break;
                case 4:
                    x--;
                    break;
                case 5:
                    x++;
            }

            int blockId = blockAccess.getBlockId(x, y, z);

            if (blockId != destBlockId)
            {
                return icon;
            }
        }

        return fullIcon;
    }

    public static Icon getSideSnowGrassTexture(IBlockAccess blockAccess, int x, int y, int z, int side)
    {
        if (!isBetterGrass())
        {
            return TextureUtils.iconSnowSide;
        }
        if (isBetterGrassFancy())
        {
            switch (side)
            {
                case 2:
                    z--;
                    break;
                case 3:
                    z++;
                    break;
                case 4:
                    x--;
                    break;
                case 5:
                    x++;
            }

            int blockId = blockAccess.getBlockId(x, y, z);
            if ((blockId != 78) && (blockId != 80))
            {
                return TextureUtils.iconSnowSide;
            }
        }

        return TextureUtils.iconSnow;
    }

    public static boolean isBetterGrass()
    {
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofBetterGrass != 3;
    }

    public static boolean isBetterGrassFancy()
    {
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofBetterGrass == 2;
    }

    public static boolean isWeatherEnabled()
    {
        if (gameSettings == null) {
            return true;
        }
        return gameSettings.ofWeather;
    }

    public static boolean isSkyEnabled()
    {
        if (gameSettings == null) {
            return true;
        }
        return gameSettings.ofSky;
    }

    public static boolean isSunMoonEnabled()
    {
        if (gameSettings == null) {
            return true;
        }
        return gameSettings.ofSunMoon;
    }

    public static boolean isStarsEnabled()
    {
        if (gameSettings == null) {
            return true;
        }
        return gameSettings.ofStars;
    }

    public static void sleep(long ms)
    {
        try
        {
            Thread.currentThread(); Thread.sleep(ms);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    public static boolean isTimeDayOnly()
    {
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofTime == 1;
    }

    public static boolean isTimeDefault()
    {
        if (gameSettings == null) {
            return false;
        }
        return (gameSettings.ofTime == 0) || (gameSettings.ofTime == 2);
    }

    public static boolean isTimeNightOnly()
    {
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofTime == 3;
    }

    public static boolean isClearWater()
    {
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofClearWater;
    }

    public static int getAnisotropicFilterLevel()
    {
        if (gameSettings == null) {
            return 1;
        }
        return gameSettings.ofAfLevel;
    }

    public static int getAntialiasingLevel()
    {
        return antialiasingLevel;
    }

    public static boolean between(int val, int min, int max)
    {
        return (val >= min) && (val <= max);
    }

    public static boolean isMultiTexture()
    {
        if (getAnisotropicFilterLevel() > 1) {
            return true;
        }

        return getAntialiasingLevel() > 0;
    }

    public static boolean isDrippingWaterLava()
    {
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofDrippingWaterLava;
    }

    public static boolean isBetterSnow()
    {
        if (gameSettings == null) {
            return false;
        }
        return gameSettings.ofBetterSnow;
    }

    public static Dimension getFullscreenDimension()
    {
        if (desktopDisplayMode == null) {
            return null;
        }
        if (gameSettings == null) {
            return new Dimension(desktopDisplayMode.getWidth(), desktopDisplayMode.getHeight());
        }
        String dimStr = gameSettings.ofFullscreenMode;
        if (dimStr.equals("Default")) {
            return new Dimension(desktopDisplayMode.getWidth(), desktopDisplayMode.getHeight());
        }
        String[] dimStrs = tokenize(dimStr, " x");
        if (dimStrs.length < 2) {
            return new Dimension(desktopDisplayMode.getWidth(), desktopDisplayMode.getHeight());
        }
        return new Dimension(parseInt(dimStrs[0], -1), parseInt(dimStrs[1], -1));
    }

    public static int parseInt(String str, int defVal)
    {
        try
        {
            if (str == null) {
                return defVal;
            }
            return Integer.parseInt(str);
        }
        catch (NumberFormatException e) {
        }
        return defVal;
    }

    public static float parseFloat(String str, float defVal)
    {
        try
        {
            if (str == null) {
                return defVal;
            }
            return Float.parseFloat(str);
        }
        catch (NumberFormatException e) {
        }
        return defVal;
    }

    public static String[] tokenize(String str, String delim)
    {
        StringTokenizer tok = new StringTokenizer(str, delim);
        List list = new ArrayList();
        while (tok.hasMoreTokens())
        {
            String token = tok.nextToken();
            list.add(token);
        }
        String[] strs = (String[])(String[])list.toArray(new String[list.size()]);
        return strs;
    }

    public static DisplayMode getDesktopDisplayMode()
    {
        return desktopDisplayMode;
    }

    public static void setDesktopDisplayMode(DisplayMode desktopDisplayMode)
    {
        Config.desktopDisplayMode = desktopDisplayMode;
    }

    public static DisplayMode[] getFullscreenDisplayModes()
    {
        try
        {
            DisplayMode[] var0 = Display.getAvailableDisplayModes();
            ArrayList<DisplayMode> var1 = new ArrayList<DisplayMode>();

            for (int var2 = 0; var2 < var0.length; ++var2)
            {
                DisplayMode var3 = var0[var2];

                if (desktopDisplayMode == null || var3.getBitsPerPixel() == desktopDisplayMode.getBitsPerPixel() && var3.getFrequency() == desktopDisplayMode.getFrequency())
                {
                    var1.add(var3);
                }
            }

            DisplayMode[] var5 = (DisplayMode[])((DisplayMode[])var1.toArray(new DisplayMode[var1.size()]));
            Arrays.sort(var5, new Comparator<DisplayMode>() {
                public int compare(DisplayMode var1, DisplayMode var2)
                {
                    DisplayMode var3 = (DisplayMode)var1;
                    DisplayMode var4 = (DisplayMode)var2;
                    return var3.getWidth() != var4.getWidth() ? var4.getWidth() - var3.getWidth() : (var3.getHeight() != var4.getHeight() ? var4.getHeight() - var3.getHeight() : 0);
                }
            });
            return var5;
        }
        catch (Exception var4)
        {
            var4.printStackTrace();
            return new DisplayMode[] {desktopDisplayMode};
        }
    }
    public static String[] getFullscreenModes() {
        DisplayMode[] modes = getFullscreenDisplayModes();
        String[] names = new String[modes.length];
        for (int i = 0; i < modes.length; i++)
        {
            DisplayMode mode = modes[i];
            String name = "" + mode.getWidth() + "x" + mode.getHeight();
            names[i] = name;
        }
        return names;
    }

    public static DisplayMode getDisplayMode(Dimension dim)
            throws LWJGLException
    {
        DisplayMode[] modes = Display.getAvailableDisplayModes();
        for (int i = 0; i < modes.length; i++)
        {
            DisplayMode dm = modes[i];
            if (dm.getWidth() != dim.width)
                continue;
            if (dm.getHeight() != dim.height)
                continue;
            if (desktopDisplayMode != null)
            {
                if (dm.getBitsPerPixel() != desktopDisplayMode.getBitsPerPixel())
                    continue;
                if (dm.getFrequency() != desktopDisplayMode.getFrequency())
                    continue;
            }
            else {
                return dm;
            }
        }
        return desktopDisplayMode;
    }

    public static boolean isAnimatedTerrain()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedTerrain;
        }
        return true;
    }

    public static boolean isAnimatedItems()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedItems;
        }
        return true;
    }

    public static boolean isAnimatedTextures()
    {
        if (gameSettings != null) {
            return gameSettings.ofAnimatedTextures;
        }
        return true;
    }

    public static boolean isSwampColors()
    {
        if (gameSettings != null) {
            return gameSettings.ofSwampColors;
        }
        return true;
    }

    public static boolean isRandomMobs()
    {
        if (gameSettings != null) {
            return gameSettings.ofRandomMobs;
        }
        return true;
    }

    public static void checkGlError(String loc)
    {
        int i = GL11.glGetError();
        if (i != 0)
        {
            String text = GLU.gluErrorString(i);
            dbg("OpenGlError: " + i + " (" + text + "), at: " + loc);
        }
    }

    public static boolean isSmoothBiomes()
    {
        if (gameSettings != null) {
            return gameSettings.ofSmoothBiomes;
        }
        return true;
    }

    public static boolean isCustomColors()
    {
        if (gameSettings != null) {
            return gameSettings.ofCustomColors;
        }
        return true;
    }

    public static boolean isCustomSky()
    {
        if (gameSettings != null) {
            return gameSettings.ofCustomSky;
        }
        return true;
    }

    public static boolean isCustomFonts()
    {
        if (gameSettings != null) {
            return gameSettings.ofCustomFonts;
        }
        return true;
    }

    public static boolean isShowCapes()
    {
        if (gameSettings != null) {
            return gameSettings.ofShowCapes;
        }
        return true;
    }

    public static boolean isConnectedTextures()
    {
        if (gameSettings != null) {
            return gameSettings.ofConnectedTextures != 3;
        }
        return false;
    }

    public static boolean isNaturalTextures()
    {
        if (gameSettings != null) {
            return gameSettings.ofNaturalTextures;
        }
        return false;
    }

    public static boolean isConnectedTexturesFancy()
    {
        if (gameSettings != null) {
            return gameSettings.ofConnectedTextures == 2;
        }
        return false;
    }

    public static String[] readLines(File file)
            throws IOException
    {
        List list = new ArrayList();

        FileInputStream fis = new FileInputStream(file);
        InputStreamReader isr = new InputStreamReader(fis, "ASCII");
        BufferedReader br = new BufferedReader(isr);
        while (true)
        {
            String line = br.readLine();
            if (line == null)
                break;
            list.add(line);
        }

        String[] lines = (String[])(String[])list.toArray(new String[list.size()]);

        return lines;
    }

    public static String readFile(File file)
            throws IOException
    {
        FileInputStream fin = new FileInputStream(file);
        return readInputStream(fin, "ASCII");
    }

    public static String readInputStream(InputStream in)
            throws IOException
    {
        return readInputStream(in, "ASCII");
    }

    public static String readInputStream(InputStream in, String encoding)
            throws IOException
    {
        InputStreamReader inr = new InputStreamReader(in, encoding);
        BufferedReader br = new BufferedReader(inr);
        StringBuffer sb = new StringBuffer();
        while (true)
        {
            String line = br.readLine();
            if (line == null)
                break;
            sb.append(line);
            sb.append("\n");
        }

        return sb.toString();
    }

    public static GameSettings getGameSettings()
    {
        return gameSettings;
    }

    public static String getNewRelease()
    {
        return newRelease;
    }

    public static void setNewRelease(String newRelease)
    {
        newRelease = newRelease;
    }

    public static int compareRelease(String rel1, String rel2)
    {
        String[] rels1 = splitRelease(rel1);
        String[] rels2 = splitRelease(rel2);

        String branch1 = rels1[0];
        String branch2 = rels2[0];
        if (!branch1.equals(branch2)) {
            return branch1.compareTo(branch2);
        }
        int rev1 = parseInt(rels1[1], -1);
        int rev2 = parseInt(rels2[1], -1);
        if (rev1 != rev2) {
            return rev1 - rev2;
        }
        String suf1 = rels1[2];
        String suf2 = rels2[2];
        return suf1.compareTo(suf2);
    }

    private static String[] splitRelease(String relStr)
    {
        if ((relStr == null) || (relStr.length() <= 0)) {
            return new String[] { "", "", "" };
        }
        String branch = relStr.substring(0, 1);
        if (relStr.length() <= 1) {
            return new String[] { branch, "", "" };
        }
        int pos = 1;
        while ((pos < relStr.length()) && (Character.isDigit(relStr.charAt(pos))))
        {
            pos++;
        }
        String revision = relStr.substring(1, pos);
        if (pos >= relStr.length()) {
            return new String[] { branch, revision, "" };
        }
        String suffix = relStr.substring(pos);
        return new String[] { branch, revision, suffix };
    }

    public static int intHash(int x)
    {
        x = x ^ 0x3D ^ x >> 16;
        x += (x << 3);
        x ^= x >> 4;
        x *= 668265261;
        x ^= x >> 15;
        return x;
    }

    public static int getRandom(int x, int y, int z, int face)
    {
        int rand = intHash(face + 37);
        rand = intHash(rand + x);
        rand = intHash(rand + z);
        rand = intHash(rand + y);

        return rand;
    }

    public static WorldServer getWorldServer()
    {
        if (minecraft == null) {
            return null;
        }
        World world = minecraft.theWorld;
        if (world == null) {
            return null;
        }
        WorldProvider wp = world.provider;
        if (wp == null) {
            return null;
        }
        int wt = wp.dimensionId;

        IntegratedServer is = minecraft.getIntegratedServer();
        if (is == null) {
            return null;
        }
        WorldServer ws = is.worldServerForDimension(wt);

        return ws;
    }

    public static int getAvailableProcessors()
    {
        if (availableProcessors < 1) {
            availableProcessors = Runtime.getRuntime().availableProcessors();
        }
        return availableProcessors;
    }

    public static boolean isSingleProcessor()
    {
        return getAvailableProcessors() <= 1;
    }

    public static boolean isSmoothWorld()
    {
        if (getAvailableProcessors() > 1) {
            return false;
        }
        if (gameSettings == null) {
            return true;
        }
        return gameSettings.ofSmoothWorld;
    }

    public static boolean isLazyChunkLoading()
    {
        if (getAvailableProcessors() > 1) {
            return false;
        }
        if (gameSettings == null) {
            return true;
        }
        return gameSettings.ofLazyChunkLoading;
    }

    public static int getChunkViewDistance()
    {
        if (gameSettings == null) {
            return 10;
        }
        int chunkDistance = gameSettings.ofRenderDistanceFine / 16;

        if (chunkDistance <= 16) {
            return 10;
        }
        return chunkDistance;
    }
}