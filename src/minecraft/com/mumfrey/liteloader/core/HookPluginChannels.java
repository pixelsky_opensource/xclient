 package com.mumfrey.liteloader.core;

 import com.mumfrey.liteloader.util.PrivateFields;
 import net.minecraft.network.packet.NetHandler;
 import net.minecraft.network.packet.Packet;
 import net.minecraft.network.packet.Packet250CustomPayload;
 import net.minecraft.util.IntHashMap;

 import java.io.DataInputStream;
 import java.io.DataOutputStream;
 import java.io.IOException;
 import java.util.Map;
 
 public class HookPluginChannels extends Packet250CustomPayload
 {
   private static boolean registered = false;
   private static LiteLoader packetHandler;
   private static Class proxyClass;
   private Packet proxyPacket;
 
   public HookPluginChannels()
   {
     try
     {
       if (proxyClass != null)
       {
         this.proxyPacket = ((Packet)proxyClass.newInstance());
       }
     }
     catch (Exception ex) {
     }
   }
 
   public HookPluginChannels(String channel, byte[] data) {
     super(channel, data);
     try
     {
       if (proxyClass != null)
       {
         this.proxyPacket = ((Packet)proxyClass.newInstance());
 
         if ((this.proxyPacket instanceof Packet250CustomPayload))
         {
           ((Packet250CustomPayload)this.proxyPacket).channel = this.channel;
           ((Packet250CustomPayload)this.proxyPacket).data = this.data;
           ((Packet250CustomPayload)this.proxyPacket).length = this.length;
         }
       }
     }
     catch (Exception ex)
     {
     }
   }
 
   public void readPacketData(DataInputStream datainputstream) throws IOException
   {
     if (this.proxyPacket != null)
     {
       this.proxyPacket.readPacketData(datainputstream);
       this.channel = ((Packet250CustomPayload)this.proxyPacket).channel;
       this.length = ((Packet250CustomPayload)this.proxyPacket).length;
       this.data = ((Packet250CustomPayload)this.proxyPacket).data;
     }
     else {
       super.readPacketData(datainputstream);
     }
   }
 
   public void writePacketData(DataOutputStream dataoutputstream) throws IOException
   {
     if (this.proxyPacket != null)
       this.proxyPacket.writePacketData(dataoutputstream);
     else
       super.writePacketData(dataoutputstream);
   }
 
   public void processPacket(NetHandler nethandler)
   {
     if (this.proxyPacket != null)
       this.proxyPacket.processPacket(nethandler);
     else {
       super.processPacket(nethandler);
     }
     if (packetHandler != null)
     {
       packetHandler.onPluginChannelMessage(this);
     }
   }
 
   public int getPacketSize()
   {
     if (this.proxyPacket != null) {
       return this.proxyPacket.getPacketSize();
     }
     return super.getPacketSize();
   }
 
   public static void registerPacketHandler(LiteLoader handler)
   {
     packetHandler = handler;
   }
 
   public static void register()
   {
     register(false);
   }
 
   public static void register(boolean force)
   {
     if ((!registered) || (force))
     {
       try
       {
         IntHashMap packetIdToClassMap = Packet.packetIdToClassMap;
         proxyClass = (Class)packetIdToClassMap.lookup(250);
 
         if (proxyClass.equals(Packet250CustomPayload.class))
         {
           proxyClass = null;
         }
 
         packetIdToClassMap.removeObject(250);
         packetIdToClassMap.addKey(250, HookPluginChannels.class);
 
         Map packetClassToIdMap = (Map) PrivateFields.StaticFields.packetClassToIdMap.get();
         packetClassToIdMap.put(HookPluginChannels.class, Integer.valueOf(250));
 
         registered = true;
       }
       catch (Exception ex)
       {
         ex.printStackTrace();
       }
     }
   }
 }