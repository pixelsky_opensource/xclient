 package com.mumfrey.liteloader.core;
 
 import com.mumfrey.liteloader.*;
import com.mumfrey.liteloader.gui.GuiControlsPaginated;
import com.mumfrey.liteloader.util.ModUtilities;
import com.mumfrey.liteloader.util.PrivateFields;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiControls;
import net.minecraft.client.gui.GuiNewChat;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.logging.ILogAgent;
import net.minecraft.network.packet.NetHandler;
import net.minecraft.network.packet.Packet1Login;
import net.minecraft.network.packet.Packet3Chat;
import net.minecraft.profiler.IPlayerUsage;
import net.minecraft.profiler.PlayerUsageSnooper;
import net.minecraft.profiler.Profiler;
import net.minecraft.util.Timer;

import javax.activity.InvalidActivityException;
import java.io.*;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.util.*;
import java.util.logging.*;
import java.util.logging.Formatter;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
 
 public final class LiteLoader
   implements FilenameFilter, IPlayerUsage
 {
   private static final String LOADER_VERSION = "1.5.2";
   private static final int LOADER_REVISION = 9;
   private static final String[] SUPPORTED_VERSIONS = { "1.5.2", "1.5.r1" };
   private static final int MAX_DISCOVERY_DEPTH = 16;
   private static LiteLoader instance;
   public static Logger logger = Logger.getLogger("liteloader");
 
   private static boolean useStdOut = false;
   private File modsFolder;
   private Minecraft minecraft = Minecraft.getMinecraft();
 
   private File propertiesFile = new File(Minecraft.getMinecraftDir(), "liteloader.properties");
 
   private Properties internalProperties = new Properties();
 
   private Properties localProperties = new Properties();
 
   private String branding = null;
 
   private boolean paginateControls = true;
   private Timer minecraftTimer;
   private String loadedModsList = "none";
 
   private Collection<LiteMod> mods = new LinkedList();
 
   private LinkedList tickListeners = new LinkedList();
 
   private LinkedList loopListeners = new LinkedList();
 
   private LinkedList initListeners = new LinkedList();
 
   private LinkedList renderListeners = new LinkedList();
 
   private LinkedList postRenderListeners = new LinkedList();
 
   private LinkedList chatRenderListeners = new LinkedList();
 
   private LinkedList chatListeners = new LinkedList();
 
   private LinkedList chatFilters = new LinkedList();
 
   private LinkedList loginListeners = new LinkedList();
 
   private LinkedList preLoginListeners = new LinkedList();
 
   private LinkedList pluginChannelListeners = new LinkedList();
 
   private HashMap pluginChannels = new HashMap();
   private Method mAddUrl;
   private boolean loaderStartupDone;
   private boolean loaderStartupComplete;
   private boolean lateInitDone;
   private boolean chatHooked;
   private boolean loginHooked;
   private boolean pluginChannelHooked;
   private boolean tickHooked;
   private HookProfiler profilerHook = new HookProfiler(this, logger);
   private ScaledResolution currentResolution;
 
   public static final LiteLoader getInstance()
   {
     if (instance == null)
     {
       instance = new LiteLoader();
       instance.initLoader();
     }
 
     return instance;
   }
 
   public static final Logger getLogger()
   {
     return logger;
   }
 
   public static final PrintStream getConsoleStream()
   {
     return useStdOut ? System.out : System.err;
   }
 
   public static final String getVersion()
   {
     return "1.5.2";
   }
 
   public static final int getRevision()
   {
     return 9;
   }
 
   private void initLoader()
   {
     if (this.loaderStartupDone) return;
     this.loaderStartupDone = true;
 
     prepareClassOverrides();
 
     if (prepareLoader())
     {
       logger.info(String.format("LiteLoader %s starting up...", new Object[] { "1.5.2" }));
 
       if (this.branding != null)
       {
         logger.info(String.format("Active Pack: %s", new Object[] { this.branding }));
       }
 
       logger.info(String.format("Java reports OS=\"%s\"", new Object[] { System.getProperty("os.name").toLowerCase() }));
 
       boolean searchMods = this.localProperties.getProperty("search.mods", "true").equalsIgnoreCase("true");
       boolean searchProtectionDomain = this.localProperties.getProperty("search.jar", "true").equalsIgnoreCase("true");
       boolean searchClassPath = this.localProperties.getProperty("search.classpath", "true").equalsIgnoreCase("true");
 
       if ((!searchMods) && (!searchProtectionDomain) && (!searchClassPath))
       {
         logger.warning("Invalid configuration, no search locations defined. Enabling all search locations.");
 
         this.localProperties.setProperty("search.mods", "true");
         this.localProperties.setProperty("search.jar", "true");
         this.localProperties.setProperty("search.classpath", "true");
 
         searchMods = true;
         searchProtectionDomain = true;
         searchClassPath = true;
       }
 
       prepareMods(searchMods, searchProtectionDomain, searchClassPath);
 
       initMods();
 
       initHooks();
 
       this.loaderStartupComplete = true;
 
       writeProperties();
     }
   }
 
   private void prepareClassOverrides()
   {
     registerBaseClassOverride(ModUtilities.getObfuscatedFieldName("net.minecraft.src.CallableJVMFlags", "h", "h"), "h");
   }
 
   private void registerBaseClassOverride(String binaryClassName, String fileName)
   {
     try
     {
       Method mDefineClass = ClassLoader.class.getDeclaredMethod("defineClass", new Class[] { String.class, byte[].class, Integer.TYPE, Integer.TYPE });
       mDefineClass.setAccessible(true);
 
       InputStream resourceInputStream = LiteLoader.class.getResourceAsStream("/classes/" + fileName + ".bin");
 
       if (resourceInputStream != null)
       {
         ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
 
         for (int readBytes = resourceInputStream.read(); readBytes >= 0; readBytes = resourceInputStream.read())
         {
           outputStream.write(readBytes);
         }
 
         byte[] data = outputStream.toByteArray();
 
         outputStream.close();
         resourceInputStream.close();
 
         logger.info("Defining class override for " + binaryClassName);
         mDefineClass.invoke(Minecraft.class.getClassLoader(), new Object[] { binaryClassName, data, Integer.valueOf(0), Integer.valueOf(data.length) });
       }
       else
       {
         logger.info("Error defining class override for " + binaryClassName + ", file not found");
       }
     }
     catch (Throwable th)
     {
       logger.log(Level.WARNING, "Error defining class override for " + binaryClassName, th);
     }
   }
 
   private boolean prepareLoader()
   {
     try
     {
       this.mAddUrl = URLClassLoader.class.getDeclaredMethod("addURL", new Class[] { URL.class });
       this.mAddUrl.setAccessible(true);
 
       prepareProperties();
 
       prepareLogger();
 
       this.paginateControls = this.localProperties.getProperty("controls.pages", "true").equalsIgnoreCase("true");
       this.localProperties.setProperty("controls.pages", String.valueOf(this.paginateControls));
 
       this.branding = this.internalProperties.getProperty("brand", null);
       if ((this.branding != null) && (this.branding.length() < 1)) this.branding = null;
 
       if (this.branding != null)
         this.localProperties.setProperty("brand", this.branding);
       else {
         this.localProperties.remove("brand");
       }
     }
     catch (Throwable th)
     {
       logger.log(Level.SEVERE, "Error initialising LiteLoader", th);
       return false;
     }
 
     return true;
   }
 
   private void prepareLogger()
     throws SecurityException, IOException
   {
     Formatter logFormatter = new LiteLoaderLogFormatter();
 
     logger.setUseParentHandlers(false);
     useStdOut = (System.getProperty("liteloader.log", "stderr").equalsIgnoreCase("stdout")) || (this.localProperties.getProperty("log", "stderr").equalsIgnoreCase("stdout"));
 
     StreamHandler consoleHandler = useStdOut ? new com.mumfrey.liteloader.util.log.ConsoleHandler() : new java.util.logging.ConsoleHandler();
     consoleHandler.setFormatter(logFormatter);
     logger.addHandler(consoleHandler);
 
     FileHandler logFileHandler = new FileHandler(new File(Minecraft.getMinecraftDir(), "LiteLoader.txt").getAbsolutePath());
     logFileHandler.setFormatter(logFormatter);
     logger.addHandler(logFileHandler);
   }
 
   private void prepareProperties()
   {
     try
     {
       InputStream propertiesStream = LiteLoader.class.getResourceAsStream("/liteloader.properties");
 
       if (propertiesStream != null)
       {
         this.internalProperties.load(propertiesStream);
         propertiesStream.close();
       }
     }
     catch (Throwable th)
     {
       this.internalProperties = new Properties();
     }
 
     try
     {
       this.localProperties = new Properties(this.internalProperties);
       InputStream localPropertiesStream = getLocalPropertiesStream();
 
       if (localPropertiesStream != null)
       {
         this.localProperties.load(localPropertiesStream);
         localPropertiesStream.close();
       }
     }
     catch (Throwable th)
     {
       this.localProperties = new Properties(this.internalProperties);
     }
   }
 
   private InputStream getLocalPropertiesStream()
     throws FileNotFoundException
   {
     if (this.propertiesFile.exists())
     {
       return new FileInputStream(this.propertiesFile);
     }
 
     return LiteLoader.class.getResourceAsStream("/liteloader.properties");
   }
 
   private void writeProperties()
   {
     try
     {
       this.localProperties.store(new FileWriter(this.propertiesFile), String.format("Properties for LiteLoader %s", new Object[] { "1.5.2" }));
     }
     catch (Throwable th)
     {
       logger.log(Level.WARNING, "Error writing liteloader properties", th);
     }
   }
 
   public File getModsFolder()
   {
     if (this.modsFolder == null)
     {
       this.modsFolder = new File(Minecraft.getMinecraftDir(), "mods");
 
       if ((!this.modsFolder.exists()) || (!this.modsFolder.isDirectory()))
       {
         try
         {
           this.modsFolder.mkdirs();
         }
         catch (Exception ex) {
         }
       }
     }
     return this.modsFolder;
   }
 
   public String getLoadedModsList()
   {
     return this.loadedModsList;
   }
 
   public String getBranding()
   {
     return this.branding;
   }
 
   public LiteMod getMod(String modName)
     throws InvalidActivityException, IllegalArgumentException
   {
     if (!this.loaderStartupComplete)
     {
       throw new InvalidActivityException("Attempted to get a reference to a mod before loader startup is complete");
     }
 
     if (modName == null)
     {
       throw new IllegalArgumentException("Attempted to get a reference to a mod without specifying a mod name");
     }
 
     for (LiteMod mod : this.mods)
     {
       if ((modName.equalsIgnoreCase(mod.getName())) || (modName.equalsIgnoreCase(mod.getClass().getSimpleName()))) return mod;
     }
 
     return null;
   }
 
   private void prepareMods(boolean searchMods, boolean searchProtectionDomain, boolean searchClassPath)
   {
     LinkedList modFiles = new LinkedList();
 
     if (searchMods)
     {
       File modFolder = getModsFolder();
       if ((modFolder.exists()) && (modFolder.isDirectory()))
       {
         logger.info("Mods folder found, searching " + modFolder.getPath());
         findModFiles(modFolder, modFiles);
         logger.info("Found " + modFiles.size() + " mod file(s)");
       }
 
     }
 
     HashMap modsToLoad = null;
     try
     {
       logger.info("Enumerating class path...");
 
       String classPath = System.getProperty("java.class.path");
       String classPathSeparator = System.getProperty("path.separator");
       String[] classPathEntries = classPath.split(classPathSeparator);
 
       logger.info(String.format("Class path separator=\"%s\"", new Object[] { classPathSeparator }));
       logger.info(String.format("Class path entries=(\n   classpathEntry=%s\n)", new Object[] { classPath.replace(classPathSeparator, "\n   classpathEntry=") }));
 
       if ((searchProtectionDomain) || (searchClassPath)) logger.info("Discovering mods on class path...");
       modsToLoad = findModClasses(classPathEntries, modFiles, searchProtectionDomain, searchClassPath);
 
       logger.info("Mod class discovery completed");
     }
     catch (Throwable th)
     {
       logger.log(Level.WARNING, "Mod class discovery failed", th);
       return;
     }
 
     loadMods(modsToLoad);
   }
 
   protected void findModFiles(File modFolder, LinkedList modFiles)
   {
     List supportedVerions = Arrays.asList(SUPPORTED_VERSIONS);
 
     for (File modFile : modFolder.listFiles(this))
     {
       try
       {
         ZipFile modZip = new ZipFile(modFile);
         ZipEntry version = modZip.getEntry("version.txt");
 
         if (version != null)
         {
           InputStream versionStream = modZip.getInputStream(version);
           BufferedReader versionReader = new BufferedReader(new InputStreamReader(versionStream));
           String strVersion = versionReader.readLine();
           versionReader.close();
 
           if ((supportedVerions.contains(strVersion)) && (addURLToClassPath(modFile.toURI().toURL())))
           {
             modFiles.add(modFile);
           }
           else
           {
             logger.info("Not adding invalid or outdated mod file: " + modFile.getAbsolutePath());
           }
         }
 
         modZip.close();
       }
       catch (Exception ex)
       {
         logger.warning("Error enumerating '" + modFile.getAbsolutePath() + "': Invalid zip file or error reading file");
       }
     }
   }
 
   public boolean accept(File dir, String fileName)
   {
     return fileName.toLowerCase().endsWith(".litemod");
   }
 
   private HashMap findModClasses(String[] classPathEntries, LinkedList modFiles, boolean searchProtectionDomain, boolean searchClassPath)
   {
     HashMap modsToLoad = new HashMap();
 
     if (searchProtectionDomain)
     {
       try
       {
         logger.info("Searching protection domain code source...");
 
         File packagePath = null;
 
         URL protectionDomainLocation = LiteLoader.class.getProtectionDomain().getCodeSource().getLocation();
         if (protectionDomainLocation != null)
         {
           if ((protectionDomainLocation.toString().indexOf('!') > -1) && (protectionDomainLocation.toString().startsWith("jar:")))
           {
             protectionDomainLocation = new URL(protectionDomainLocation.toString().substring(4, protectionDomainLocation.toString().indexOf('!')));
           }
 
           packagePath = new File(protectionDomainLocation.toURI());
         }
         else
         {
           String reflectionClassPath = LiteLoader.class.getResource("/com/mumfrey/liteloader/core/LiteLoader.class").getPath();
 
           if (reflectionClassPath.indexOf('!') > -1)
           {
             reflectionClassPath = URLDecoder.decode(reflectionClassPath, "UTF-8");
             packagePath = new File(reflectionClassPath.substring(5, reflectionClassPath.indexOf('!')));
           }
         }
 
         if (packagePath != null)
         {
           LinkedList modClasses = getSubclassesFor(packagePath, Minecraft.class.getClassLoader(), LiteMod.class, "LiteMod");
 
           for (Class mod : (Collection<Class>)modClasses)
           {
             if (modsToLoad.containsKey(mod.getSimpleName()))
             {
               logger.warning("Mod name collision for mod with class '" + mod.getSimpleName() + "', maybe you have more than one copy?");
             }
 
             modsToLoad.put(mod.getSimpleName(), mod);
           }
 
           if (modClasses.size() > 0) logger.info(String.format("Found %s potential matches", new Object[] { Integer.valueOf(modClasses.size()) }));
         }
       }
       catch (Throwable th)
       {
         logger.warning("Error loading from local class path: " + th.getMessage());
       }
     }
 
     if (searchClassPath)
     {
       for (String classPathPart : classPathEntries)
       {
         logger.info(String.format("Searching %s...", new Object[] { classPathPart }));
 
         File packagePath = new File(classPathPart);
         LinkedList modClasses = getSubclassesFor(packagePath, Minecraft.class.getClassLoader(), LiteMod.class, "LiteMod");
 
         for (Class mod : (Collection<Class>)modClasses)
         {
           if (modsToLoad.containsKey(mod.getSimpleName()))
           {
             logger.warning("Mod name collision for mod with class '" + mod.getSimpleName() + "', maybe you have more than one copy?");
           }
 
           modsToLoad.put(mod.getSimpleName(), mod);
         }
 
         if (modClasses.size() <= 0) continue; logger.info(String.format("Found %s potential matches", new Object[] { Integer.valueOf(modClasses.size()) }));
       }
 
     }
 
     for (File modFile : (Collection<File>)modFiles)
     {
       logger.info(String.format("Searching %s...", new Object[] { modFile.getAbsolutePath() }));
 
       LinkedList modClasses = getSubclassesFor(modFile, Minecraft.class.getClassLoader(), LiteMod.class, "LiteMod");
 
       for (Class mod : (Collection<Class>)modClasses)
       {
         if (modsToLoad.containsKey(mod.getSimpleName()))
         {
           logger.warning("Mod name collision for mod with class '" + mod.getSimpleName() + "', maybe you have more than one copy?");
         }
 
         modsToLoad.put(mod.getSimpleName(), mod);
       }
 
       if (modClasses.size() > 0) logger.info(String.format("Found %s potential matches", new Object[] { Integer.valueOf(modClasses.size()) }));
     }
 
     return modsToLoad;
   }
 
   private void loadMods(HashMap modsToLoad)
   {
     if (modsToLoad == null)
     {
       logger.info("Mod class discovery failed. Not loading any mods!");
       return;
     }
 
     logger.info("Discovered " + modsToLoad.size() + " total mod(s)");
 
     for (Class mod : (Collection<Class>)modsToLoad.values())
     {
       try
       {
         logger.info("Loading mod from " + mod.getName());
 
         LiteMod newMod = (LiteMod)mod.newInstance();
         this.mods.add(newMod);
 
         logger.info("Successfully added mod " + newMod.getName() + " version " + newMod.getVersion());
       }
       catch (Throwable th)
       {
         logger.warning(th.toString());
         th.printStackTrace();
       }
     }
   }
 
   private void initMods()
   {
     this.loadedModsList = "";
     int loadedModsCount = 0;
 
     for (Iterator iter = this.mods.iterator(); iter.hasNext(); )
     {
       LiteMod mod = (LiteMod)iter.next();
       try
       {
         logger.info("Initialising mod " + mod.getName() + " version " + mod.getVersion());
 
         mod.init();
 
         if ((mod instanceof Tickable))
         {
           addTickListener((Tickable)mod);
         }
 
         if ((mod instanceof GameLoopListener))
         {
           addLoopListener((GameLoopListener)mod);
         }
 
         if ((mod instanceof InitCompleteListener))
         {
           addInitListener((InitCompleteListener)mod);
         }
 
         if ((mod instanceof RenderListener))
         {
           addRenderListener((RenderListener)mod);
         }
 
         if ((mod instanceof PostRenderListener))
         {
           addPostRenderListener((PostRenderListener)mod);
         }
 
         if ((mod instanceof ChatFilter))
         {
           addChatFilter((ChatFilter)mod);
         }
 
         if ((mod instanceof ChatListener))
         {
           if ((mod instanceof ChatFilter))
           {
             logger.warning(String.format("Interface error initialising mod '%1s'. A mod implementing ChatFilter and ChatListener is not supported! Remove one of these interfaces", new Object[] { mod.getName() }));
           }
           else
           {
             addChatListener((ChatListener)mod);
           }
         }
 
         if ((mod instanceof ChatRenderListener))
         {
           addChatRenderListener((ChatRenderListener)mod);
         }
 
         if ((mod instanceof PreLoginListener))
         {
           addPreLoginListener((PreLoginListener)mod);
         }
 
         if ((mod instanceof LoginListener))
         {
           addLoginListener((LoginListener)mod);
         }
 
         if ((mod instanceof PluginChannelListener))
         {
           addPluginChannelListener((PluginChannelListener)mod);
         }
 
         this.loadedModsList += String.format("\n          - %s version %s", new Object[] { mod.getName(), mod.getVersion() });
         loadedModsCount++;
       }
       catch (Throwable th)
       {
         logger.log(Level.WARNING, "Error initialising mod '" + mod.getName(), th);
         iter.remove();
       }
     }
 
     this.loadedModsList = String.format("%s loaded mod(s)%s", new Object[] { Integer.valueOf(loadedModsCount), this.loadedModsList });
   }
 
   private void initHooks()
   {
     try
     {
       if (((this.chatListeners.size() > 0) || (this.chatFilters.size() > 0)) && (!this.chatHooked))
       {
         this.chatHooked = true;
         HookChat.register();
         HookChat.registerPacketHandler(this);
       }
 
       if (((this.preLoginListeners.size() > 0) || (this.loginListeners.size() > 0)) && (!this.loginHooked))
       {
         this.loginHooked = true;
         ModUtilities.registerPacketOverride(1, HookLogin.class);
         HookLogin.loader = this;
       }
 
       if ((this.pluginChannelListeners.size() > 0) && (!this.pluginChannelHooked))
       {
         this.pluginChannelHooked = true;
         HookPluginChannels.register();
         HookPluginChannels.registerPacketHandler(this);
       }
 
       if (!this.tickHooked)
       {
         this.tickHooked = true;
         PrivateFields.minecraftProfiler.setFinal(this.minecraft, this.profilerHook);
       }
 
       PlayerUsageSnooper snooper = this.minecraft.getPlayerUsageSnooper();
       PrivateFields.playerStatsCollector.setFinal(snooper, this);
     }
     catch (Exception ex)
     {
       logger.log(Level.WARNING, "Error creating hooks", ex);
       ex.printStackTrace();
     }
   }
 
   public void addTickListener(Tickable tickable)
   {
     if (!this.tickListeners.contains(tickable))
     {
       this.tickListeners.add(tickable);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addLoopListener(GameLoopListener loopListener)
   {
     if (!this.loopListeners.contains(loopListener))
     {
       this.loopListeners.add(loopListener);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addInitListener(InitCompleteListener initCompleteListener)
   {
     if (!this.initListeners.contains(initCompleteListener))
     {
       this.initListeners.add(initCompleteListener);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addRenderListener(RenderListener tickable)
   {
     if (!this.renderListeners.contains(tickable))
     {
       this.renderListeners.add(tickable);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addPostRenderListener(PostRenderListener tickable)
   {
     if (!this.postRenderListeners.contains(tickable))
     {
       this.postRenderListeners.add(tickable);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addChatFilter(ChatFilter chatFilter)
   {
     if (!this.chatFilters.contains(chatFilter))
     {
       this.chatFilters.add(chatFilter);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addChatListener(ChatListener chatListener)
   {
     if (!this.chatListeners.contains(chatListener))
     {
       this.chatListeners.add(chatListener);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addChatRenderListener(ChatRenderListener chatRenderListener)
   {
     if (!this.chatRenderListeners.contains(chatRenderListener))
     {
       this.chatRenderListeners.add(chatRenderListener);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addPreLoginListener(PreLoginListener loginListener)
   {
     if (!this.preLoginListeners.contains(loginListener))
     {
       this.preLoginListeners.add(loginListener);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addLoginListener(LoginListener loginListener)
   {
     if (!this.loginListeners.contains(loginListener))
     {
       this.loginListeners.add(loginListener);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   public void addPluginChannelListener(PluginChannelListener pluginChannelListener)
   {
     if (!this.pluginChannelListeners.contains(pluginChannelListener))
     {
       this.pluginChannelListeners.add(pluginChannelListener);
       if (this.loaderStartupComplete) initHooks();
     }
   }
 
   private static LinkedList getSubclassesFor(File packagePath, ClassLoader classloader, Class superClass, String prefix)
   {
     LinkedList classes = new LinkedList();
     try
     {
       if (packagePath.isDirectory())
       {
         enumerateDirectory(prefix, superClass, classloader, classes, packagePath);
       }
       else if ((packagePath.isFile()) && ((packagePath.getName().endsWith(".jar")) || (packagePath.getName().endsWith(".zip")) || (packagePath.getName().endsWith(".litemod"))))
       {
         enumerateCompressedPackage(prefix, superClass, classloader, classes, packagePath);
       }
     }
     catch (Throwable th)
     {
       logger.log(Level.WARNING, "Enumeration error", th);
     }
 
     return classes;
   }
 
   private static void enumerateCompressedPackage(String prefix, Class superClass, ClassLoader classloader, LinkedList classes, File packagePath)
     throws IOException, FileNotFoundException
   {
     FileInputStream fileinputstream = new FileInputStream(packagePath);
     ZipInputStream zipinputstream = new ZipInputStream(fileinputstream);
 
     ZipEntry zipentry = null;
     do
     {
       zipentry = zipinputstream.getNextEntry();
 
       if ((zipentry == null) || (!zipentry.getName().endsWith(".class")))
         continue;
       String classFileName = zipentry.getName();
       String className = classFileName.lastIndexOf('/') > -1 ? classFileName.substring(classFileName.lastIndexOf('/') + 1) : classFileName;
 
       if ((prefix != null) && (!className.startsWith(prefix)))
         continue;
       try
       {
         String fullClassName = classFileName.substring(0, classFileName.length() - 6).replaceAll("/", ".");
         checkAndAddClass(classloader, superClass, classes, fullClassName);
       }
       catch (Exception ex)
       {
       }
     }
 
     while (zipentry != null);
 
     fileinputstream.close();
   }
 
   private static void enumerateDirectory(String prefix, Class superClass, ClassLoader classloader, LinkedList classes, File packagePath)
   {
     enumerateDirectory(prefix, superClass, classloader, classes, packagePath, "", 0);
   }
 
   private static void enumerateDirectory(String prefix, Class superClass, ClassLoader classloader, LinkedList classes, File packagePath, String packageName, int depth)
   {
     if (depth > 16) return;
 
     File[] classFiles = packagePath.listFiles();
 
     for (File classFile : classFiles)
     {
       if (classFile.isDirectory())
       {
         enumerateDirectory(prefix, superClass, classloader, classes, classFile, packageName + classFile.getName() + ".", depth + 1);
       }
       else
       {
         if ((!classFile.getName().endsWith(".class")) || ((prefix != null) && (!classFile.getName().startsWith(prefix))))
           continue;
         String classFileName = classFile.getName();
         String className = packageName + classFileName.substring(0, classFileName.length() - 6);
         checkAndAddClass(classloader, superClass, classes, className);
       }
     }
   }
 
   private static void checkAndAddClass(ClassLoader classloader, Class superClass, LinkedList classes, String className)
   {
     if (className.indexOf('$') > -1) {
       return;
     }
     try
     {
       Class subClass = classloader.loadClass(className);
 
       if ((subClass != null) && (!superClass.equals(subClass)) && (superClass.isAssignableFrom(subClass)) && (!subClass.isInterface()) && (!classes.contains(subClass)))
       {
         classes.add(subClass);
       }
     }
     catch (Throwable th)
     {
       logger.log(Level.WARNING, "checkAndAddClass error", th);
     }
   }
 
   private boolean addURLToClassPath(URL classUrl)
   {
     try
     {
       if (((Minecraft.class.getClassLoader() instanceof URLClassLoader)) && (this.mAddUrl != null) && (this.mAddUrl.isAccessible()))
       {
         URLClassLoader classLoader = (URLClassLoader)Minecraft.class.getClassLoader();
         this.mAddUrl.invoke(classLoader, new Object[] { classUrl });
         return true;
       }
     }
     catch (Throwable th)
     {
       logger.log(Level.WARNING, "Error adding class path entry", th);
     }
 
     return false;
   }
 
   public void onInit()
   {
     if (!this.lateInitDone)
     {
       this.lateInitDone = true;
 
       for (InitCompleteListener initMod : (Collection<InitCompleteListener>)this.initListeners)
       {
         try
         {
           logger.info("Calling late init for mod " + initMod.getName());
           initMod.onInitCompleted(this.minecraft, this);
         }
         catch (Throwable th)
         {
           logger.log(Level.WARNING, "Error initialising mod " + initMod.getName(), th);
         }
       }
     }
   }
 
   public void onRender()
   {
     if ((this.paginateControls) && (this.minecraft.currentScreen != null) && (this.minecraft.currentScreen.getClass().equals(GuiControls.class)))
     {
       try
       {
         GuiScreen parentScreen = (GuiScreen)PrivateFields.guiControlsParentScreen.get((GuiControls)this.minecraft.currentScreen);
         this.minecraft.displayGuiScreen(new GuiControlsPaginated(parentScreen, this.minecraft.gameSettings));
       }
       catch (Exception ex) {
       }
     }
     for (RenderListener renderListener : (Collection<RenderListener>)this.renderListeners)
       renderListener.onRender();
   }
 
   public void postRenderEntities()
   {
     float partialTicks = this.minecraftTimer != null ? this.minecraftTimer.elapsedPartialTicks : 0.0F;
 
     for (PostRenderListener renderListener : (Collection<PostRenderListener>)this.postRenderListeners)
       renderListener.onPostRenderEntities(partialTicks);
   }
 
   public void postRender()
   {
     float partialTicks = this.minecraftTimer != null ? this.minecraftTimer.elapsedPartialTicks : 0.0F;
 
     for (PostRenderListener renderListener : (Collection<PostRenderListener>)this.postRenderListeners)
       renderListener.onPostRender(partialTicks);
   }
 
   public void onBeforeGuiRender()
   {
     for (RenderListener renderListener : (Collection<RenderListener>)this.renderListeners)
       renderListener.onRenderGui(this.minecraft.currentScreen);
   }
 
   public void onSetupCameraTransform()
   {
     for (RenderListener renderListener : (Collection<RenderListener>)this.renderListeners)
       renderListener.onSetupCameraTransform();
   }
 
   public void onBeforeChatRender()
   {
     this.currentResolution = new ScaledResolution(this.minecraft.gameSettings, this.minecraft.displayWidth, this.minecraft.displayHeight);
     int screenWidth = this.currentResolution.getScaledWidth();
     int screenHeight = this.currentResolution.getScaledHeight();
 
     GuiNewChat chat = this.minecraft.ingameGUI.getChatGUI();
 
     for (ChatRenderListener chatRenderListener : (Collection<ChatRenderListener>)this.chatRenderListeners)
       chatRenderListener.onPreRenderChat(screenWidth, screenHeight, chat);
   }
 
   public void onAfterChatRender()
   {
     int screenWidth = this.currentResolution.getScaledWidth();
     int screenHeight = this.currentResolution.getScaledHeight();
 
     GuiNewChat chat = this.minecraft.ingameGUI.getChatGUI();
 
     for (ChatRenderListener chatRenderListener : (Collection<ChatRenderListener>)this.chatRenderListeners)
       chatRenderListener.onPostRenderChat(screenWidth, screenHeight, chat);
   }
 
   public void onTimerUpdate()
   {
     for (GameLoopListener loopListener : (Collection<GameLoopListener>)this.loopListeners)
       loopListener.onRunGameLoop(this.minecraft);
   }
 
   public void onTick(Profiler profiler, boolean tick)
   {
     float partialTicks = 0.0F;
 
     if ((tick) || (this.minecraftTimer == null))
     {
       this.minecraftTimer = ((Timer)PrivateFields.minecraftTimer.get(this.minecraft));
     }
 
     if (this.minecraftTimer != null)
     {
       partialTicks = this.minecraftTimer.renderPartialTicks;
       tick = this.minecraftTimer.elapsedTicks > 0;
     }
 
     boolean inGame = (this.minecraft.renderViewEntity != null) && (this.minecraft.renderViewEntity.worldObj != null);
 
     for (Tickable tickable : (Collection<Tickable>)this.tickListeners)
     {
       profiler.startSection(tickable.getClass().getSimpleName());
       tickable.onTick(this.minecraft, partialTicks, inGame, tick);
       profiler.endSection();
     }
   }
 
   public boolean onChat(Packet3Chat chatPacket)
   {
     for (ChatFilter chatFilter : (Collection<ChatFilter>)this.chatFilters) {
       if (!chatFilter.onChat(chatPacket)) {
         return false;
       }
     }
     for (ChatListener chatListener : (Collection<ChatListener>)this.chatListeners) {
       chatListener.onChat(chatPacket.message);
     }
     return true;
   }
 
   public boolean onPreLogin(NetHandler netHandler, Packet1Login loginPacket)
   {
     boolean cancelled = false;
 
     for (PreLoginListener loginListener : (Collection<PreLoginListener>)this.preLoginListeners)
     {
       cancelled |= !loginListener.onPreLogin(netHandler, loginPacket);
     }
 
     return !cancelled;
   }
 
   public void onConnectToServer(NetHandler netHandler, Packet1Login loginPacket)
   {
     for (LoginListener loginListener : (Collection<LoginListener>)this.loginListeners) {
       loginListener.onLogin(netHandler, loginPacket);
     }
     setupPluginChannels();
   }
 
   public void onPluginChannelMessage(HookPluginChannels hookPluginChannels)
   {
     if ((hookPluginChannels != null) && (hookPluginChannels.channel != null) && (this.pluginChannels.containsKey(hookPluginChannels.channel)))
     {
       for (PluginChannelListener pluginChannelListener : (Collection<PluginChannelListener>)this.pluginChannels.get(hookPluginChannels.channel))
       {
         try
         {
           pluginChannelListener.onCustomPayload(hookPluginChannels.channel, hookPluginChannels.length, hookPluginChannels.data);
         }
         catch (Exception ex)
         {
         }
       }
     }
   }
 
   public void sendPluginChannelMessage(String channel, byte[] data)
   {
     ModUtilities.sendPluginChannelMessage(channel, data);
   }
 
   protected void setupPluginChannels()
   {
     this.pluginChannels.clear();
 
     for (PluginChannelListener pluginChannelListener : (Collection<PluginChannelListener>)pluginChannelListeners) {
 
       List channels = pluginChannelListener.getChannels();
 
       if (channels != null)
       {
         for (String channel : (Collection<String>)channels)
         {
           if ((channel.length() > 16) || (channel.toUpperCase().equals("REGISTER")) || (channel.toUpperCase().equals("UNREGISTER"))) {
             continue;
           }
           if (!this.pluginChannels.containsKey(channel))
           {
             this.pluginChannels.put(channel, new LinkedList());
           }
 
           ((LinkedList)this.pluginChannels.get(channel)).add(pluginChannelListener);
         }
       }
     }
     PluginChannelListener pluginChannelListener;
     if (this.pluginChannels.keySet().size() > 0)
     {
       StringBuilder channelList = new StringBuilder();
       boolean separator = false;
 
       for (String channel : (Collection<String>)this.pluginChannels.keySet())
       {
         if (separator) channelList.append("");
         channelList.append(channel);
         separator = true;
       }
 
       byte[] registrationData = channelList.toString().getBytes(Charset.forName("UTF8"));
 
       sendPluginChannelMessage("REGISTER", registrationData);
     }
   }
 
   public void addServerStatsToSnooper(PlayerUsageSnooper var1)
   {
     this.minecraft.addServerStatsToSnooper(var1);
   }
 
   public void addServerTypeToSnooper(PlayerUsageSnooper var1)
   {
     sanityCheck();
     this.minecraft.addServerTypeToSnooper(var1);
   }
 
   public boolean isSnooperEnabled()
   {
     return this.minecraft.isSnooperEnabled();
   }
 
   public ILogAgent getLogAgent()
   {
     return this.minecraft.getLogAgent();
   }
 
   private void sanityCheck()
   {
     if ((this.tickHooked) && (this.minecraft.mcProfiler != this.profilerHook))
     {
       PrivateFields.minecraftProfiler.setFinal(this.minecraft, this.profilerHook);
     }
   }
 }