package com.mumfrey.liteloader;

import com.mumfrey.liteloader.core.LiteLoader;
import net.minecraft.client.Minecraft;

public abstract interface InitCompleteListener extends Tickable
{
  public abstract void onInitCompleted(Minecraft paramMinecraft, LiteLoader paramLiteLoader);
}