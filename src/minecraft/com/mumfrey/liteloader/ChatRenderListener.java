package com.mumfrey.liteloader;

import net.minecraft.client.gui.GuiNewChat;

public abstract interface ChatRenderListener extends LiteMod
{
  public abstract void onPreRenderChat(int paramInt1, int paramInt2, GuiNewChat paramGuiNewChat);

  public abstract void onPostRenderChat(int paramInt1, int paramInt2, GuiNewChat paramGuiNewChat);
}