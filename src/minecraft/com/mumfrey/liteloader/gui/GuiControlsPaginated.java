 package com.mumfrey.liteloader.gui;
 
 import java.util.List;
 import net.minecraft.client.Minecraft;
 import net.minecraft.client.gui.GuiButton;
 import net.minecraft.client.gui.GuiScreen;
 import net.minecraft.client.gui.GuiSmallButton;
 import net.minecraft.client.settings.GameSettings;
 import net.minecraft.client.settings.KeyBinding;
 import net.minecraft.util.StringTranslate;
 
 public class GuiControlsPaginated extends GuiScreen
 {
   protected int controlsPerPage;
   protected int startIndex;
   protected int endIndex;
   protected GuiScreen parentScreen;
   protected GameSettings gameSettings;
   protected GuiButton btnNext;
   protected GuiButton btnPrevious;
   protected String screenTitle = "Controls";
 
   protected int activeButtonId = -1;
 
   public GuiControlsPaginated(GuiScreen parentScreen, GameSettings gameSettings)
   {
     this.parentScreen = parentScreen;
     this.gameSettings = gameSettings;
 
     this.controlsPerPage = 14;
     this.endIndex = (this.gameSettings.keyBindings.length - (this.gameSettings.keyBindings.length % this.controlsPerPage == 0 ? this.controlsPerPage : this.gameSettings.keyBindings.length % this.controlsPerPage));
   }
 
   protected List getLegacyControlList()
   {
     return this.buttonList;
   }
 
   protected final int getHeight()
   {
     return this.height;
   }
 
   protected final int getWidth()
   {
     return this.width;
   }
 
   public GuiScreen getParentScreen()
   {
     return this.parentScreen;
   }
 
   public void initGui()
   {
     StringTranslate stringtranslate = StringTranslate.getInstance();
     getLegacyControlList().clear();
 
     int oldControlsPerPage = this.controlsPerPage;
     this.controlsPerPage = ((getHeight() - 70) / 24 * 2);
     this.endIndex = (this.gameSettings.keyBindings.length - (this.gameSettings.keyBindings.length % this.controlsPerPage == 0 ? this.controlsPerPage : this.gameSettings.keyBindings.length % this.controlsPerPage));
     if (oldControlsPerPage != this.controlsPerPage) this.startIndex = 0;
 
     for (int controlId = 0; controlId < this.gameSettings.keyBindings.length; controlId++)
     {
       boolean buttonVisible = (controlId >= this.startIndex) && (controlId < this.startIndex + this.controlsPerPage);
       int left = buttonVisible ? getWidth() / 2 - 155 : getWidth() + 10000;
       int top = getHeight() / 6 + 24 * (controlId - this.startIndex >> 1);
       getLegacyControlList().add(new GuiSmallButton(controlId, left + (controlId - this.startIndex) % 2 * 160, top, 70, 20, this.gameSettings.getOptionDisplayString(controlId)));
     }
 
     int buttonY = getHeight() / 6 + (this.controlsPerPage >> 1) * 24;
 
     if (this.gameSettings.keyBindings.length > this.controlsPerPage)
     {
       getLegacyControlList().add(this.btnNext = new GuiButton(201, getWidth() / 2 - 51, buttonY, 50, 20, ">>"));
       getLegacyControlList().add(this.btnPrevious = new GuiButton(202, getWidth() / 2 - 103, buttonY, 50, 20, "<<"));
       getLegacyControlList().add(new GuiButton(200, getWidth() / 2 + 1, buttonY, 100, 20, stringtranslate.translateKey("gui.done")));
 
       this.btnNext.enabled = (this.startIndex < this.endIndex);
       this.btnPrevious.enabled = (this.startIndex > 0);
     }
     else
     {
       getLegacyControlList().add(new GuiButton(200, getWidth() / 2 - 100, buttonY, stringtranslate.translateKey("gui.done")));
     }
 
     this.screenTitle = stringtranslate.translateKey("controls.title");
   }
 
   protected String getKeybindDescription(int controlId)
   {
     return this.gameSettings.getKeyBindingDescription(controlId);
   }
 
   protected void actionPerformed(GuiButton guibutton)
   {
     for (int i = 0; i < this.gameSettings.keyBindings.length; i++)
     {
       ((GuiButton)getLegacyControlList().get(i)).displayString = this.gameSettings.getOptionDisplayString(i);
     }
 
     if (guibutton.id == 200)
     {
       this.mc.displayGuiScreen(this.parentScreen);
     }
     else if (guibutton.id == 201)
     {
       this.startIndex += this.controlsPerPage;
       this.startIndex = Math.min(this.endIndex, this.startIndex);
       initGui();
     }
     else if (guibutton.id == 202)
     {
       this.startIndex -= this.controlsPerPage;
       this.startIndex = Math.max(0, this.startIndex);
       initGui();
     }
     else
     {
       this.activeButtonId = guibutton.id;
       guibutton.displayString = String.format("> %s <", new Object[] { this.gameSettings.getOptionDisplayString(guibutton.id) });
     }
   }
 
   protected void mouseClicked(int mouseX, int mouseY, int mouseButton)
   {
     if (this.activeButtonId >= 0)
     {
       this.gameSettings.setKeyBinding(this.activeButtonId, -100 + mouseButton);
       ((GuiButton)getLegacyControlList().get(this.activeButtonId)).displayString = this.gameSettings.getOptionDisplayString(this.activeButtonId);
       this.activeButtonId = -1;
       KeyBinding.resetKeyBindingArrayAndHash();
     }
     else
     {
       super.mouseClicked(mouseX, mouseY, mouseButton);
     }
   }
 
   protected void keyTyped(char keyChar, int keyCode)
   {
     if (this.activeButtonId >= 0)
     {
       this.gameSettings.setKeyBinding(this.activeButtonId, keyCode);
       ((GuiButton)getLegacyControlList().get(this.activeButtonId)).displayString = this.gameSettings.getOptionDisplayString(this.activeButtonId);
       this.activeButtonId = -1;
       KeyBinding.resetKeyBindingArrayAndHash();
     }
     else
     {
       super.keyTyped(keyChar, keyCode);
     }
   }
 
   public void drawScreen(int mouseX, int mouseY, float partialTick)
   {
     drawDefaultBackground();
     drawCenteredString(this.fontRenderer, this.screenTitle, this.width / 2, 20, 16777215);
 
     for (int controlId = 0; controlId < this.gameSettings.keyBindings.length; controlId++)
     {
       boolean conflict = false;
 
       for (int id = 0; id < this.gameSettings.keyBindings.length; id++)
       {
         if ((id == controlId) || (this.gameSettings.keyBindings[controlId].keyCode != this.gameSettings.keyBindings[id].keyCode))
           continue;
         conflict = true;
         break;
       }
 
       if (this.activeButtonId == controlId)
       {
         ((GuiButton)getLegacyControlList().get(controlId)).displayString = "§f> §e??? §f<";
       }
       else if (conflict)
       {
         ((GuiButton)getLegacyControlList().get(controlId)).displayString = ("§c" + this.gameSettings.getOptionDisplayString(controlId));
       }
       else
       {
         ((GuiButton)getLegacyControlList().get(controlId)).displayString = this.gameSettings.getOptionDisplayString(controlId);
       }
 
       int left = (controlId >= this.startIndex) && (controlId < this.startIndex + this.controlsPerPage) ? getWidth() / 2 - 155 : getWidth() + 10000;
       drawString(this.fontRenderer, getKeybindDescription(controlId), left + (controlId - this.startIndex) % 2 * 160 + 70 + 6, getHeight() / 6 + 24 * (controlId - this.startIndex >> 1) + 7, 16777215);
     }
 
     super.drawScreen(mouseX, mouseY, partialTick);
   }
 }

/* Location:           /home/arthur/dev/jirai/xClient-mcp/mcp/build/liteloader_1.5.2_deobf.zip
 * Qualified Name:     com.mumfrey.liteloader.gui.GuiControlsPaginated
 * JD-Core Version:    0.6.0
 */