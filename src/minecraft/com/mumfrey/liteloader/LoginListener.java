package com.mumfrey.liteloader;

import net.minecraft.network.packet.NetHandler;
import net.minecraft.network.packet.Packet1Login;

public abstract interface LoginListener extends LiteMod
{
  public abstract void onLogin(NetHandler paramNetHandler, Packet1Login paramPacket1Login);
}