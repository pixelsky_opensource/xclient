package com.mumfrey.liteloader;

public abstract interface ChatListener extends LiteMod
{
  public abstract void onChat(String paramString);
}