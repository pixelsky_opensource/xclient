package com.mumfrey.liteloader;

import net.minecraft.client.gui.GuiScreen;

public abstract interface RenderListener extends LiteMod
{
  public abstract void onRender();

  public abstract void onRenderGui(GuiScreen paramGuiScreen);

  public abstract void onRenderWorld();

  public abstract void onSetupCameraTransform();
}