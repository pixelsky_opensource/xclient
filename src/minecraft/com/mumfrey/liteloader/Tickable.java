package com.mumfrey.liteloader;

import net.minecraft.client.Minecraft;

public abstract interface Tickable extends LiteMod
{
  public abstract void onTick(Minecraft paramMinecraft, float paramFloat, boolean paramBoolean1, boolean paramBoolean2);
}