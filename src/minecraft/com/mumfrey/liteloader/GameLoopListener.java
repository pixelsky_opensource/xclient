package com.mumfrey.liteloader;

import net.minecraft.client.Minecraft;

public abstract interface GameLoopListener
{
  public abstract void onRunGameLoop(Minecraft paramMinecraft);
}