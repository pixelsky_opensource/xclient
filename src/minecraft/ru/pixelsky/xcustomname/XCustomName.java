package ru.pixelsky.xcustomname;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.Player;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Mod(modid = "xCustomName", name = "xCustomName", version = "1.0.0")
@NetworkMod(serverSideRequired = false, clientSideRequired = false, channels = {"Prefix|reset", "Prefix|set"}, packetHandler = XCustomName.PrefixPacketHandler.class)
public class XCustomName {
    private final static String channelSet = "Prefix|set";
    private final static String channelReset = "Prefix|reset";
    private static Map<Integer, String> displayNames = new ConcurrentHashMap<Integer, String>();

    public static String getDisplayName(EntityPlayer player) {
        String displayName = displayNames.get(player.entityId);
        return displayName != null ? displayName : player.username;
    }

    public static void setDisplayName(int entityId, String displayName) {
        displayNames.put(entityId, displayName);
    }

    public static void resetDisplayName(int entityId) {
        displayNames.remove(entityId);
    }

    public static class PrefixPacketHandler implements IPacketHandler {
        @Override
        public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player player) {
            DataInputStream dis = new DataInputStream(new ByteArrayInputStream(packet.data));
            try {
                int entityId = dis.readInt();
                if (packet.channel.equals(channelSet)) {
                    int bytesLength = dis.readInt();
                    byte[] nameBytes = new byte[bytesLength];
                    dis.read(nameBytes);
                    setDisplayName(entityId, new String(nameBytes, Charset.forName("UTF8")));
                } else {
                    resetDisplayName(entityId);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
